const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();
const mainProcess = remote.require('./main.js');

const config = require('../config.json');

const teamsList = document.querySelector('#teams-list');
const createTeamButton = document.querySelector('#create-team-button');

const usersListHeader = document.querySelector('#users-list-header');
const usersList = document.querySelector('#users-list');

const mainListHeader = document.querySelector('#main-list-header');
const mainListLabel = document.querySelector('#main-list-label');
const mainList = document.querySelector('#main-list');
const addNewFileButton = document.querySelector('#add-file-button');
const addUserToTeamButton = document.querySelector('#add-user-to-team-button');
const invitationsButton = document.querySelector('#invitations-button');
const logoutButton = document.querySelector('#logout-button');

const searchButton = document.querySelector('#search-button')
const searchInputBox = document.querySelector('#search-input-box')
const myAccountButton = document.querySelector('#my-account-button')

const urlToParamDict = (url) =>
    url
        .split('?')[1]
        .split('&')
        .reduce((dict, param) => {
            let key = param.split('=')[0]
            let val = param.split('=')[1]

            dict[key] = val;
            return dict;
        }, {})

//get context from url:
let params = urlToParamDict(window.location.href);
let chosenTeam = params.teamName
let currentlyClickedUser = undefined

const MAX_FILES_PER_PAGE = 4;
let currentFilesPage = 0;

//teamSummaries
let username = undefined;
let synchronizeUsername = () => {
    let requestGetMyUsername = net.request({
        method: 'GET',
        url: `http://localhost:${config.http_port}/username`
    });
    requestGetMyUsername.on('response', (response) => {
        if (response.statusCode === 200) {
            response.on('data', (usernameResponse) => {
                username = usernameResponse.toString('utf-8');
                myAccountButton.innerHTML = `My Account - ${username}`
            });
        } else {

        }
    })
    requestGetMyUsername.end();
}
synchronizeUsername();

const State = {
    FILES: 'files',
    INVITATIONS: 'invitations',
    USERS_LIST: 'users-list',
    USER_DETAILS: 'user_details',
    SEARCH_RESULTS: 'search-results'
}
let state = undefined
if (chosenTeam !== undefined){
    state = State.FILES;
    changeViewToTeamDetails(chosenTeam)
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

createTeamButton.addEventListener('click', () => {
    mainProcess.changeView(__dirname + `/../create-team/create-team.html`, undefined, true);
})

teamsList.addEventListener('click', (event) => {
    if (!isTeamsListEntry(event.target)){
        return
    }
    currentFilesPage = 0
    state = State.FILES;

    chosenTeam = event.target.innerHTML;
    changeViewToTeamDetails(chosenTeam)
})

function changeViewToTeamDetails(teamName){

    mainListHeader.innerHTML =
        `
            <h1 class="main-list-header">Files shared in team <b>${teamName}</b></h1>
        `

    mainListLabel.innerHTML =
        `
            <h1 class="main-list-label">Files:</h1>
        `
    mainListLabel.style.display = 'none';

    addNewFileButton.style.display = 'block';

    addUserToTeamButton.style.display = 'block';
}

const TEAMS_LIST_ENTRY_CLASS_NAME = "team-list-entry"
const isTeamsListEntry = (element) => {
    return element.className === TEAMS_LIST_ENTRY_CLASS_NAME
}

searchButton.addEventListener('click', (event) => {
    let searchedPhase = searchInputBox.value
    if (searchedPhase === ""){
        return;
    }

    state = State.SEARCH_RESULTS
    mainListHeader.innerHTML = `Search results for phase ${searchedPhase}:`
    mainListLabel.innerHTML = 'Click team label on left sidebar to get back to team\'s file list';

    addNewFileButton.style.display = 'none';
})
myAccountButton.addEventListener('click', (event) => {
    state = State.USER_DETAILS
    currentlyClickedUser = username
})
invitationsButton.addEventListener('click', (event) => {
    state = State.INVITATIONS;

    mainListHeader.innerHTML = 'Your team invitations:';
    mainListLabel.innerHTML = 'Click team label on left sidebar to get back to team\'s file list';

    addNewFileButton.style.display = 'none';
});
logoutButton.addEventListener('click', event => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/logout`
    });
    request.on('response', (response) => {
        if (response.statusCode === 200) {
            mainProcess.changeView(__dirname + '/../login/login.html');
        }
    });
    request.end();
})
addUserToTeamButton.addEventListener('click', (event) => {
    state = State.USERS_LIST

    mainListHeader.innerHTML = "Users list:"
    mainListLabel.innerHTML = 'Click team label on left sidebar to get back to team\'s file list';

    addNewFileButton.style.display = 'none';
});

const showTeams = (teamsDetails) => {
    let sortedTeamsDetails = teamsDetails.sort((a, b) => a.teamName.localeCompare(b.teamName));

    let newTeamsListInnerHtml = ``;
    sortedTeamsDetails.forEach((entry) => {
        newTeamsListInnerHtml +=
            `
                <a class="${TEAMS_LIST_ENTRY_CLASS_NAME}">${entry.teamName}</a>
            `
    });
    if (teamsList.innerHTML !== newTeamsListInnerHtml) {
        teamsList.innerHTML = newTeamsListInnerHtml;
    }
}

const showTeamsUsers = (teamDetails) => {
    if (chosenTeam === undefined) {
        return;
    }
    usersListHeader.innerHTML = `Users in team: ` + chosenTeam;

    let newUsersListInnerHTML = ``;
    teamDetails
        .filter((teamDetails) => teamDetails.teamName === chosenTeam)
        .flatMap((teamDetails) => teamDetails.participants)
        .sort((a,b) => a.name.localeCompare(b.name))
        .forEach((participant) => {
            newUsersListInnerHTML +=
                `
                    <a class="user-list-entry">${participant.name}</a>
                `
        });
    if (usersList.innerHTML !== newUsersListInnerHTML) {
        usersList.innerHTML = newUsersListInnerHTML;
        let userListEntries = usersList.getElementsByClassName('user-list-entry');
        for (let item of userListEntries) {
            item.addEventListener('click', (event) => {
                state = State.USER_DETAILS;
                currentlyClickedUser = event.target.text;
                console.log(currentlyClickedUser)
            })
        }
    }
}

const FILE_LIST_ENTRY_CLASS_NAME = 'file-list-entry'
const FILE_NAME_CLASS_NAME = 'file-name'
mainList.addEventListener('click', (event) => {
    if (state === State.FILES) {
        let chosenElement = event.target;
        let fileListEntry = getParentOrThisWithClass(chosenElement, FILE_LIST_ENTRY_CLASS_NAME);
        if (fileListEntry !== undefined){
            let chosenFileName = fileListEntry.querySelector("."+FILE_NAME_CLASS_NAME + " b em").textContent
            mainProcess.changeView(__dirname + '/../file-manage/file-manage.html', {
                teamName: chosenTeam,
                filePublicName: chosenFileName,
            });
        }
    }
    else if (state === State.INVITATIONS){
        let isButton = event.target.type === 'submit';
        if(isButton){
            let teamName = event.target.parentNode.children[0].children[0].innerHTML;
            let decision = event.target.innerHTML === 'accept' ? 'true' : 'false';

            let request = net.request({
                method: 'GET',
                url: `http://localhost:` + config.http_port + `/answerToInvitation?teamName=${teamName}&decision=${decision}`
            });
            request.on('response', (response) => {
                if (response.statusCode === 200) {
                    response.on('data', (chunk) => {
                        let result = chunk.toString('utf-8');
                        //TODO add error message on screen
                    })
                }

            })
            request.end();
        }
    }
    else if (state === State.USERS_LIST){
        let isButton = event.target.type === 'submit';
        if (isButton) {
            let userToAdd = event.target.value;//username is stored in "value" attribute of button
            addUserToTeam(chosenTeam, userToAdd);
        }
    }
})

function getParentOrThisWithClass(element, classname) {
    if (element.className.split(' ').indexOf(classname)>=0) return element;
    if (element.parentNode){
        return getParentOrThisWithClass(element.parentNode, classname);
    }
}

addNewFileButton.addEventListener('click', () => {
    mainProcess.changeView(__dirname + '/../file-manage-new/file-manage-new.html', {
        teamName: chosenTeam
    });
});

const showTeamsDetails = (teamDetails) => {
    if (chosenTeam === undefined){
        return;
    }

    let files = teamDetails
        .filter((teamDetails) => teamDetails.teamName === chosenTeam)
        .flatMap((teamDetails) => teamDetails.sharedFiles)
        .sort((a,b) => a.publicName.localeCompare(b.publicName));
    let currentAllFilesCount = files.length;

    let teamsDetailsFilesListInnerHTML = getFilesListHTML(files)
    if (currentAllFilesCount > MAX_FILES_PER_PAGE){
        teamsDetailsFilesListInnerHTML += getPaginationButtonsHTML();
    }

    if(mainList.innerHTML !== teamsDetailsFilesListInnerHTML) {
        mainList.innerHTML = teamsDetailsFilesListInnerHTML;
        if (currentAllFilesCount > MAX_FILES_PER_PAGE) {
            document.querySelector('#next-page').addEventListener('click', (event) => {
                if (MAX_FILES_PER_PAGE * (currentFilesPage + 1) < currentAllFilesCount) {
                    currentFilesPage += 1
                }
            });
            document.querySelector('#previous-page').addEventListener('click', (event) => {
                if (currentFilesPage > 0) {
                    currentFilesPage -= 1
                }
            })
        }
    }
}

const getFilesListHTML = (files) => {
    files = files.slice(currentFilesPage * MAX_FILES_PER_PAGE, (currentFilesPage + 1) * MAX_FILES_PER_PAGE)

    let newFilesListInnerHtml = `
                <div class="w3-container">
                    <ul class="w3-ul w3-card-4" style="margin-right:40px">
                `;
    files.forEach(file => {
        newFilesListInnerHtml +=
            `
                    <li class="w3-bar file-list-entry" style="height:70px; padding-bottom: 5px; background-color: lightgray; border: solid black 1px">
                        <img src="file.png" class="w3-bar-item w3-circle w3-hide-large file-name-image" style="width:70px">
                        <div class="w3-bar-item">
                            <span class="w3-medium ${FILE_NAME_CLASS_NAME}">File name: <b><em>${file.publicName}</em></b></span>
                            <br>
                            <span class="w3-small file-description">File name: ${file.publicName}</span>
                        </div>
                    </li>
                `
    });
    newFilesListInnerHtml += `</ul></div>`
    return newFilesListInnerHtml;
}

const getPaginationButtonsHTML = () => {
    return `
        <div class="pagination">      
            <a id="previous-page">Previous</a>
            <a id="next-page">Next</a>
        </div>
    `
}

const showInvitations = () => {
    let request = net.request({
        method: 'GET',
        url: 'http://localhost:' + config.http_port + '/invitations'
    });
    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let invitations = JSON.parse(chunk.toString('utf-8'));
                let newInnerHTML =
                    invitations
                        .sort((a,b) => a.relatedTeamName.localeCompare(b.relatedTeamName))
                        .map(invitation => {
                            let wasNotDisplayedHTML = (invitation.wasDisplayed)
                                ? ``
                                : `<a class="new-element-tip" style="background-color: gold;">(new)</a>`;
                            return `<div data-id=\"${invitation.id}\" class="invitation-div">
                                    <a><b>${invitation.relatedTeamName}</b></a>
                                    <button class="accept-invitation-button">accept</button>
                                    <button class="reject-invitation-button">reject</button>
                                    <a>from user <b>${invitation.sender.name}</b></a>
                                    ${wasNotDisplayedHTML}
                                </div>`
                        })
                        .reduce((a,b) => a + b, ``)

                if (mainList.innerHTML !== newInnerHTML){
                    mainList.innerHTML = newInnerHTML;
                    addOnHoverChangeInvitationStyleListener()
                }
            })
        }
    })

    request.end();
};

const addOnHoverChangeInvitationStyleListener = () => {
    //console.log("HERE")
    let invitationElements = document.getElementsByClassName("invitation-div")
    //console.log(invitationElements)
    for (let element of invitationElements){
        //console.log(element.getAttribute("data-id"))
        element.addEventListener('mouseenter', () => {
            sendInvitationDisplayedEvent(element.getAttribute("data-id"))
            //element.querySelector(".new-element-tip").innerHTML=``
        })
    }
}

const sendInvitationDisplayedEvent = (invitationId) => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:${config.http_port}/invitationWasDisplayed?id=${invitationId}`
    });
    request.on('response', (response) => {
        if (response.statusCode === 200) {
            console.log("OK")
        } else{
            console.log("ERR")
        }
    });
    request.end();
}

const addUserToTeam = (teamName, userToAdd) => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/addUserToTeam?teamName=${teamName}&userToAdd=${userToAdd}`
    });
    request.on('response', (response) => {
        if (response.statusCode === 200) {
            //TODO add error showing
        }
    });
    request.end();
}

function showUserDetails(username){
    state = State.USER_DETAILS;
    let requestGetUserSummary = net.request({
        method: 'GET',
        url: `http://localhost:${config.http_port}/userSummary?userName=${username}`
    });

    requestGetUserSummary.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (userSummary) => {
                performShowUserDetails(JSON.parse(userSummary.toString('utf-8')));
            })
        } else {

        }
    })
    requestGetUserSummary.end();
}

const performShowUserDetails = (userSummary) => {
    addNewFileButton.style.display = 'none';
    //addUserToTeamButton.style.display = 'none';
    mainListHeader.innerHTML =
        `
            <h1 class="main-list-header">Details of user: <b>${currentlyClickedUser}</b></h1>
        `
    mainListLabel.style.display = 'none'


    let isActiveDot = (userSummary.active ? `<span class="user-online"></span>` : `<span class="user-offline"></span>`);
    console.log(userSummary.name)
    console.log(username)
    let firstDescriptionRow = (userSummary.name === username ? `<p><b>(You)</b></p>`
        : `<p>You cooperate in ${userSummary.numberOfTeamsCooperating} team(s)</p>`);


    let userDetailsHTML = `
        <div class="user-details">
            <img src="user.png" alt="Avatar">
            <div class="container">
                <h4>${isActiveDot}<b> ${userSummary.name}</b></h4> 
                ${firstDescriptionRow}
                <p><b>IPv4 Address</b>: ${userSummary.ipAddress}</p>
                <p><b>Port</b>: ${userSummary.port}</p> 
                <p><b>Type of connection</b>: ${userSummary.connectionType}</p> 
                <p><b>Local IPv4 Address</b>: ${userSummary.localAddress}</p>
                <p><b>Local UDP Address</b>: ${userSummary.localUdpPort}</p>
            </div>
        </div>
    `
    if (mainList.innerHTML !== userDetailsHTML){
        mainList.innerHTML = userDetailsHTML
    }
}

const showUsers = (teamDetails) => {
    let request = net.request({
        method: 'GET',
        url: 'http://localhost:' + config.http_port + '/usersList'
    });
    let userNamesInCurrentTeam =
        teamDetails
            .filter((team) => team.teamName === chosenTeam)
            .flatMap((team) => team.participants)
            .map(participant => participant.name)

    request.on('response', (response) => {
        if (response.statusCode === 200) {
            response.on('data', (chunk) => {

                let usersListHTML =
                    JSON.parse(chunk.toString('utf-8'))
                        .filter(user => ! userNamesInCurrentTeam.includes(user.name))//user is not in current team
                        /*.filter(user =>
                            usersListSearchInputBox.value === '' ?
                                true : user.name.includes(usersListSearchInputBox.value))*/
                        .map(user =>
                            (user.active ? `<span class="user-online"></span>` : `<span class="user-offline"></span>`)
                            + `<a>${user.name}</a>`
                            + (chosenTeam ?
                                `<button value="${user.name}" class="main-button">Add to team ${chosenTeam}</button>` : ``)
                            + `<br>`
                        )
                        .reduce((a,b) => a + b, ``)

                if (mainList.innerHTML !== usersListHTML) {
                    console.log(mainList.innerHTML);
                    mainList.innerHTML = usersListHTML;
                }
            })
        }
    })
    request.end();
}


const showSearchResults = () => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/teamSummaries`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let teamsSummary = JSON.parse(chunk.toString('utf-8'))
                console.log(teamsSummary)
            });
        }
    });
    request.end();
}

async function synchronizeLists() {
    while(true){
        let request = net.request({
            method: 'GET',
            url: `http://localhost:` + config.http_port + `/myTeamsDetails`
        });

        request.on('response', (response) => {
            if (response.statusCode === 200){
                response.on('data', (chunk) => {
                    let teamsDetails = JSON.parse(chunk.toString('utf-8'))
                    showTeams(teamsDetails);
                    showTeamsUsers(teamsDetails);
                    if (state === State.FILES){
                        showTeamsDetails(teamsDetails);
                    }
                    else if (state === State.INVITATIONS){
                        showInvitations();
                    }
                    else if (state === State.USERS_LIST){
                        showUsers(teamsDetails)
                    }
                    else if (state === State.USER_DETAILS){
                        showUserDetails(currentlyClickedUser)
                    }
                    else if (state === State.SEARCH_RESULTS){
                        showSearchResults()
                    }
                })
            }
        });
        request.end();
        await sleep(200);
    }
}

synchronizeLists();