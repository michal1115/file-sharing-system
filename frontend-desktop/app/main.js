const { app, BrowserWindow, dialog, net } = require('electron')
const fs = require('fs')
const exec = require("child_process").execSync;
const {spawn} = require("child_process")

const config = require('./config.json');

let window = null;
let secondaryWindow = null;

const changeView = exports.changeView = (path, params, onSecondaryWindow = false) => {
    let targetWindow = onSecondaryWindow ? secondaryWindow : window;

    let xPos = undefined
    let yPos = undefined
    if (window !== null) {
        xPos = window.getBounds().x + 20
        yPos = window.getBounds().y + 20
    }
    if (targetWindow == null){
        targetWindow = new BrowserWindow({
            show: false,
            resizable: false,
            webPreferences: {
                nodeIntegration: true
            },
            x: xPos,
            y: yPos
        })
    }


    //prepare parameter string var1=1&var2=32&hello=world
    let paramsString = '';
    if (params !== undefined){
        Object.keys(params)
            .forEach((key) => {
                paramsString =
                    paramsString + key.toString() + '=' + params[key].toString() + '&'
            })
    }
    paramsString = paramsString.slice(0, paramsString.length - 1);


    targetWindow.loadURL('file://' + path + `?` + paramsString);
    targetWindow.once('ready-to-show', () => {
        targetWindow.show();
    });

    targetWindow.on('closed', () => {
        if(onSecondaryWindow){
            secondaryWindow = null;
        }
        else{
            window = null;
        }
    });

    if(onSecondaryWindow){
        secondaryWindow = targetWindow;
    }
    else{
        window = targetWindow;
    }
}

const closeSecondaryWindow = exports.closeSecondaryWindow = () => {
    secondaryWindow.close();
}

const getFileFromUser = exports.getFileFromUser = (targetWindow) => {
    const files = dialog.showOpenDialog({
        properties: ['openFile']
    });

    if (!files){
        return;
    }
    return files
        .then(files => files['filePaths'][0])//get first file in list
        .then(file => ({
            name: file,
            content: fs.readFileSync(file, {encoding: 'utf-8'})
        }))
        .then(data => targetWindow.webContents.send('file-taken', data));
}

app.on('ready', () => {
    //console.log(check_if_command_executes_with_result("python --version", (result) => ))
    let python_command = get_python_command()
    let java_command = get_java_command()
    let http_port_status = get_application_run_status("tcp", config.http_port)
    let udp_port_status = get_application_run_status("tcp", config.http_port)

    if (python_command === undefined){
        console.log("Please install python")
        return
    }
    if (java_command === undefined){
        console.log("please install java")
        return
    }

    if (http_port_status === "IMPOSSIBLE_TO_START" || udp_port_status === "IMPOSSIBLE_TO_START"){
        console.log("Cannot start application, ports are occupied")
    }
    if (http_port_status === "NOT_RUNNING" && udp_port_status === "NOT_RUNNING"){
        let backend_jar_file_path = config.backend_jar
        let run_backend_command = `${java_command} -jar ${backend_jar_file_path}`
        spawn_backend_process(java_command, backend_jar_file_path)
    } else {
        establish_frontend()
    }
});

const get_python_command = () => {
    if (check_if_command_executes_with_result("python --version", is_python_version_3)){
        return "python"
    }
    if (check_if_command_executes_with_result("py --version", is_python_version_3)){
        return "py"
    }
    if (check_if_command_executes_with_result("python3 --version", is_python_version_3)){
        return "python3"
    }
}

const get_java_command = () => {
    if (check_if_command_executes_with_result("java -version")){
        return "java"
    }
}

const is_python_version_3 = (python_version_output) => {
    return python_version_output.toString()
        .replace("Python", "").trim()
        .startsWith("3")
}

const is_java_version_1_8 = (java_version_output) => {
    return java_version_output.toString()
        .replace("java", "").trim()
        .replace("version", "").trim()
        .replace("\"", "").trim()
        .startsWith("1.8")
}

const check_if_command_executes_with_result = (cmd, result_checker = (result) => true) => {
    try{
        let result = exec(cmd);
        return result_checker(result)
    } catch(error){
        return false
    }
}

const get_application_run_status = (proto, port) => {
    let tcp_command = `netstat -ano -p ${proto} |find \"${port}\"`
    try {
        let tcp_command_result = exec(tcp_command);
        let lines = tcp_command_result.toString().split("\n")
            .map(line => line.toString().replace(/\s\s+/g, ' '))
        let listening_connection = lines.filter(line => line.split(" ")[4] === "LISTENING")
        let pid = listening_connection[0].toString().split(" ")[5]
        return is_process_taken_by_java(pid)
    }catch (e) {
        return "NOT_RUNNING"
    }
}

const is_process_taken_by_java = (pid) => {
    let process_details_command = `tasklist |find \"${pid}\"`
    let get_command_status = exec(process_details_command);
    let spaces_trimmed = get_command_status.toString().replace(/\s\s+/g, ' ').split(" ")
    let process_name = spaces_trimmed[0]
    return process_name === "java.exe" ? "RUNNING" : "IMPOSSIBLE_TO_START"
}

const spawn_backend_process = (java_cmd, jar_file_path) => {
    const script = spawn(java_cmd, ["-jar", jar_file_path], {detached: true});

    script.stdout.on('data', (data) => {
        console.log(`${data}`);
        if (data.toString().includes("BACKEND_READY")){
            establish_frontend()
        }
    });

    script.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
    });

    script.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
}

const establish_frontend = () => {
    let req = net.request({
        method: "GET",
        url: `http://localhost:` + config.http_port + `/isLogged`
    })
    req.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let startingView = chunk.toString('utf-8') === 'true' ?
                    (__dirname + '/main-menu/main-menu.html') : (__dirname + '/login/login.html');
                changeView(startingView);
            })
        }
        else{
            console.log("Connection error! Client is not available")
        }
    })
    req.end()
}


