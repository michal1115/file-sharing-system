const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();

const config = require('../config.json');

const teamNameLabel = document.querySelector('#team-name-label');
const publicFileNameLabel = document.querySelector('#public-file-name-label');
const usersConnectionDetailsList = document.querySelector('#users-connection-details-list')
const sendFileButton = document.querySelector('#send-file-button')
const errorLabel = document.querySelector('#error-label');

const mainProcess = remote.require('./main.js');
const urlToParamDict = (url) =>
    url
        .split('?')[1]
        .split('&')
        .reduce((dict, param) => {
            let key = param.split('=')[0]
            let val = param.split('=')[1]

            dict[key] = val;
            return dict;
        }, {})

//get context from url:
let params = urlToParamDict(window.location.href);
const teamName = params.teamName
const filePublicName = params.filePublicName
const hidden = params.hidden
const sendButtonLabel = hidden === "true" ? "Send File With Steganography" : "Send File Without Steganography"

teamNameLabel.innerHTML = `Team: ${teamName}`
publicFileNameLabel.innerHTML = `File: ${filePublicName}`
sendFileButton.innerHTML = sendButtonLabel

const sendFileState = () => {
    let urlPath = 'sendFileState' + (hidden === "true" ? 'Hidden' : '');
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/${urlPath}?teamName=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200) {
            response.on('data', (chunk) => {
                mainProcess.closeSecondaryWindow();
            })
        }
    })
    request.end();
}

sendFileButton.addEventListener('click', () => {
    sendFileState();
})

const showTeamMembersConnectionDetails = (teamDetails) => {
    let request = net.request({
        method: 'GET',
        url: 'http://localhost:' + config.http_port + `/teamMembersConnectionDetails?teamName=${teamName}&isViaHiddenChannel=${hidden}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200) {
            response.on('data', (chunk) => {
                usersConnectionDetailsList.innerHTML = JSON.parse(chunk.toString('utf-8'))
                    .map(user => {
                        let isUserAccessibleDotIcon =
                            (user.accessible ? `<span class="user-online"></span>` : `<span class="user-offline"></span>`)
                        let usernameLabel = `<label style="color: rgb(0, 162, 232)">${user.username}</label>`
                        let userConnectionStatusDescriptionLabel = `<label style="color: white">: ${user.reason}</label>`
                        return `<div>
                                        ${isUserAccessibleDotIcon}
                                        ${usernameLabel}
                                        ${userConnectionStatusDescriptionLabel}
                                    </div>`
                    }).reduce((a, b) => a + b, ``);
            })
        }
    })
    request.end();
}
showTeamMembersConnectionDetails()