const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();

const config = require('../config.json');

const submitButton = document.querySelector('#submit-button');
const usernameTextBox = document.querySelector('#username-text-box');
const passwordTextBox = document.querySelector('#password-text-box');
const errorLabel = document.querySelector('#error-label');
const registerButton = document.querySelector('#register-button')

const mainProcess = remote.require('./main.js');

submitButton.addEventListener('click', () => {
    let username = usernameTextBox.value;
    let password = passwordTextBox.value;

    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/login?username=${username}&password=${password}`
    });


    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                console.log(chunk.toString('utf-8'));
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){
                    mainProcess.changeView(__dirname + '/../main-menu/main-menu.html')
                }
                else{
                    errorLabel.style.display = "block";
                    setTimeout(() => {
                        errorLabel.style.display = "none";
                    }, 3000);
                }
            })
        }
    })
    request.end();
})
registerButton.addEventListener('click', () => {
    mainProcess.changeView(__dirname + `/../register/register.html`);
})