const { remote, ipcRenderer } = require('electron')
const net = remote.net
const currentWindow = remote.getCurrentWindow();

const config = require('../config.json');

const usernameTextBox = document.querySelector('#username-text-box');
const emailTextBox = document.querySelector('#email-text-box');
const passwordTextBox = document.querySelector('#password-text-box');
const passwordConfirmTextBox = document.querySelector('#password-confirm-text-box');
const submitButton = document.querySelector('#submit-btn');
const gotoLoginButton = document.querySelector('#goto-login-btn');
const passwordsDoNotMatchLabel = document.querySelector('#passwords-do-not-match-label');
const registrationFailedLabel = document.querySelector('#registration-failed-label');

const mainProcess = remote.require('./main.js');

gotoLoginButton.addEventListener('click', () => {
    mainProcess.changeView(__dirname + `/../login/login.html`);
})

submitButton.addEventListener('click', () => {
    let username = usernameTextBox.value;
    let email = emailTextBox.value;
    let password = passwordTextBox.value;
    let passwordConfirm = passwordConfirmTextBox.value;

    if (password !== passwordConfirm){

        passwordsDoNotMatchLabel.style.display = 'block';
        setTimeout(() => {
            passwordsDoNotMatchLabel.style.display = "none";
        }, 3000);

        return;
    }

    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/register?username=${username}&password=${password}&email=${email}`
    });


    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                console.log(chunk.toString('utf-8'));
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){
                    mainProcess.changeView(__dirname + '/../main-menu/main-menu.html')
                }
                else{
                    registrationFailedLabel.style.display = "block";
                    setTimeout(() => {
                        registrationFailedLabel.style.display = "none";
                    }, 3000);
                }
            })
        }
    })
    request.end();
})




