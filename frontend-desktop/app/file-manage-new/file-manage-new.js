const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();
const mainProcess = remote.require('./main.js');

const config = require('../config.json');


const urlToParamDict = (url) =>
    url
    .split('?')[1]
    .split('&')
    .reduce((dict, param) => {
        let key = param.split('=')[0]
        let val = param.split('=')[1]

        dict[key] = val;
        return dict;
    }, {})

//get context from url:
let params = urlToParamDict(window.location.href);
const teamName = params.teamName

const backButton = document.querySelector('#back-btn');
const fileChooserButton = document.querySelector('#file-chooser-btn');
const publicFileNameInput = document.querySelector('#public-file-name-input');
const errorLabel = document.querySelector('#error-label')

backButton.addEventListener('click', () => {
    mainProcess.changeView(__dirname + '/../main-menu/main-menu.html', {
        teamName: teamName
    });
})

const createNewFile = ((localFilePath) => {
    localFilePath = btoa(localFilePath);
    let publicFileName = publicFileNameInput.value;

    if (localFilePath === '' || localFilePath === undefined || publicFileName === '' || publicFileName === undefined){
        errorLabel.innerHTML = `Cannot send empty data`;
        errorLabel.style.display = "block";
        setTimeout(() => {
            errorLabel.style.display = "none";
        }, 3000);
        return;
    }

    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/addFileToShared?localFilePath=${localFilePath}&fileName=${publicFileName}&teamName=${teamName}`
    });
    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                console.log(chunk.toString('utf-8'));
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){
                    mainProcess.changeView(__dirname + '/../file-manage/file-manage.html', {
                        teamName: teamName,
                        filePublicName: publicFileName
                    });
                }
                else{
                    errorLabel.style.display = "block";
                    setTimeout(() => {
                        errorLabel.style.display = "none";
                    }, 3000);
                }
            })
        }
    })
    request.end();
})

fileChooserButton.addEventListener('click', () => {
    mainProcess.getFileFromUser(currentWindow);
});

ipcRenderer.on('file-taken', (event, data) => {
    console.log(data)
    createNewFile(data.name);
});









