const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();

const config = require('../config.json');

const createTeamButton = document.querySelector('#create-team-button');
const teamNameTextBox = document.querySelector('#team-name-text-box');
const errorLabel = document.querySelector('#error-label');

const mainProcess = remote.require('./main.js');

createTeamButton.addEventListener('click', () => {
    let newTeamName = teamNameTextBox.value;
    if (newTeamName === ''){
        errorLabel.innerHTML = 'Team name cannot be empty';
        errorLabel.style.display = "block";
        setTimeout(() => {
            errorLabel.style.display = "none";
        }, 3000);
    }
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/createTeam?teamName=${newTeamName}`
    });


    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){
                    mainProcess.changeView(__dirname + '/../main-menu/main-menu.html')
                    mainProcess.closeSecondaryWindow();
                }
                else{
                    errorLabel.innerHTML = 'Team with given name cannot be created';
                    errorLabel.style.display = "block";
                    setTimeout(() => {
                        errorLabel.style.display = "none";
                    }, 3000);
                }
            })
        }
    })
    request.end();
})