const { remote, ipcRenderer } = require('electron')
const net = remote.net;
const currentWindow = remote.getCurrentWindow();
const mainProcess = remote.require('./main.js');
const config = require('../config.json');


const localFilePathInputBox = document.querySelector('#local-file-path-input');
const fileChooserButton = document.querySelector('#file-chooser-btn');
const publicFileNameLabel = document.querySelector('#public-file-name');
const usersList = document.querySelector('#users-list');
const backButton = document.querySelector('#back-btn');
const fileLocation = document.querySelector('#local-file-path');
const locateFileButton = document.querySelector('#locate-file-button');
const sendFileStateButton = document.querySelector('#send-file-state');
const sendFileStateHiddenButton = document.querySelector('#send-file-state-hidden');
const otherUsersTextField = document.querySelector('#others-users-text-field')
const myTextField = document.querySelector('#my-text-field')
const sendFileStateButton1 = document.querySelector('#send-file-state-1')
const sendFileStateButton2 = document.querySelector('#send-file-state-2')
const sendFileStateHidden1 = document.querySelector('#send-file-state-hidden-1')
const sendFileStateHidden2 = document.querySelector('#send-file-state-hidden-2')
const replaceFileVersionButton = document.querySelector('#replace-file-version-button')
const saveFileVersionButton = document.querySelector('#save-file-version-button')


const errorLabel = document.querySelector('#error-label');
const showError = (err) => {
    errorLabel.innerHTML = err;
    errorLabel.style.display = 'block';
    setTimeout(() => {
        errorLabel.style.display = 'none';
    }, 3000);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const urlToParamDict = (url) =>
    url
        .split('?')[1]
        .split('&')
        .reduce((dict, param) => {
            let key = param.split('=')[0]
            let val = param.split('=')[1]

            dict[key] = val;
            return dict;
        }, {})

//get context from url:
let params = urlToParamDict(window.location.href);
const teamName = params.teamName
const filePublicName = params.filePublicName


//check if file is located on users device. If not, show communicate instead of current location
let fileIsLocated = false;
let currentFileLocation = undefined;
let username = null;
let currentlyClickedUser = undefined

replaceFileVersionButton.addEventListener('click', (event) => {
    if (!fileIsLocated){
        showError("File is not located!")
        return
    }

    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/sendFileContent?teamname=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            myTextField.value = otherUsersTextField.value
            response.on('data', (chunk) => {
                myTextField.value = otherUsersTextField.value
            })
        } else{
            console.log("err")
        }
    });
    request.write(otherUsersTextField.value)
    request.end()
})

saveFileVersionButton.addEventListener('click', (event) => {
    if (!fileIsLocated){
        showError("File is not located!")
        return
    }
    saveCurrentMainTextFieldContent(() => {})
})

const saveCurrentMainTextFieldContent = (callback) => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/sendFileContent?teamname=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('end', () => {
                callback()
            })
            response.on('data', (chunk) => {
                //callback()
            })
        } else{
            console.log("err")
        }
    });
    request.write(myTextField.value)
    request.end()
}

const checkUsernameAndSynchronizeFileLocation = exports.getUsername = () => {
    if (username === null){
        let request = net.request({
            method: 'GET',
            url: `http://localhost:` + config.http_port + `/username`
        });

        request.on('response', (response) => {
            if (response.statusCode === 200){
                response.on('data', (chunk) => {
                    username = chunk.toString('utf-8')
                    synchronizeFileLocation();
                })
            }
        });
        request.end()
    }
}
const synchronizeFileLocation = () => {
    let requestFileLocations = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/myFileLocations`
    });
    requestFileLocations.on('response', (response) => {
        if(response.statusCode === 200){
            response.on('data', (chunk) => {
                let fileLocations = JSON.parse(chunk.toString('utf-8'))
                showIfFileIsLocated(fileLocations);
                synchronizeView();
                synchronizeFileContent()
            })
        }
    })
    requestFileLocations.end();
}
const showIfFileIsLocated = (fileLocations) => {
    let fileLocation = fileLocations
        .filter((fLoc) => fLoc.filePublicName === filePublicName)

    fileIsLocated = fileLocation.length !== 0;
    if (fileIsLocated){
        currentFileLocation = fileLocation[0].filePath;
    }
}

checkUsernameAndSynchronizeFileLocation();
let synchronizeFileContent = () => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/fileContent?username=${username}&teamname=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            let fullDownloadedData = '';
            response.on('end', () => {
                myTextField.value = fullDownloadedData;
            });
            response.on('data', (data) => {
                let dataAsString = data.toString('utf-8');
                fullDownloadedData += dataAsString
            });
        } else {
            myTextField.value = '';
        }
    })
    request.end();
}


const synchronizeView = () => {
    publicFileNameLabel.innerHTML =
        `
            <em><b>${filePublicName}</b></em>
        `

    if (fileIsLocated){
        fileLocation.innerHTML = currentFileLocation
    }
    else{
        fileLocation.innerHTML =
            `File is not located! All network utilities associated with this file are disabled!
            `
    }
}
//back to main menu:
backButton.addEventListener('click', () => {
    downloadCurrentFileContent((fileContent) =>
        handleMyTextFieldAndFileContentNotConsistent(fileContent, () => {
            mainProcess.changeView(__dirname + '/../main-menu/main-menu.html', {
                teamName: teamName
            });
        }))
})

fileChooserButton.addEventListener('click', () => {
    console.log("OK")
    mainProcess.getFileFromUser(currentWindow);
});
ipcRenderer.on('file-taken', (event, data) => {
    sendNewFileLocation(data.name);
});

const sendNewFileLocation = (fileLocation) => {
    if (fileLocation === '' || fileLocation === undefined){
        showError('New file path is empty!')
        return;
    }
    let filePathEncoded = btoa(fileLocation);


    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/locateFile?filePublicName=${filePublicName}&teamName=${teamName}&path=${filePathEncoded}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){
                    fileIsLocated = true;
                    currentFileLocation = fileLocation;
                    synchronizeView();
                    synchronizeFileContent();
                }
                else{
                    showError('Network error!')
                }
            })
        }
    })
    request.end();
}

const MAX_USERS_PER_PAGE = 3
let currentUsersPage = 0
const showUsers = (fileVersionDetails) => {
    //let usersListInnerHTML = `<li><a id="user-list-header">Replace file version from user:</a></li>`;

    /*let usersNames = teamsDetails
        .filter((teamDetail) => teamDetail.teamName === teamName)
        .flatMap((teamDetail) => teamDetail.participants)
        .map((participant) => participant.name)
        .sort((a,b) => a.localeCompare(b))
        .filter((name) => name !== username)*/

    let fileVersionDetailsSorted = fileVersionDetails
        .filter(detail => detail.ownerUsername !== username)
        .sort((a,b) => a.ownerUsername.localeCompare(b.ownerUsername))


    let usersCount = fileVersionDetailsSorted.length

    let usersListInnerHTML = ``
    if (usersCount > MAX_USERS_PER_PAGE && usersCount > MAX_USERS_PER_PAGE && currentUsersPage > 0){
        usersListInnerHTML += `<li><i id="previous-page" class="material-icons clickable-icon" style="font-size:36px">west</i></li>`;
    }
    for (let i = currentUsersPage; i < Math.min(currentUsersPage + MAX_USERS_PER_PAGE, usersCount); i++) {
        let fileVersionDetail = fileVersionDetailsSorted[i]
        usersListInnerHTML += (fileVersionDetail.wasVersionDisplayed)
            ? `<li><button class="send-btn user-entry">${fileVersionDetail.ownerUsername}</button></li>`
            : `<li><button style="background-color: gold" class="send-btn user-entry">${fileVersionDetail.ownerUsername}</button></li>`
    }
    if (usersCount > MAX_USERS_PER_PAGE && currentUsersPage < usersCount - MAX_USERS_PER_PAGE){
        usersListInnerHTML += `<li><i id="next-page" class="material-icons clickable-icon" style="font-size:36px">east</i></li>`;
    }

    if(usersList.innerHTML !== usersListInnerHTML) {
        usersList.innerHTML = usersListInnerHTML;
        if (usersCount > MAX_USERS_PER_PAGE && currentUsersPage > 0) {
            document.querySelector('#previous-page').addEventListener('click', (event) => {
                currentUsersPage -=1;
            });
        }
        if (usersCount > MAX_USERS_PER_PAGE && currentUsersPage < usersCount - MAX_USERS_PER_PAGE) {
            document.querySelector('#next-page').addEventListener('click', (event) => {
                currentUsersPage += 1
            })
        }

        let allUsersElements = usersList.getElementsByClassName('user-entry');
        //console.log(allUsersElements)
        for (let i = 0; i < allUsersElements.length; i++) {
            allUsersElements[i].addEventListener('click', (event) => {
                //TODO dodanie zmiany stylu buttona
                replaceFileVersionButton.style.display = "block"
                currentlyClickedUser = allUsersElements[i].innerHTML
                setUsersFileContentOnBottomTextField(allUsersElements[i].innerHTML)
            })
        }
    }
}

const setUsersFileContentOnBottomTextField = (username) =>{
    let request = net.request({
        method: 'GET',
        url: `http://localhost:${config.http_port}/fileContent?username=${username}&teamname=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            let fullDownloadedData = '';
            response.on('end', () => {
                otherUsersTextField.value = fullDownloadedData;
            });
            response.on('data', (data) => {
                let dataAsString = data.toString('utf-8');
                fullDownloadedData += dataAsString
            });
        } else {
            otherUsersTextField.value = '';
        }
    })
    request.end();
}

const sendFileState = (hidden) => {
    let urlPath = 'sendFileState' + (hidden ? 'Hidden' : '');
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/${urlPath}?teamName=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){

                }
                else{
                    showError('Cannot send file')
                }
            })
        }
    })
    request.end();
}

const handleClickOnSendNotHidden = () => {
    if (!fileIsLocated){
        showError("File is not located!")
        return
    }
    handleSendFileButtonClicked(false)
}

const handleClickOnSendHidden = () => {
    if (!fileIsLocated){
        showError("File is not located!")
        return
    }
    handleSendFileButtonClicked(true)
}


sendFileStateButton1.addEventListener('click', handleClickOnSendNotHidden)
sendFileStateButton2.addEventListener('click', handleClickOnSendNotHidden)
sendFileStateHidden1.addEventListener('click', handleClickOnSendHidden)
sendFileStateHidden2.addEventListener('click', handleClickOnSendHidden)

const handleSendFileButtonClicked = (sendViaHiddenChannel) => {
    downloadCurrentFileContent((fileContent) =>
        handleMyTextFieldAndFileContentNotConsistent(fileContent, () => showSendFileWindow(sendViaHiddenChannel)))
}

const downloadCurrentFileContent = (callback) => {
    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/fileContent?username=${username}&teamname=${teamName}&publicFileName=${filePublicName}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            let fullFileContent = '';
            response.on('end', function() {
                console.log(fullFileContent)
                console.log(fullFileContent.length)
                callback(fullFileContent)
            });
            response.on('data', function(data) {
                let dataAsString = data.toString('utf-8');
                fullFileContent += dataAsString
            });
        } else {
            let dataAsString = '';
            callback(dataAsString)
        }
    })
    request.end();
}

const handleMyTextFieldAndFileContentNotConsistent = (fileContent, callback) => {
    if (myTextField.value !== fileContent){
        if (confirm("You have unsaved file changes. Would you like to save them?")){
            saveCurrentMainTextFieldContent(callback)
        }else {
            callback()
        }
    } else {
        callback()
    }
}

const showSendFileWindow = (sendViaHiddenChannel) => {
    mainProcess.changeView(__dirname + `/../send-file-details/send-file-details.html`, {
        teamName: teamName,
        filePublicName: filePublicName,
        hidden: sendViaHiddenChannel
    }, true);
}

/*usersList.addEventListener('click', (event) => {
    if (['users-list', 'user-list-header'].includes(event.target.id)){
        //clicked element is users list header or background
        return;
    }
    console.log("ok")
    let selectedUser = event.target.innerHTML;

    let request = net.request({
        method: 'GET',
        url: `http://localhost:` + config.http_port + `/replaceFileVersion?teamName=${teamName}&publicFileName=${filePublicName}&otherUser=${selectedUser}`
    });

    request.on('response', (response) => {
        if (response.statusCode === 200){
            response.on('data', (chunk) => {
                let dataAsString = chunk.toString('utf-8');
                if (dataAsString === "true"){

                }
                else{

                }
            })
        }
    })
    request.end();

});*/




/*async function synchronize() {
    while(true){

        let requestTeamsDetails = net.request({
            method: 'GET',
            url: `http://localhost:` + config.http_port + `/myTeamsDetails`
        });
        requestTeamsDetails.on('response', (response) => {
            if (response.statusCode === 200){
                response.on('data', (chunk) => {
                    let teamsDetails = JSON.parse(chunk.toString('utf-8'))
                    showUsers(teamsDetails);
                    if (currentlyClickedUser !== undefined) {
                        setUsersFileContentOnBottomTextField(currentlyClickedUser)
                    }
                })
            }
        });

        requestTeamsDetails.end();
        await sleep(500);
    }
}*/
async function synchronize() {
    while(true){

        let requestTeamsDetails = net.request({
            method: 'GET',
            url: `http://localhost:` + config.http_port + `/fileDetails?teamName=${teamName}&fileName=${filePublicName}`
        });
        requestTeamsDetails.on('response', (response) => {
            if (response.statusCode === 200){
                response.on('data', (chunk) => {
                    let fileVersionsDetails = JSON.parse(chunk.toString('utf-8'))
                    showUsers(fileVersionsDetails);
                    if (currentlyClickedUser !== undefined) {
                        setUsersFileContentOnBottomTextField(currentlyClickedUser)
                    }
                })
            }
        });

        requestTeamsDetails.end();
        await sleep(500);
    }
}
synchronize();