from PIL import Image
import sys
file_input = sys.argv[1]
file_output = sys.argv[2]

format_map = {
    "jpg": "JPEG",
    "png": "png"
}

format_out = file_output.split(".")[-1] #get file extension

im = Image.open(file_input)

im.convert('RGB').save(file_output, format_map[format_out])
