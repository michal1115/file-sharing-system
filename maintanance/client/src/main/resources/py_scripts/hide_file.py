import sys
from utils import CryptoSteganography

password = sys.argv[1]
input_file = sys.argv[2]#steganography base image
output_file = sys.argv[3]
content_to_hide = open(sys.argv[4], "r+").read()

crypto = CryptoSteganography(password)
crypto.hide(input_file, output_file, content_to_hide)
