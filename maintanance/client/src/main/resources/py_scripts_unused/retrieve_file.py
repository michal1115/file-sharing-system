from cryptosteganography import CryptoSteganography
import sys
import os

password = sys.argv[1]
input_file = sys.argv[2]

output_file = sys.argv[3]
if os.path.exists(output_file):
  os.remove(output_file)

crypto = CryptoSteganography(password)
secret_msg = crypto.retrieve(input_file)


if secret_msg is not None:
    file = open(output_file, "w+")
    file.write(secret_msg)
    file.close()