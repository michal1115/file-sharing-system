package maintanance.client.proto.templates.messages;

public interface IMessage {

    public MessageType getType();
}
