package maintanance.client.proto.direct.messages;

import maintanance.client.proto.templates.messages.IMessage;

import java.util.Arrays;

public abstract class DirectMessage implements IMessage {

    public abstract byte[] toBytes();

    public static DirectMessage fromBytes(byte[] bytes){ return null; }

    static String getParameter(String metaData, String parameterName){
        int parameterValueStart = metaData.indexOf(parameterName) + parameterName.length() + 1;
        int parameterValueEnd = metaData.indexOf( "#", parameterValueStart);
        if (parameterValueStart == -1) System.out.println("THERE IS NO: " + parameterName);
        return metaData.substring(parameterValueStart, parameterValueEnd);
    }

    public static byte[] getFileContent(byte[] message){
        for (int i = 0; i < message.length - "#fileContent=".length(); i++){
            if (areFollowingBytesFileContentMarker(message, i)){
                return Arrays.copyOfRange(message, i + "#fileContent=".length(), message.length);
            }
        }
        return null;
    }

    private static boolean areFollowingBytesFileContentMarker(byte[] data, int startIndex){
        String cmp = "#fileContent=";
        for (int i = 0; i < cmp.length(); i++){
            if (! ( (char)data[startIndex + i] == cmp.charAt(i)) ){
                return false;
            }
        }
        return true;
    }
}
