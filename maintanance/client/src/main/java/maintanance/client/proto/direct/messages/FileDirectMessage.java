package maintanance.client.proto.direct.messages;

import lombok.*;
import maintanance.client.proto.templates.messages.IMessage;
import maintanance.client.proto.templates.messages.MessageType;

import javax.persistence.*;
import java.util.Arrays;

@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
public class FileDirectMessage extends DirectMessage {
    /**
     * Represents packet unique UUID.
     */
    private String packetUUID;

    /**
     * Represents if acknowledgement was received.
     */
    private Boolean isDelivered;

    /**
     * Represents sender of message (logged user when message was sent).
     */
    @ManyToOne
    private String sender;

    /**
     * Represents receiver of message.
     */
    @ManyToOne
    private String receiver;

    /**
     * Represents associated publicFile with this packet.
     */
    @ManyToOne
    private String publicFile;

    /**
     * Represents associated team with this packet.
     */
    @ManyToOne
    private String teamName;

    /**
     * Represents full file size.
     */
    private Integer fileSize;

    /**
     * Represents size of this packet.
     */
    private Integer packageSize;

    /**
     * Represents index of file part after dividing full file content to parts that can be sent via network.
     */
    private Integer packageIndex;

    private String securityKey;

    /**
     * Represents file content
     */
    @Lob
    private byte[] fileContent;

    public String getMetaData(){
        return "#msgType=FILE_PART"
                + "#packetUUID=" + packetUUID
                + "#sender=" + sender
                + "#receiver=" + receiver
                + "#publicFile=" + publicFile
                + "#teamName=" + teamName
                + "#fileSize=" + fileSize
                + "#packageSize=" + packageSize
                + "#packageIndex=" + packageIndex
                + "#securityKey=" + securityKey
                + "#fileContent=";
    }

    public byte[] getFileContent(){
        return fileContent;
    }

    public byte[] toBytes(){
        byte[] fullMessageContent = new byte[getMetaData().getBytes().length + getFileContent().length];
        System.arraycopy(getMetaData().getBytes(), 0, fullMessageContent, 0, getMetaData().getBytes().length);
        System.arraycopy(getFileContent(), 0, fullMessageContent, getMetaData().getBytes().length, getFileContent().length);
        return fullMessageContent;
    }

    public static FileDirectMessage fromBytes(byte[] bytes){
        String messageAsString = new String(bytes);
        return FileDirectMessage.builder()
                .packetUUID(getParameter(messageAsString, "packetUUID"))
                .sender(getParameter(messageAsString, "sender"))
                .receiver(getParameter(messageAsString, "receiver"))
                .publicFile(getParameter(messageAsString, "publicFile"))
                .teamName(getParameter(messageAsString, "teamName"))
                .fileSize(Integer.parseInt(getParameter(messageAsString, "fileSize")))
                .packageSize(Integer.parseInt(getParameter(messageAsString, "packageSize")))
                .packageIndex(Integer.parseInt(getParameter(messageAsString, "packageIndex")))
                .securityKey(getParameter(messageAsString, "securityKey"))
                .fileContent(getFileContent(bytes))
                .build();
    }

    public static void main(String[] args){
        FileDirectMessage message = FileDirectMessage.builder()
                .packetUUID("abc")
                .sender("send")
                .receiver("rec")
                .publicFile("file")
                .teamName("tm")
                .fileSize(12)
                .packageSize(56000)
                .packageIndex(0)
                .securityKey("hello")
                .fileContent("content".getBytes())
                .build();

        System.out.println(new String(message.toBytes()));
        System.out.println(FileDirectMessage.fromBytes(message.toBytes()));
    }

    @Override
    public MessageType getType() {
        return MessageType.FILE_CONTENT;
    }
}
