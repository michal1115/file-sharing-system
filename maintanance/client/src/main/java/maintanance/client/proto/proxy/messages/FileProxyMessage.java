package maintanance.client.proto.proxy.messages;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
public class FileProxyMessage {
    private String user;//sender when receiving, receiver when sending
    private String teamName;
    private String fileName;
}
