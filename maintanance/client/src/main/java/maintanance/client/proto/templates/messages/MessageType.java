package maintanance.client.proto.templates.messages;

public enum MessageType {
    FILE_CONTENT,
    KEY_EXCHANGE,
    ACKNOWLEDGEMENT
}
