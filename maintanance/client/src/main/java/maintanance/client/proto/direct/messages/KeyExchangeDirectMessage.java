package maintanance.client.proto.direct.messages;

import lombok.*;
import maintanance.client.proto.templates.messages.MessageType;

@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString
public class KeyExchangeDirectMessage extends DirectMessage{
    private String sender;
    private String receiver;
    private long publicKey;

    public byte[] toBytes(){
        String rawMessage = "#msgType=KEY_EXCHANGE"
                + "#sender=" + sender
                + "#receiver=" + receiver
                + "#publicKey=" + publicKey
                + "#";
        return rawMessage.getBytes();
    }

    public static KeyExchangeDirectMessage fromBytes(byte[] bytes){
        String messageAsString = new String(bytes);
        return KeyExchangeDirectMessage.builder()
                .sender(getParameter(messageAsString, "sender"))
                .receiver(getParameter(messageAsString, "receiver"))
                .publicKey(Long.parseLong(getParameter(messageAsString, "publicKey")))
                .build();
    }

    @Override
    public maintanance.client.proto.templates.messages.MessageType getType() {
        return MessageType.KEY_EXCHANGE;
    }
}
