package maintanance.client.proto.templates.synchronizers;

import maintanance.client.proto.templates.messages.IMessage;
import org.springframework.messaging.Message;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

/**
 * Interface for operations related with protocol for sharing files, contains
 * all methods that must be implemented to start participation in listening for upcoming files
 */
public interface ISynchronizer {

    /**
     * Send file normally
     * @param message
     */
    void send(IMessage message);

    /**
     * Send file hidden using stegano
     * @param message
     */
    void sendHidden(IMessage message);

    /**
     * Start listening for upcoming messages, should call 'next' after receiving and parsing message
     */
    void listenForMessages();

    /**
     * Get default listener that handles messages
     * @return
     */
    List<IMessage> getDefaultIncomingMessageListener();

    /**
     * Get secondary listeners for handling messages (those listeners should be treated as plugins)
     * @return
     */
    List<Consumer<IMessage>> getMessageListeners();

    /**
     * Register new message listener
     * @param messageListener
     */
    void registerMessageListener(Consumer<IMessage> messageListener);

    default void next(IMessage message){
        getMessageListeners().forEach(listener -> listener.accept(message));
    }
}
