package maintanance.client.proto.direct.messages;

public enum MessageType {
    FILE_CONTENT,
    ACKNOWLEDGEMENT,
    KEY_EXCHANGE
}
