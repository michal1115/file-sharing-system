package maintanance.client.cryptography.asymmetric;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ECCMessage {
    private static String ITEM_SEPARATOR = "\r\n";

    private int contentLength;
    private byte[] content;

    public ECCMessage(byte[] content){
        contentLength = content.length;
        this.content = content;
    }

    public byte[] toBytes(){
        int dataLength = getContentLengthIntegerLength() + ITEM_SEPARATOR.length() + contentLength;
        byte[] msg = new byte[dataLength];

        byte[] contentLengthLine = getContentLengthLine().getBytes();
        System.arraycopy(contentLengthLine, 0, msg, 0, contentLengthLine.length);
        System.arraycopy(content, 0, msg, contentLengthLine.length, content.length);
        return msg;
    }

    private String getContentLengthLine(){
        return String.valueOf(contentLength) + ITEM_SEPARATOR;
    }

    private int getContentLengthIntegerLength(){
        return String.valueOf(contentLength).length();
    }

    public static ECCMessage fromBytes(byte[] eccMessage){
        String msgAsString = new String(eccMessage);
        String contentLine = msgAsString.split("\r\n")[0];
        int contentLength = Integer.parseInt(contentLine);

        int startingContentPoint = contentLine.length() + ITEM_SEPARATOR.length();

        byte[] content = new byte[contentLength];
        System.arraycopy(eccMessage, startingContentPoint, content, 0, contentLength);
        return new ECCMessage(content);
    }

    public static void main(String[] args){
        ECCMessage eccMessage = new ECCMessage("123abc".getBytes());
        byte[] encoded = eccMessage.toBytes();
        ECCMessage eccMessage1 = ECCMessage.fromBytes(encoded);
        System.out.println(eccMessage1.getContentLength());
        System.out.println(new String(eccMessage1.getContent()));
    }
}
