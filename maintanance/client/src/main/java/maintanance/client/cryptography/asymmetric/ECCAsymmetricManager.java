package maintanance.client.cryptography.asymmetric;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.net.*;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Component
public class ECCAsymmetricManager {

    public ECCAsymmetricManager(){
        Security.addProvider(new BouncyCastleProvider());
    }

    public KeyPair generateKeyPair() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("ECDSA", BouncyCastleProvider.PROVIDER_NAME);
        generator.initialize(new ECGenParameterSpec("secp256r1"));
        return generator.generateKeyPair();
    }

    public String serializePublicKey(PublicKey publicKey){
        return Base64.getEncoder().encodeToString(publicKey.getEncoded());
    }

    public PublicKey deserializePublicKey(String encodedPublicKeyInBase64) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] encodedPublicKey = Base64.getDecoder().decode(encodedPublicKeyInBase64);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
        KeyFactory kf = KeyFactory.getInstance("ECDSA");
        return kf.generatePublic(spec);
    }

    public byte[] encryptMessage(byte[] message, PublicKey publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher encryptor = Cipher.getInstance("ECIES", BouncyCastleProvider.PROVIDER_NAME);
        encryptor.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encrypted =  encryptor.doFinal(message);
        return new ECCMessage(encrypted).toBytes();
    }

    public byte[] decryptMessage(byte[] message, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        ECCMessage eccMessage = ECCMessage.fromBytes(message);
        Cipher decryptor = Cipher.getInstance("ECIES", BouncyCastleProvider.PROVIDER_NAME);
        decryptor.init(Cipher.DECRYPT_MODE, privateKey);
        return decryptor.doFinal(eccMessage.getContent());
    }

    public static void main(String[] args) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, IllegalBlockSizeException, InterruptedException, IOException, BadPaddingException, NoSuchPaddingException, InvalidKeyException {
        ECCAsymmetricManager manager = new ECCAsymmetricManager();
        udpDecryptionTest(manager);
    }

    private static void udpDecryptionTest(ECCAsymmetricManager manager) throws IOException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, InterruptedException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
        KeyPair keyPair = manager.generateKeyPair();
        new Thread(() -> {
            try {
                listenAndDecrypt(manager, keyPair);
            } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | NoSuchProviderException | InvalidKeyException e) {
                e.printStackTrace();
            }
        }).start();
        Thread.sleep(1000);
        encryptAndSend(manager, keyPair);
    }

    private static void encryptAndSend(ECCAsymmetricManager manager, KeyPair keyPair) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException {
        DatagramSocket datagramSocket = new DatagramSocket(10001);
        byte[] snd = manager.encryptMessage("abce2131321cxzd".getBytes(), keyPair.getPublic());
        System.out.println(new String(snd));
        DatagramPacket packet = new DatagramPacket(snd, snd.length, InetAddress.getByName("localhost"), 10000);
        datagramSocket.send(packet);
    }

    private static void listenAndDecrypt(ECCAsymmetricManager manager, KeyPair keyPair) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException {
        DatagramSocket datagramSocket = new DatagramSocket(10000);
        byte[] recv = new byte[60000];
        DatagramPacket packet = new DatagramPacket(recv, recv.length);
        datagramSocket.receive(packet);
        String decrypted = new String(manager.decryptMessage(recv, keyPair.getPrivate()));
        System.out.println(decrypted);
    }
}
