package maintanance.client.model.repositories;

import maintanance.client.model.entities.FileVersion;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;

public interface TeamRepository extends NativeQueryExecutor, CrudRepository<Team, Integer> {
    @Query(value =  "SELECT t as team "
                    + "FROM Team t "
                    + "WHERE t.teamName = :teamName")
    Optional<Team> findByName(
            @Param("teamName") String teamName
    );

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "TEAM_INCIDENT_REPORTS");
        persistOtherUsersTableContent(dataSource, username, "TEAM_PARTICIPANTS");
        persistOtherUsersTableContent(dataSource, username, "TEAM");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException {
        persistCurrentUserTableContent(dataSource, username, "TEAM_INCIDENT_REPORTS");
        persistCurrentUserTableContent(dataSource, username, "TEAM_PARTICIPANTS");
        persistCurrentUserTableContent(dataSource, username, "TEAM");
    }
}
