package maintanance.client.model.repositories;

import maintanance.client.model.entities.User;
//import org.springframework.data.jpa.repository.JpaRepository;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface UserRepository extends NativeQueryExecutor, CrudRepository<User, Integer> {
    //we collect all users that do not have their exchange key calculated (reason might be package loss or
    //user has just logged in)
    @Query(value =  "SELECT u as user "     +
                    "FROM User as u "       +
                    "WHERE u.exchangeKey = 0 ")
    Collection<User> getUsersWithoutExchangeKey();

    @Query(value =  "SELECT u as user "     +
            "FROM User as u "       +
            "WHERE u.privateKey = 0 ")
    Collection<User> getUsersWithNonGeneratedPrivateKey();

    @Query(value =  "SELECT u as user "     +
            "FROM User as u "       +
            "WHERE u.exchangeKey <> 0 ")
    Collection<User> getUsersWithGeneratedExchangeKey();

    @Query(value =  "SELECT u as user "     +
                    "FROM User u "          +
                    "WHERE u.name = :name")
    Optional<User> findByName(
            @Param("name") String name
    );

    @Query( value = "SELECT u as user "     +
                    "FROM User u "          +
                    "WHERE u.name = :name AND u.ipAddr = :ipAddr")
    Optional<User> findByNameAddr(
            @Param("name") String name,
            @Param("ipAddr") String ipAddr);

    @Query( value = "SELECT u.exchangeKey as user "     +
                    "FROM User u "                      +
                    "WHERE u.name = :name")
    Optional<Long> getExchangeKeyByName(
            @Param("name") String name
    );

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "USER");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException{
        persistCurrentUserTableContent(dataSource, username, "USER");
    }
}
