package maintanance.client.model.repositories;

import maintanance.client.model.entities.File;
import maintanance.client.model.entities.FileLocationCache;
import maintanance.client.model.entities.SentPacket;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

public interface FileLocationCacheRepository extends NativeQueryExecutor, CrudRepository<FileLocationCache, Integer> {
    @Query(value = "SELECT f "
            + "FROM FileLocationCache f "
            + "WHERE f.username = :ownerName")
    Collection<FileLocationCache> findByOwner(
            @Param("ownerName") String ownerName
    );

    @Query(value = "SELECT f "
            + "FROM FileLocationCache f "
            + "WHERE f.username = :ownerName "
            + "AND f.teamName = :teamName "
            + "AND f.filePublicName = :filePublicName")
    Optional<FileLocationCache> findByOwnerFilePublicNameAndTeam(
            @Param("ownerName") String ownerName,
            @Param("teamName") String teamName,
            @Param("filePublicName") String filePublicName
    );
}
