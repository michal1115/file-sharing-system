package maintanance.client.model.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

/**
 * Class represents all the data needed to send packet again when there was no acknowledgement from receiver. When
 * acknowledgement is received, isReceived attribute is being set to true and given packet is no more being sent.
 * IMPORTANT: Whenever we decide to add new content to packets, sentPacket must contain it
 * After isReceived is set to true, those objects should be deleted from db because they store whole file content
 */
@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
public class SentPacket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * Represents packet unique UUID.
     */
    @Column(unique = true)
    private String packetUUID;

    /**
     * Represents if acknowledgement was received.
     */
    private Boolean isDelivered;

    /**
     * Represents sender of message (logged user when message was sent).
     */
    @ManyToOne
    private User sender;

    /**
     * Represents receiver of message.
     */
    @ManyToOne
    private User receiver;

    /**
     * Represents associated publicFile with this packet.
     */
    @ManyToOne
    private File publicFile;

    /**
     * Represents associated team with this packet.
     */
    @ManyToOne
    private Team team;

    /**
     * Represents full file size.
     */
    private Integer fileSize;

    /**
     * Represents size of file content in this packet.
     */
    private Integer packageFileContentSize;

    /**
     * Represents index of file part after dividing full file content to parts that can be sent via network.
     */
    private Integer packageIndex;

    private String securityKey;

    /**
     * Represents file content
     */
    @Lob
    private byte[] fileContent;

    public String getMetaData(){
        return "#msgType=FILE_PART"
                + "#packetUUID=" + packetUUID
                + "#username=" + sender.getName()
                + "#receiver=" + receiver.getName()
                + "#fileName=" + publicFile.getPublicName()
                + "#teamName=" + team.getTeamName()
                + "#fileSize=" + fileSize
                + "#packageFileContentSize=" + packageFileContentSize
                + "#packageIndex=" + packageIndex
                + "#securityKey=" + securityKey
                + "#fileContent=";
    }

    public byte[] getFileContent(){
        return fileContent;
    }

    public byte[] getBytes(){
        byte[] fullMessageContent = new byte[getMetaData().getBytes().length + getFileContent().length];
        System.arraycopy(getMetaData().getBytes(), 0, fullMessageContent, 0, getMetaData().getBytes().length);
        System.arraycopy(getFileContent(), 0, fullMessageContent, getMetaData().getBytes().length, getFileContent().length);
        return fullMessageContent;
    }
}
