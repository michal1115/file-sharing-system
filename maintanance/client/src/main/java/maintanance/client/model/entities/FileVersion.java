package maintanance.client.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@Builder
@Entity
//@Table(uniqueConstraints={
//        @UniqueConstraint(columnNames = {"related_file_id", "version_owner_id"})
//})
public class FileVersion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String filePath;

    @ManyToOne
    private File relatedFile;

    @ManyToOne
    private User versionOwner;

    private boolean wasDisplayed = true;

    public FileVersion(String filePath, File relatedFile, User versionOwner){
        this.filePath = filePath;

        this.relatedFile = relatedFile;
        relatedFile.getFileVersions().add(this);

        this.versionOwner = versionOwner;
        versionOwner.getRelatedFileVersion().add(this);
    }

    public FileVersion(){}
}
