package maintanance.client.model.repositories;

import maintanance.client.model.entities.ReceivedPacket;
import maintanance.client.model.entities.SentPacket;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface ReceivedPacketRepository extends NativeQueryExecutor, CrudRepository<ReceivedPacket, Integer> {
    @Query(value = "SELECT rp "
            + "FROM ReceivedPacket rp "
            + "WHERE rp.isAckDelivered = false")
    Collection<ReceivedPacket> findNotAcknowledged();

    @Query(value = "SELECT rp "
            + "FROM ReceivedPacket rp "
            + "WHERE rp.packetUUID = :packetUUID")
    Optional<ReceivedPacket> findByUUID(@Param("packetUUID") String packetUUID);

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "RECEIVED_PACKET");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException{
        persistCurrentUserTableContent(dataSource, username, "RECEIVED_PACKET");
    }
}
