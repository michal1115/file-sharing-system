package maintanance.client.model.repositories;

import maintanance.client.model.entities.File;
//import org.springframework.data.jpa.repository.JpaRepository;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface FileRepository extends NativeQueryExecutor, CrudRepository<File, Integer> {
    @Query(value =  "SELECT f "
            + "FROM File f "
            + "WHERE f.publicName = :publicName AND f.connectedTeam = :team")
    Optional<File> findByNameAndTeam(
            @Param("publicName") String publicName,
            @Param("team") Team team
    );

    @Query(value = "SELECT f "
            + "FROM File f "
            + "WHERE f.publicName = :publicName AND f.connectedTeam.teamName = :teamName")
    Optional<File> findByPublicNameAndTeamName(
            @Param("publicName") String publicName,
            @Param("teamName") String teamName
    );

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "FILE");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException{
        persistCurrentUserTableContent(dataSource, username, "FILE");
    }
}
