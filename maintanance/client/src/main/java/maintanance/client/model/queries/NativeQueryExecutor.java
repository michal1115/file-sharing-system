package maintanance.client.model.queries;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.text.MessageFormat;

public interface NativeQueryExecutor {

    default void executeQuery(DataSource dataSource, String query) throws SQLException {
        dataSource.getConnection().prepareStatement(query).executeUpdate();
    }

    default void persistOtherUsersTableContent(DataSource dataSource, String username, String tableName) throws SQLException {
        try{
            String dropTableQuery = MessageFormat.format("DROP TABLE {1}_{0};", username, tableName);
            System.out.println(dropTableQuery);
            executeQuery(dataSource, dropTableQuery);
            System.out.println("OKS");
        } catch (SQLException ignoredThrownIfTableDidNotExist){
        }
        String createTableCopyQuery = MessageFormat.format("CREATE TABLE {1}_{0} as SELECT * from {1};", username, tableName);
        executeQuery(dataSource, createTableCopyQuery);
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username, String tableName) throws SQLException{
        String copyDataFromUsersBackupToTable = MessageFormat.format("SELECT * INTO {1} FROM {1}_{0};", username, tableName);
        executeQuery(dataSource, copyDataFromUsersBackupToTable);
    }
}
