package maintanance.client.model.repositories;

import maintanance.client.model.entities.FileVersion;
//import org.springframework.data.jpa.repository.JpaRepository;
import maintanance.client.model.entities.User;
import maintanance.client.model.entities.File;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;


@Repository
public interface FileVersionRepository extends NativeQueryExecutor, CrudRepository<FileVersion, Integer> {
    @Query(value =  "SELECT fv as file_version "    +
                    "FROM FileVersion fv "          +
                    "WHERE fv.relatedFile = :file " +
                    "AND fv.versionOwner = :user ")
    Optional<FileVersion> findFileVersionByUserAndPublicFile(
            @Param(value="file") File publicFile,
            @Param(value="user") User relatesUser
    );

    @Query(value =  "SELECT fv as file_version "                            +
                    "FROM FileVersion fv "                                  +
                    "WHERE fv.relatedFile.publicName = :fileName "          +
                    "AND fv.versionOwner.name = :userName "                 +
                    "AND fv.relatedFile.connectedTeam.teamName = :teamName")
    Optional<FileVersion> findUsersFileVersion(
            @Param(value="fileName") String fileName,
            @Param(value="userName") String userName,
            @Param(value="teamName") String teamName
    );

    @Query(value =  "SELECT fv as file_version " +
                    "FROM FileVersion fv " +
                    "WHERE fv.relatedFile = :file ")
    Collection<FileVersion> findAllFileVersionsByFile(
            @Param(value="file") File file);

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "FILE_VERSION");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException{
        persistCurrentUserTableContent(dataSource, username, "FILE_VERSION");
    }
}
