package maintanance.client.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Entity
@Setter
@Getter
public class File {
    @Id
    private Integer id;

    private String publicName;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Team connectedTeam;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedFile", fetch = FetchType.EAGER)
    private Set<FileVersion> fileVersions = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "publicFile", fetch = FetchType.EAGER)
    private Set<SentPacket> packetsConnected = new HashSet<>();

    public File(String publicName, Team connectedTeam){
        this.publicName = publicName;
        this.connectedTeam = connectedTeam;
    }

    public File(){}

    public Optional<FileVersion> getFileVersionByUsername(String username){
        return fileVersions.stream()
                .filter(fileVersion -> fileVersion.getVersionOwner().getName().equals(username))
                .findFirst();
    }
}
