package maintanance.client.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
public class Team {
    @Id
    private Integer id;

    @NonNull
    private String teamName;

    private String securityKey;

    //this key is still valid, as long as new key wont be generated to prevent situations when user A didnt receive new key
    //and user B gets terminated key
    private String previousSecurityKey;

    @OneToMany(mappedBy = "connectedTeam", fetch = FetchType.EAGER)
    private Set<File> sharedFiles = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> participants = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "team", fetch = FetchType.EAGER)
    private Set<SentPacket> packetsConnected = new HashSet<>();

    /*@JsonIgnore
    @OneToMany(mappedBy = "relatedTeam")
    private Set<Invitation> relatedInvitations = new HashSet<>();*/

    @ElementCollection
    @Builder.Default
    private Set<String> incidentReports = new HashSet<>();

    public Team(String teamName){
        this.teamName = teamName;
    }

    public Team(){
    }

    public void addSharedFile(File file){
        sharedFiles.add(file);
    }

    public void addParticipant(User user){
        this.participants.add(user);
    }

}
