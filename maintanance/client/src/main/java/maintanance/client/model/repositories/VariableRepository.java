package maintanance.client.model.repositories;

import maintanance.client.model.entities.User;
import maintanance.client.model.entities.Variable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

public interface VariableRepository extends CrudRepository<Variable, Integer> {

    @Query(value =  "SELECT v as variable "     +
            "FROM Variable as v "       +
            "WHERE v.key = :key")
    Optional<Variable> findByKey(@Param("key") String key);
}
