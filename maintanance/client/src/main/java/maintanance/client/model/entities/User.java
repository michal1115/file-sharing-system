package maintanance.client.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import maintanance.client.utils.ConnectionType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
@Entity
public class User {
    @Id
    private Integer id;

    private String name;

    private String ipAddr;
    private int port;

    private String localAddress;
    private Integer localUdpPort;

    private boolean active;

    private ConnectionType connectionType;

    //every pair of users have different keys
    private long privateKey;//for every user, different private key is used in case of security
    private long exchangeKey;//public exchange key calculated after getting another users public key

    private String cryptoPublicKey;//used to send data to other client

    @JsonIgnore
    @ManyToMany(mappedBy = "participants", fetch = FetchType.EAGER)
    private Set<Team> teams = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "versionOwner", fetch = FetchType.EAGER)
    private Set<FileVersion> relatedFileVersion  = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "sender", fetch = FetchType.EAGER)
    private Set<SentPacket> packetsReceivedByThis = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "receiver", fetch = FetchType.EAGER)
    private Set<SentPacket> packetsSentToThis = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "sender", fetch = FetchType.EAGER)
    private Set<ReceivedPacket> packetsSentByThis = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "receiver", fetch = FetchType.EAGER)
    private Set<Invitation> invitationsReceived = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "sender", fetch = FetchType.EAGER)
    private Set<Invitation> invitationsSent = new HashSet<>();

    public User(String name, String ipAddr, int port) {
        this.name = name;
        this.ipAddr = ipAddr;
        this.port = port;
    }

    public User(){}

    public void addTeam(Team team){
        teams.add(team);
    }

    public boolean wasPublicKeyNegotiated(){
        return exchangeKey != 0;
    }
}
