package maintanance.client.model.repositories;

import maintanance.client.model.entities.File;
import maintanance.client.model.entities.FileVersion;
import maintanance.client.model.entities.Invitation;
import maintanance.client.model.entities.User;
import maintanance.client.model.queries.NativeQueryExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;

public interface InvitationRepository extends NativeQueryExecutor, CrudRepository<Invitation, Integer> {
    @Query(value =  "SELECT i as invitation "    +
            "FROM Invitation i "          +
            "WHERE i.receiver.name = :receiverName " +
            "AND i.relatedTeamName = :teamName ")
    Optional<Invitation> findInvitationByReceiverNameAndTeamName(
            @Param(value="receiverName") String receiverName,
            @Param(value="teamName") String teamName
    );

    default void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        persistOtherUsersTableContent(dataSource, username, "INVITATION");
    }

    default void persistCurrentUserTableContent(DataSource dataSource, String username) throws SQLException{
        persistCurrentUserTableContent(dataSource, username, "INVITATION");
    }
}
