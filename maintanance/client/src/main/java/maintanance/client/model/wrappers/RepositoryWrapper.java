package maintanance.client.model.wrappers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import maintanance.client.model.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class purpose is to pass all database access to class that requires it
 */

@Component
@Getter
@Setter
public class RepositoryWrapper {
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private FileVersionRepository fileVersionRepository;
    @Autowired
    private ReceivedPacketRepository receivedPacketRepository;
    @Autowired
    private SentPacketRepository sentPacketRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InvitationRepository invitationRepository;
}
