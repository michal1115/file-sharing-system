package maintanance.client.model.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Class represents packets that were received, but not acknowledged. When user logs in, information about those packets
 * is stored in database. When packet is received, it contains packetUUID. This packet uuid is stored in this object.
 * When acknowledgement is sent, isAckDelivered attribute is set to true. When message is received, it's uuid is retrieved and
 * new object is created or isAckDelivered is set to false (that situation means that acknowledgement was lost and sender decided
 * to send packet again). Here comes another purpose of this object - preventing new FilePart creation when receiver
 * obtains same packet again (when received packet's uuid is in database, filePart creation is prevented)
 */
@Entity
@Getter
@Setter
public class ReceivedPacket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * Represents packetUUID.
     */
    @Column(unique = true)
    private String packetUUID;

    @ManyToOne
    private User sender;

    /**
     * Provides information about whenever acknowledgement was delivered.
     */
    private Boolean isAckDelivered;

    public ReceivedPacket(){}

    public ReceivedPacket(String packetUUID, boolean isSent, User sender){
        this.packetUUID = packetUUID;
        this.isAckDelivered = isSent;
        this.sender = sender;
    }
}
