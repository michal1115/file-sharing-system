package maintanance.client.model.queries;

import org.h2.mvstore.db.TransactionStore;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.sql.SQLException;

public class UserRepositoryQueries {

    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    @Transactional
    public void persistOtherUsersTableContent(DataSource dataSource, String username) throws SQLException {
        /*EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        int res = entityManager.createNativeQuery("CREATE TABLE user_" + username +" as SELECT * from user;").executeUpdate();
        transaction.commit();
        System.out.println(res);*/
        //dataSource.getConnection();
        try{
            dataSource.getConnection().prepareStatement("DROP TABLE user_" + username).executeUpdate();
        } catch (SQLException ignoredThrownIfTableDidNotExist){
        }
        dataSource.getConnection().prepareStatement("CREATE TABLE user_" + username +" as SELECT * from USER;").executeUpdate();
        //ScriptUtils.executeSqlScript(dataSource.getConnection(), "CREATE TABLE user_" + username +" as SELECT * from user;");
    }

}
