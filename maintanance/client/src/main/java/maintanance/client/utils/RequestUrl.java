package maintanance.client.utils;

import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.Map;

public class RequestUrl {
    private String ipAddr = "";
    private String port = "";
    private String path = "";
    private Map<String, String> params = new HashMap<>();

    @SneakyThrows
    public String toString(){
        StringBuilder urlBuilder = new StringBuilder();

        if (ipAddr.isEmpty() ^ port.isEmpty()){
            throw new Exception();
        }
        if (! ipAddr.isEmpty()){
            urlBuilder.append("http://");
            urlBuilder.append(ipAddr);
            urlBuilder.append(":");
            urlBuilder.append(port);
        }
        urlBuilder.append(path);
        if (!params.isEmpty()){
            urlBuilder.append("?");
            params.forEach((key, value) -> {
                urlBuilder.append(key);
                urlBuilder.append("=");
                urlBuilder.append(value);
                urlBuilder.append("&");
            });
            urlBuilder.deleteCharAt(urlBuilder.length() - 1); //remove leading & in url
        }
        return urlBuilder.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder{
        private String ipAddr = "";
        private String port = "";
        private String path = "";
        private Map<String, String> params = new HashMap<>();

        public Builder ipAddr(String ipAddr){
            this.ipAddr = ipAddr;
            return this;
        }

        public Builder port(String port){
            this.port = port;
            return this;
        }

        public Builder path(String path){
            this.path = path;
            return this;
        }

        public Builder param(String key, String value){
            this.params.put(key, value);
            return this;
        }

        public RequestUrl build(){
            RequestUrl requestUrl = new RequestUrl();
            requestUrl.ipAddr = this.ipAddr;
            requestUrl.port = this.port;
            requestUrl.path = this.path;
            requestUrl.params.putAll(this.params);


            this.ipAddr = "";
            this.port = "";
            this.path = "";
            this.params.clear();


            return requestUrl;
        }

    }
}
