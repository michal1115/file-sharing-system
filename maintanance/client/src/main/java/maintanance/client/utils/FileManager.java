package maintanance.client.utils;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;


public class FileManager {

    public static byte[] getFileContent(String filePath) throws IOException {
        return Files.readAllBytes(Paths.get(filePath));
    }

    public static boolean writeToFile (String filePath, String fileContent) throws IOException {
        byte[] buffer = fileContent.getBytes();

        FileOutputStream outputStream = new FileOutputStream(filePath);
        outputStream.write(buffer);
        outputStream.close();

        return true;
    }

    public static boolean writeToFile (String filePath, byte[] buffer) throws IOException {

        FileOutputStream outputStream = new FileOutputStream(filePath);
        System.out.println("#writeToFile");
        System.out.println(filePath);
        System.out.println(outputStream);
        System.out.println(buffer);
        outputStream.write(buffer);
        outputStream.close();

        return true;
    }

    public static void main(String[] args) throws IOException {
    }
}