package maintanance.client.utils;

public enum ConnectionType {
    PROXY,
    DIRECT,
    HOLE_PUNCHING
}
