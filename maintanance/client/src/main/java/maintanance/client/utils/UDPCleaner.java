package maintanance.client.utils;

public class UDPCleaner {

    public static byte[] cleanPacket(byte[] received){
        System.out.println("CLEANING UDP");
        System.out.println(new String(received));
        System.out.println();
        int lastUsefulByte = received.length - 1;
        while (lastUsefulByte >= 0 && received[lastUsefulByte] == 0){
            lastUsefulByte --;
        }
        System.out.println(lastUsefulByte);
        byte[] onlyUsefulBytes = new byte[lastUsefulByte + 1];
        for (int i = 0; i <= lastUsefulByte; i++){
            onlyUsefulBytes[i] = received[i];
        }

        System.out.println("AFTER CLEANING UDP");
        System.out.println(new String(onlyUsefulBytes));
        System.out.println();
        return received;
    }
}
