package maintanance.client.utils;

import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.utils.DynamicPropertiesManager;
import okhttp3.*;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SocketHttpRequest {
    public static String ERROR_REASON_HEADER_KEY = "error-reason";
    public static String jwt;
    private static final Semaphore socketAccessSemaphore = new Semaphore(1);
    private static DynamicPropertiesManager properties;
    static {
        try {
            properties = new DynamicPropertiesManager("dynamic.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String sendRequestAuthorized(String path, String jwt, Socket socket) throws IOException {
        return sendRequestAuthorized(path, jwt, "".getBytes(), socket);
    }

    public static void setJwt(String jwt){
        System.out.println("is set");
        System.out.println(jwt);
        SocketHttpRequest.jwt = jwt;
    }

    public static String sendRequestAuthorized(String path, Socket socket) throws IOException {
        if (jwt == null){
            throw new RuntimeException("JWT IS NULL");
        }
        //System.out.println(jwt);
        return sendRequestAuthorized(path, jwt, "".getBytes(), socket);
    }

    public static String sendRequestAuthorized(String path, String jwt, byte[] data, Socket socket) throws IOException{
        //TODO przeczytać coś o tym jak formułuje się requesty HTTP, bo to jest nieporozumienie, żeby słać całe to coś za każdym razem XD
        boolean debug = false;
        if (debug) {
            System.out.println("DEBUG:");
            System.out.println(path);
            System.out.println();
        }
        String messageTitleAndHeaders = "GET " + path + " HTTP/1.0" + "\r\n"
                + "Content-Length: " + data.length + "\r\n"
                + "Content-Type: application/binary\r\n"
                + "Authorization: Bearer " + jwt + "\r\n"
                + "\r\n";
        //System.out.println(messageTitleAndHeaders);

        DataOutputStream wr = new DataOutputStream(socket.getOutputStream());
        wr.write(messageTitleAndHeaders.getBytes());
        wr.write(data);
        /*wr.write(("GET " + path + " HTTP/1.0" + "\r\n").getBytes());
        //wr.write("Connection: keep-alive\r\n");
        wr.write(("Content-Length: " + data.length + "\r\n").getBytes());
        wr.write("Content-Type: application/binary\r\n".getBytes());
        wr.write(("Authorization: Bearer " + jwt + "\r\n").getBytes());
        wr.write("\r\n".getBytes());*/
        //wr.write(data);

        wr.flush();

        BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        StringBuilder resultBuilder = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            //System.out.println("OK");
            resultBuilder.append(line).append("\n");
        }
        //System.out.println(resultBuilder.toString());
        return resultBuilder.toString();
    }

    public static String sendRequest(String path, Socket socket) throws IOException{
        return sendRequest(path, "".getBytes(), socket);
    }

    public static String sendRequest(String path, byte[] data, Socket socket) throws IOException {
        //TODO przeczytać coś o tym jak formułuje się requesty HTTP, bo to jest nieporozumienie, żeby słać całe to coś za każdym razem XD
        boolean debug = false;
        if (debug) {
            System.out.println("DEBUG:");
            System.out.println(path);
            System.out.println();
        }

        DataOutputStream wr = new DataOutputStream(socket.getOutputStream());

        wr.write(("GET " + path + " HTTP/1.0" + "\r\n").getBytes());
        //wr.write("Connection: keep-alive\r\n");
        wr.write(("Content-Length: " + data.length + "\r\n").getBytes());
        wr.write(("Content-Type: application/json\r\n").getBytes());
        if (jwt != null){
            wr.write(("Authorization: Bearer " + jwt + "\r\n").getBytes());
        }
        wr.write("\r\n".getBytes());
        wr.write(data);

        wr.flush();

        BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        StringBuilder resultBuilder = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            resultBuilder.append(line).append("\n");
        }

        return resultBuilder.toString();
    }

    public static byte[] sendRequestGetByteArray(String path, Socket socket) throws IOException{
        return sendRequestGetByteArray(path, "", socket);
    }

    public static byte[] sendRequestGetByteArray(String path, String data, Socket socket) throws IOException {
        //TODO przeczytać coś o tym jak formułuje się requesty HTTP, bo to jest nieporozumienie, żeby słać całe to coś za każdym razem XD
        boolean debug = false;
        if (debug) {
            System.out.println("DEBUG:");
            System.out.println(path);
            System.out.println();
        }

        BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
        wr.write("GET " + path + " HTTP/1.0" + "\r\n");
        //wr.write("Connection: keep-alive\r\n");
        wr.write("Content-Length: " + data.length() + "\r\n");
        wr.write("Content-Type: application/json\r\n");
        if (jwt != null){
            wr.write("Authorization: Bearer " + jwt + "\r\n");
        }
        wr.write("\r\n");
        wr.write(data);

        wr.flush();

        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

        List<Byte> readBytes = new LinkedList<Byte>();
        byte[] chunk = new byte[65000];
        while (dataInputStream.read(chunk) != -1){
            for (int i = 0; i < chunk.length; i++){
                readBytes.add(chunk[i]);
            }
            System.out.println("#sendRequestGetByteArray");
            System.out.println(new String(chunk));
            System.out.println("---------------------------------------------------------------------------------------");
            chunk = new byte[65000];
        }

        byte[] result = new byte[readBytes.size()];
        for (int i = 0; i < readBytes.size(); i++){
            result[i] = readBytes.get(i);
        }
        return result;
    }

    public static Socket openServerSocket() throws IOException, InterruptedException {
        socketAccessSemaphore.acquire();
        InetAddress serverAddress
                = InetAddress.getByName(properties.getProperty("serverAddr"));
        Integer serverPort = Integer.parseInt(properties.getProperty("serverPort"));

        boolean done = false;
        Socket socket = null;
        while (!done) {
            try {
                socket = new Socket();
                socket.bind(new InetSocketAddress(Integer.parseInt(properties.getProperty("localPort"))));
                socket.connect(new InetSocketAddress(serverAddress, serverPort));
                done = true;
            } catch (Exception e) {
                Thread.sleep(200);
            }
        }
        return socket;
    }

    public static void closeServerSocket(Socket socket) throws IOException {
        socket.close();
        socketAccessSemaphore.release();
    }

    public static int getCode(String httpMessage){
        return Integer.parseInt(httpMessage.split(" ")[1]);
    }

    public static String removeHttpHeader(String httpMessage){
        //return httpMessage.split("\n")[httpMessage.split("\n").length - 1];
        String[] headersAndBody = httpMessage.split("\n\n");
        return headersAndBody.length > 1 ? removeLastLineBreak(headersAndBody[1]) : null;
    }

    public static StatusMessage getRequestStatus(String httpMessage){
        return Arrays.stream(removeHttpBody(httpMessage).split("\n"))
                .filter(line -> line.startsWith(ERROR_REASON_HEADER_KEY))
                .map(SocketHttpRequest::getValue)
                .findFirst()
                .map(StatusMessage::valueOf).orElse(StatusMessage.OK);
    }

    public static Optional<String> getHeaderValue(String httpMessage, String header){
        return Arrays.stream(removeHttpBody(httpMessage).split("\r\n"))
                .filter(line -> line.startsWith(header))
                .map(SocketHttpRequest::getValue)
                .findFirst();
    }

    private static String getValue(String header){
        String headerValue = header.split(":")[1];
        return headerValue.substring(1);
    }

    public static String removeHttpBody(String httpMessage){
        //return httpMessage.split("\n")[httpMessage.split("\n").length - 1];
        String[] headersAndBody = httpMessage.split("\n\n");
        return headersAndBody[0];
    }

    private static String removeLastLineBreak(String httpBody){
        return httpBody.substring(0, httpBody.length() - 1);
    }

    public static byte[] getBodyBinary(byte[] httpMessage){
        int dataLength = getHeaderValue(new String(httpMessage), "Content-Length")
                .map(Integer::parseInt)
                .orElseThrow(RuntimeException::new);
        return Arrays.copyOfRange(getRawBinaryBody(httpMessage), 0, dataLength);
    }

    public static byte[] getRawBinaryBody(byte[] httpMessage){
        for (int i = 0 ; i < httpMessage.length; i++){
            if (httpMessage[i] == "\n".getBytes()[0]
                    && httpMessage[i + 1] == "\r".getBytes()[0]
                    && httpMessage[i + 2] == "\n".getBytes()[0]
            ) {
                return Arrays.copyOfRange(httpMessage, i + 3, httpMessage.length);
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        //TODO użyć tego:
        String jwt = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtZSIsImV4cCI6MTYwNTU2NjA5OSwiaWF0IjoxNjA1NTYwNjk5fQ.TXB_L-QEBnkQNN2ZAqzwPmCpPrXORdXaB1T6LEEcT1q7I_Uhtn2RsW54syFYVaULVEnVsOsBjRhZ3lW5JWbHMw";

        OkHttpClient client = new OkHttpClient();
        /*HttpUrl mySearchUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("localhost")
                .port(10000)
                .addPathSegment("test")
                .addQueryParameter("code", "45")
                .build();

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/binary"), "abc".getBytes());
        Request request = new Request.Builder()
                .url(mySearchUrl)
                .addHeader("h", "val")
                .addHeader("Authorization", "Bearer " + jwt)
                .post(requestBody)
                .build();*/
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/binary"), "abc".getBytes());
        HttpUrl mySearchUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("localhost")
                .port(10000)
                .addPathSegment("test")
                .addQueryParameter("code", "123")
                .build();
        Request request = new Request.Builder()
                .url(mySearchUrl)
                .addHeader("Authorization", "Bearer " + jwt)
                .addHeader("h", "val")
                .get()
                //.post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        Response response2 = client.newCall(request).execute();
        System.out.println("OPK");
        System.out.println(response.body().string());
        System.out.println(response.headers());
        System.out.println(response.code());
        System.out.println(response);
    }
}