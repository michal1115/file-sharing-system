package maintanance.client.utils;

public class ExternalProcessWatcher extends Thread {
    private Process process;
    private volatile boolean finished = false;

    public ExternalProcessWatcher(Process process) {
        this.process = process;
        this.start();
    }

    public boolean isFinished() {
        return finished;
    }

    public void run() {
        try {
            process.waitFor();
        } catch (Exception ignored) {}
        finished = true;
    }
}

