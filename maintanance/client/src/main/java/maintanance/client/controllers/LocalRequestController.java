package maintanance.client.controllers;

import lombok.SneakyThrows;
import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.clientApp.exceptions.*;
import maintanance.client.controllers.dto.FileVersionDetails;
import maintanance.client.controllers.dto.TeamSummary;
import maintanance.client.controllers.dto.UserConnectionDetails;
import maintanance.client.controllers.dto.UserSummary;
import maintanance.client.model.entities.File;
import maintanance.client.model.entities.FileVersion;
import maintanance.client.model.entities.Invitation;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.FileManager;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@Service
public class LocalRequestController {

    private DynamicPropertiesManager properties;

    private LocalRequestControllerMode controllerMode;

    @SneakyThrows
    public LocalRequestController(){
        this.properties = new DynamicPropertiesManager("dynamic.properties");
        controllerMode = LocalRequestControllerMode.valueOf(properties.getProperty("client_endpoint_mode").toUpperCase());
    }

    @Autowired
    private ClientApp clientApp;

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    @Autowired
    private Environment env;

    @GetMapping("username")
    public String getUsernameAction(){
        return clientApp.getUsername();
    }

    @PostConstruct
    private void printBackendReady(){
        System.out.println("BACKEND_READY");
    }

    @GetMapping("login")
    public boolean loginAction(
            @RequestParam(value="username") String username,
            @RequestParam(value="password") String password
    ){
        try {
            return clientApp.login(username, password).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            return false;
        }
    }

    @GetMapping("register")
    public boolean registerAction(
            @RequestParam(value="username") String username,
            @RequestParam(value="password") String password,
            @RequestParam(value="email") String email
    ){
        try {
            return clientApp.register(username, password, email).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            return false;
        }
    }

    @GetMapping("myTeamsDetails")
    public List<Team> myTeamsDetailsAction(){
        try {
            if (controllerMode == LocalRequestControllerMode.SNAPSHOT){
                return IterableUtils.toList(repositoryWrapper.getTeamRepository().findAll());
            } else if (controllerMode == LocalRequestControllerMode.PROXY){
                return clientApp.myTeamsDetails().getData();
            }
            throw new InvalidControllerModeException();
        } catch (IOException | InterruptedException e) {
            return new LinkedList<>();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return new LinkedList<>();
        }
    }

    @GetMapping("createTeam")
    public boolean createTeamAction(
            @RequestParam(value="teamName") String teamName
    ){
        try {
            return clientApp.createTeam(teamName).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return false;
        }
    }

    @GetMapping("addFileToShared")
    public boolean addFileToSharedAction(
            @RequestParam(value="localFilePath") String localFilePath,
            @RequestParam(value="fileName") String fileName,
            @RequestParam(value="teamName") String teamName
    ){
        String filePathDecoded = new String(Base64.getDecoder().decode(localFilePath));
        try {
            return clientApp.addFileToShared(filePathDecoded, fileName, teamName).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return false;
        }
    }

    @GetMapping("myFileLocations")
    public List<Map<String, String>> myFileLocationsAction(){
        try {
            return clientApp.getMyFilesLocations();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return null;
        }
    }

    @GetMapping("nonLocatedFiles")
    public List<Map<String, String>> notLocatedFilesAction(){
        try {
            return clientApp.getNotLocatedFiles();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return null;
        }
    }

    @GetMapping("locateFile")
    public boolean locateFileAction(
            @RequestParam(value="filePublicName") String filePublicName,
            @RequestParam(value="teamName") String teamName,
            @RequestParam(value="path") String path
    ){
        String filePathDecoded = new String(Base64.getDecoder().decode(path));
        try {
            clientApp.locateFile(filePublicName, teamName, filePathDecoded);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @GetMapping("sendFileState")
    public boolean sendFileStateAction(
            @RequestParam(value="teamName") String teamName,
            @RequestParam(value="publicFileName") String publicFileName
    ){
        System.out.println(teamName);
        System.out.println(publicFileName);
        try {
            clientApp.sendFileStateNotHidden(teamName, publicFileName);
        } catch (UserLocalFilePathNotSpecifiedException | KeyExchangeNotFinishedException e) {
            e.printStackTrace();
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
        }
        return true;
    }

    @GetMapping("sendFileStateHidden")
    public boolean sendFileStateHiddenAction(
            @RequestParam(value="teamName") String teamName,
            @RequestParam(value="publicFileName") String publicFileName
    ){
        try {
            clientApp.sendFileStateHidden(teamName, publicFileName);
        } catch (KeyExchangeNotFinishedException | UserLocalFilePathNotSpecifiedException e) {
            e.printStackTrace();
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
        }
        return true;
    }

    @GetMapping("replaceFileVersion")
    public void replaceFileVersionAction(
            @RequestParam(value="teamName") String teamName,
            @RequestParam(value="publicFileName") String publicFileName,
            @RequestParam(value="otherUser") String otherUser
    ){
        try {
            clientApp.replaceFileVersion(teamName, publicFileName, otherUser);
        } catch (TeamDoesNotExistException | PublicFileDoesNotExistException
                | FileVersionDoesNotExistException | UserLocalFilePathNotSpecifiedException
                | IOException | UserDoesNotBelongToTeamException
                | UserDoesNotExistException e) {
            e.printStackTrace();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
        }
    }


    @GetMapping("invitations")
    public Collection<Invitation> invitationsAction(){
        try {
            if (controllerMode == LocalRequestControllerMode.SNAPSHOT){
                return IterableUtils.toList(repositoryWrapper.getInvitationRepository().findAll());
            } else if (controllerMode == LocalRequestControllerMode.PROXY){
                return clientApp.getInvitations().getData();
            }
            throw new InvalidControllerModeException();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return null;
        }
        return null;
    }

    @GetMapping("answerToInvitation")
    public boolean answerToInvitationAction(
            @RequestParam(value = "teamName") String teamName,
            @RequestParam(value = "decision") String decisionAsStr
    ){
        boolean decision = Boolean.parseBoolean(decisionAsStr);
        try {
            return clientApp.answerToInvitation(teamName, decision).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return false;
        }
    }

    @GetMapping("usersList")
    public List<User> usersListAction(

    ){
        try {
            if (controllerMode == LocalRequestControllerMode.SNAPSHOT){
                return IterableUtils.toList(repositoryWrapper.getUserRepository().findAll());
            } else if (controllerMode == LocalRequestControllerMode.PROXY){
                return clientApp.usersList().getData();
            }
            throw new InvalidControllerModeException();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return new LinkedList<>();
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return new LinkedList<>();
        }
    }

    @GetMapping("addUserToTeam")
    public boolean addUserToTeamAction(
            @RequestParam(value = "teamName") String teamName,
            @RequestParam(value = "userToAdd") String userToAdd
    ){
        try {
            return clientApp.addUserToTeam(teamName, userToAdd).getStatusMessage().equals(StatusMessage.OK);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (NotLoggedInException e) {
            //TODO SEND REQUEST_NOT_POSSIBLE
            return false;
        }
    }

    @GetMapping("isLogged")
    public boolean isLoggedAction(){
        return clientApp.isLogged();
    }

    @GetMapping("logout")
    public void logout() throws NotLoggedInException, IOException, InterruptedException {
        clientApp.logout();
    }

    @GetMapping("userSummary")
    public ResponseEntity<UserSummary> userSummaryAction(
        @RequestParam(value = "userName") String userName
    ){
        if (!isLoggedAction()){
            return ResponseEntity.badRequest().build();
        }
        User myUserRecord = usersListAction().stream()
                .filter(user -> user.getName().equals(getUsernameAction()))
                .findFirst().get();
        return usersListAction().stream()
                .filter(user -> user.getName().equals(userName))
                .findFirst()
                .map(user -> ResponseEntity.ok(new UserSummary(user, myUserRecord)))
                .orElse(ResponseEntity.badRequest().build());
    }

    @GetMapping("teamSummaries")
    public Collection<TeamSummary> getMyTeamsSummaries(){
        Collection<TeamSummary> teamSummaries = new LinkedList<>();
        myTeamsDetailsAction()
                .forEach(team ->
                    teamSummaries.add(new TeamSummary(team))
                );
        return teamSummaries;
    }

    @GetMapping("fileContent")
    public ResponseEntity<byte[]> getFileContent(
            @RequestParam(value = "username") String userName,
            @RequestParam(value = "teamname") String teamName,
            @RequestParam(value = "publicFileName") String publicFileName
    ) {
        if (!isLoggedAction()) {
            return ResponseEntity.badRequest().build();
        }
        Optional<FileVersion> usersFileVersion = repositoryWrapper.getFileVersionRepository()
                .findUsersFileVersion(publicFileName, userName, teamName);
        if (usersFileVersion.isPresent()) {
            usersFileVersion.get().setWasDisplayed(true);
            repositoryWrapper.getFileVersionRepository().save(usersFileVersion.get());
            try {
                byte[] fileContent = FileManager.getFileContent(usersFileVersion.get().getFilePath());
                if (fileContent.length == 0){
                    return ResponseEntity.badRequest().build();
                }
                return ResponseEntity.ok(fileContent);
            } catch (IOException e) {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("sendFileContent")
    public ResponseEntity<?> sendFileContent(
            @RequestParam(value = "teamname") String teamName,
            @RequestParam(value = "publicFileName") String publicFileName,
            @RequestBody(required = false) byte[] content
    ) {
        if (!isLoggedAction()) {
            return ResponseEntity.badRequest().build();
        }
        Optional<FileVersion> usersFileVersion = repositoryWrapper.getFileVersionRepository()
                .findUsersFileVersion(publicFileName, getUsernameAction(), teamName);
        if (usersFileVersion.isPresent()) {
            try {
                if (content != null && content.length > 0) {
                    FileManager.writeToFile(usersFileVersion.get().getFilePath(), content);
                } else {
                    FileManager.writeToFile(usersFileVersion.get().getFilePath(), "");
                }
                return ResponseEntity.ok().build();
            } catch (IOException e) {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("teamMembersConnectionDetails")
    public ResponseEntity<List<UserConnectionDetails>> teamMembersConnectionDetailsAction(
            @RequestParam(value = "teamName") String teamName,
            @RequestParam(value = "isViaHiddenChannel") boolean isViaHiddenChannel
    ) {
        return repositoryWrapper.getTeamRepository().findByName(teamName)
                .map(value -> ResponseEntity.ok(value.getParticipants().stream()
                                    .filter(participant -> !participant.getName().equals(getUsernameAction()))
                                    .map(participant -> new UserConnectionDetails(participant, isViaHiddenChannel))
                                    .collect(Collectors.toList())))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping("invitationWasDisplayed")
    public ResponseEntity<?> invitationWasDisplayedAction(
            @RequestParam(value = "id") int invitationId
    ){
        System.out.println(invitationId);
        Optional<Invitation> invitation = repositoryWrapper.getInvitationRepository().findById(invitationId);
        if (invitation.isPresent()){
            invitation.get().setWasDisplayed(true);
            repositoryWrapper.getInvitationRepository().save(invitation.get());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("fileDetails")
    public ResponseEntity<List<FileVersionDetails>> fileVersionDetailsAction(
            @RequestParam(value = "teamName") String teamName,
            @RequestParam(value = "fileName") String fileName
    ){
        Optional<Team> relatedTeam = repositoryWrapper.getTeamRepository().findByName(teamName);
        if (!relatedTeam.isPresent()){
            return ResponseEntity.badRequest().build();
        }

        Optional<File> relatedFile = repositoryWrapper.getFileRepository().findByNameAndTeam(fileName, relatedTeam.get());
        if (!relatedFile.isPresent()){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(repositoryWrapper.getFileVersionRepository().findAllFileVersionsByFile(relatedFile.get()).stream()
                .map(fileVersion -> FileVersionDetails.builder()
                        .ownerUsername(fileVersion.getVersionOwner().getName())
                        .wasVersionDisplayed(fileVersion.isWasDisplayed())
                        .build())
                .collect(Collectors.toList()));
    }

    private Collection<UserConnectionDetails> usersListToConnectionDetails(Collection<User> usersList,
                                                                           boolean isViaHiddenChannel){
        return usersList.stream()
                .map(user -> new UserConnectionDetails(user, isViaHiddenChannel))
                .collect(Collectors.toList());
    }
}








