package maintanance.client.controllers.dto;

public enum ElementType {
    TEAM,
    USER,
    FILE
}
