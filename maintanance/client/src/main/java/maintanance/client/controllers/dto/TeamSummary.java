package maintanance.client.controllers.dto;


import lombok.Getter;
import lombok.Setter;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;

@Getter
@Setter
public class TeamSummary {
    private String name;
    private int membersCount;
    private int currentlyOnlineMembersCount;
    private int filesCount;
    private String currentSecurityKey;
    private int securityIncidentsCount;

    public TeamSummary(Team team){
        name = team.getTeamName();
        membersCount = team.getParticipants().size();
        currentlyOnlineMembersCount = (int)team.getParticipants().stream().filter(User::isActive).count();
        filesCount = team.getSharedFiles().size();
        currentSecurityKey = team.getSecurityKey();
        securityIncidentsCount = team.getIncidentReports().size();
    }
}
