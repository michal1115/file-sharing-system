package maintanance.client.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class FileVersionDetails {
    private String ownerUsername;
    private boolean wasVersionDisplayed;
}
