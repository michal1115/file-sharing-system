package maintanance.client.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import maintanance.client.model.entities.User;
import maintanance.client.utils.ConnectionType;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserConnectionDetails {

    private boolean isAccessible;

    private String reason;

    private String username;

    private static Map<ConnectionType, String> notHiddenConnectionReasons = new HashMap<>();

    private static Map<ConnectionType, Boolean> notHiddenConnectionAccessibilities = new HashMap<>();

    private static Map<ConnectionType, String> hiddenConnectionReasons = new HashMap<>();

    private static Map<ConnectionType, Boolean> hiddenConnectionAccessibilities = new HashMap<>();

    static {
        notHiddenConnectionReasons.put(ConnectionType.DIRECT, "User is connected directly via UDP");
        notHiddenConnectionAccessibilities.put(ConnectionType.DIRECT, true);

        notHiddenConnectionReasons.put(ConnectionType.PROXY, "User is connected via proxy server");
        notHiddenConnectionAccessibilities.put(ConnectionType.PROXY, true);

        hiddenConnectionReasons.put(ConnectionType.DIRECT, "User is connected directly via UDP - can use stegano");
        hiddenConnectionAccessibilities.put(ConnectionType.DIRECT, true);

        hiddenConnectionReasons.put(ConnectionType.PROXY, "User is not connected directly - stegano has no point if someone knows about the fact that transmitted file is stegano-base");
        hiddenConnectionAccessibilities.put(ConnectionType.PROXY, false);
    }

    private static String NOT_EXCHANGED_KEYS_REASON = "Exchange keys were not negotiated yet - file will not be sent!";

    public UserConnectionDetails(User user, boolean isViaHiddenChannel){
        username = user.getName();
        if (isViaHiddenChannel){
            if (!user.wasPublicKeyNegotiated()){
                isAccessible = false;
                reason = UserConnectionDetails.NOT_EXCHANGED_KEYS_REASON;
            }
            isAccessible = hiddenConnectionAccessibilities.get(user.getConnectionType());
            reason = hiddenConnectionReasons.get(user.getConnectionType());
        } else {
            isAccessible = notHiddenConnectionAccessibilities.get(user.getConnectionType());
            reason = notHiddenConnectionReasons.get(user.getConnectionType());
        }


    }
}
