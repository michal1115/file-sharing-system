package maintanance.client.controllers;

public enum LocalRequestControllerMode {
    SNAPSHOT,
    PROXY
}
