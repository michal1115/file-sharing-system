package maintanance.client.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.utils.ConnectionType;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserSummary {
    private String name;
    private boolean isActive;
    private String ipAddress;
    private int port;
    private ConnectionType connectionType;
    private String localAddress;
    private int localUdpPort;
    private Collection<String> cooperatingTeamsWithMe;
    private int numberOfTeamsCooperating;

    public UserSummary(User user, User myUserRecord){
        this.name = user.getName();
        this.isActive = user.isActive();
        this.ipAddress = user.getIpAddr();
        this.port = user.getPort();
        this.connectionType = user.getConnectionType();
        this.localAddress = user.getLocalAddress();
        this.localUdpPort = user.getLocalUdpPort();

        Collection<String> otherUsersTeams = user.getTeams().stream().map(Team::getTeamName).collect(Collectors.toList());
        Collection<String> myTeams = myUserRecord.getTeams().stream().map(Team::getTeamName).collect(Collectors.toList());
        this.cooperatingTeamsWithMe = otherUsersTeams.stream().filter(myTeams::contains).collect(Collectors.toList());
        this.numberOfTeamsCooperating = this.cooperatingTeamsWithMe.size();
    }
}
