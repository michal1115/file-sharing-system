package maintanance.client.steganography;

import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.ExternalProcessWatcher;
import maintanance.client.utils.FileManager;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class SteganoManager {
    public static String python;

    static {
        try {
            python = new DynamicPropertiesManager("dynamic.properties")
                        .getProperty("python_interpreter_cmd");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String hideFilePath;
    private static String retrieveFilePath;

    private static void dumpScripts() throws IOException {
        File file1 = File.createTempFile("hide_file", ".py");
        try (BufferedWriter br = new BufferedWriter(new FileWriter(file1))) {
            br.write("from cryptosteganography import CryptoSteganography\n");
            br.write("import sys\n");
            br.write("password = sys.argv[1]\n");
            br.write("input_file = sys.argv[2]#steganography base image\n");
            br.write("output_file = sys.argv[3]\n");
            br.write("content_to_hide = open(sys.argv[4], \"r+\").read()\n");
            br.write("crypto = CryptoSteganography(password)\n");
            br.write("crypto.hide(input_file, output_file, content_to_hide)\n");
        }
        hideFilePath = file1.getPath();
        System.out.println(hideFilePath);

        File file2 = File.createTempFile("retrieve_file", ".py");
        try (BufferedWriter br = new BufferedWriter(new FileWriter(file2))) {
            br.write("from cryptosteganography import CryptoSteganography\n");
            br.write("import sys\n");
            br.write("import os\n");
            br.write("password = sys.argv[1]\n");
            br.write("input_file = sys.argv[2]\n");
            br.write("output_file = sys.argv[3]\n");
            br.write("if os.path.exists(output_file):\n");
            br.write("\tos.remove(output_file)\n");
            br.write("crypto = CryptoSteganography(password)\n");
            br.write("secret_msg = crypto.retrieve(input_file)\n");
            br.write("if secret_msg is not None:\n");
            br.write("\tfile = open(output_file, \"w+\")\n");
            br.write("\tfile.write(secret_msg)\n");
            br.write("\tfile.close()\n");
        }
        retrieveFilePath = file2.getPath();
        System.out.println(retrieveFilePath);
    }
    static {
        try {
            dumpScripts();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void runProc(String[] args) throws IOException {
        Process proc = Runtime.getRuntime().exec(args);
        ExternalProcessWatcher processWatcher = new ExternalProcessWatcher(proc);

        while(!processWatcher.isFinished()){ //wait for process to finish
        }
    }

    public static void hideFile(String password, String inputFile, String outputFile, String fileToHide) throws IOException {
        //String scriptPath = "classpath:py_scripts\\hide_file.py";
        String scriptPath = "hide_file.py";
        System.out.println(scriptPath);
        String[] args = {python, hideFilePath, password, inputFile, outputFile, fileToHide};
        runProc(args);
    }

    public static boolean retrieveFile(String password, String inputFile, String outputFile) throws IOException {
        //String scriptPath = "classpath:py_scripts\\retrieve_file.py";
        String scriptPath = "retrieve_file.py";
        String[] args = {python, retrieveFilePath, password, inputFile, outputFile};

        runProc(args);

        File file = new File(outputFile);
        return file.exists();
    }

    public static void getFileFromServer(String outputPath, String theme, String size) throws IOException, InterruptedException {
        System.out.println(outputPath);
        System.out.println(theme);
        System.out.println(size);
        FileManager.writeToFile(outputPath, ResourceProvider.getResource(theme, size));
    }

    public static List<String> getAvailableThemesFromServer() throws IOException, InterruptedException {
        return ResourceProvider.getAvailableThemes();
    }

    public static String getFakeTeamFromServer(String theme) throws IOException, InterruptedException {
        return ResourceProvider.getFakeTeam(theme);
    }

    public static String getFakeFileNameFromServer(String theme) throws IOException, InterruptedException {
        return ResourceProvider.getFakeFileName(theme);
    }





    public static void main(String[] args) throws IOException {

        SteganoManager steganoManager = new SteganoManager();

        /*Socket socket = FakeSocket.getSocket();
        getFileFromServer(socket, "in.png", "small");
        hideFile("secret", "in.png", "out.png", "test.txt");
        retrieveFile("secret", "out.png", "test_out.txt");*/
    }
}
