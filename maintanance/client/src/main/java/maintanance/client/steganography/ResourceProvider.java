package maintanance.client.steganography;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.SneakyThrows;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.SocketHttpRequest;
import maintanance.client.utils.RequestUrl;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
@Component
public class ResourceProvider {

    private static Gson gson = new Gson();

    protected static String serverHostname;
    protected static int serverPort;
    private static DynamicPropertiesManager properties;
    static {
        try {
            properties = new DynamicPropertiesManager("dynamic.properties");
            serverHostname = properties.getProperty("serverAddr");
            serverPort = Integer.parseInt(properties.getProperty("serverPort"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //takes picture from server
    public static byte[] getResource(String theme, String size) throws IOException, InterruptedException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("randomPicture")
                .addPathSegment(theme)
                .addPathSegment(size)
                .build();
        Response response = execute(url);
        byte[] responseAsBytes =  response.body().bytes();
        System.out.println(url);
        System.out.println("#getResource");
        System.out.println(responseAsBytes.length);
        return responseAsBytes;
    }

    public static List<String> getAvailableThemes() throws IOException, InterruptedException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("themes")
                .build();
        Response response = execute(url);
        Type stringListType = new TypeToken<List<String>>(){}.getType();
        return Arrays.asList(gson.fromJson(response.body().string(), String[].class));
    }

    public static String getFakeTeam(String theme) throws IOException, InterruptedException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("randomTeam")
                .addPathSegment(theme)
                .build();
        return execute(url).body().string();
    }

    public static String getFakeFileName(String theme) throws IOException, InterruptedException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("randomFileName")
                .addPathSegment(theme)
                .build();
        return execute(url).body().string();
    }

    private static Response execute(HttpUrl url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        return client.newCall(request).execute();
    }

    //TEST:
    @SneakyThrows
    public static void main(String[] args) throws IOException {
        System.out.println(getResource(getAvailableThemes().get(0), "big"));
        System.out.println(getFakeTeam("wallpapers"));
        System.out.println(getFakeFileName("wallpapers"));
    }
}
