package maintanance.client.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;

import java.awt.image.AreaAveragingScaleFilter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class AuthRequest {
    private String username;
    private String password;
    private String email;
    private String localAddress;
    private int localUdpPort;
    private String cryptoPublicKey;

    public String toJson(){
        return new Gson().toJson(this);
    }
}
