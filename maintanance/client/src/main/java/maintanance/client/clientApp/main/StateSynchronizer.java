package maintanance.client.clientApp.main;

import lombok.SneakyThrows;
import maintanance.client.clientApp.main.ClientApp;

public class StateSynchronizer extends Thread{

    private ClientApp clientApp;

    private int synchronization_interval;

    public StateSynchronizer(ClientApp clientApp, int synchronization_interval){
        this.clientApp = clientApp;
        this.synchronization_interval = synchronization_interval;
    }

    @SneakyThrows
    public void run(){
        while(!interrupted()){
            Thread.sleep(synchronization_interval);
            clientApp.synchronizeState();
        }
    }
}
