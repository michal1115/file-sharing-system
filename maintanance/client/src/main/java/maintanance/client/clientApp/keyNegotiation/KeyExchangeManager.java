package maintanance.client.clientApp.keyNegotiation;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import lombok.SneakyThrows;
import maintanance.client.clientApp.fileSynchronization.UDPLocalSynchronizer;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.ConnectionType;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.SocketHttpRequest;
import maintanance.client.model.entities.User;
import maintanance.client.model.repositories.UserRepository;
import maintanance.client.utils.RequestUrl;

import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

public class KeyExchangeManager extends Thread {
    private UserRepository userRepository;
    private UDPLocalSynchronizer UDPLocalSynchronizer;
    private String myUserName;
    private KeyMessageBuffer keyMessageBuffer;

    private int keyMessageResendInterval;
    private DynamicPropertiesManager properties;

    private ClientApp clientApp;

    public KeyExchangeManager(UserRepository userRepository, UDPLocalSynchronizer UDPLocalSynchronizer, String myUserName, KeyMessageBuffer keyMessageBuffer, ClientApp clientApp) throws IOException {
        this.userRepository = userRepository;
        this.UDPLocalSynchronizer = UDPLocalSynchronizer;
        this.myUserName = myUserName;
        this.keyMessageBuffer = keyMessageBuffer;
        this.clientApp = clientApp;

        properties = new DynamicPropertiesManager("dynamic.properties");
        keyMessageResendInterval = Integer.parseInt(properties.getProperty("key_message_resend_interval")) * 1000;
    }

    @SneakyThrows
    @Override
    public void run(){
        while(!interrupted()){

            while (!keyMessageBuffer.isEmpty()){
                KeyExchangeMsg keyExchangeMsg = keyMessageBuffer.take();
                System.out.println("RECEIVED DIRECT: "+ keyExchangeMsg.getPublicKey());
                Optional<User> senderOpt = userRepository.findByName(keyExchangeMsg.getUsername());
                if (senderOpt.isPresent()){
                    saveReceivedKey(keyExchangeMsg.getUsername(), keyExchangeMsg.getPublicKey());
                }
                if (shouldResendKey()){
                    Optional<User> user = userRepository.findByName(keyExchangeMsg.getUsername());
                    if (user.isPresent()) {
                        sendKeyToUser(user.get());
                    }
                }
            }

            ProxyKeyExchanger.downloadKeysFromProxy()
                    .forEach(keyRecord -> {
                        try {
                            System.out.println("RECEIVED PROXY: "+ keyRecord.getKey());
                            saveReceivedKey(keyRecord.getSenderName(), Long.parseLong(keyRecord.getKey()));
                            if (shouldResendKey()){
                                Optional<User> user = userRepository.findByName(keyRecord.getSenderName());
                                if (user.isPresent()) {
                                    sendKeyToUser(user.get());
                                }
                            }
                        } catch (IOException | InterruptedException | NumberFormatException e) {
                            e.printStackTrace();
                        }
                    });

            Thread.sleep(keyMessageResendInterval);
            synchronized(userRepository) {
                generateAndSendKeys();
            }
        }
    }

    private boolean shouldResendKey(){
        Random random = new Random();
        return random.nextInt() % 10 > 3;
    }


    private void saveReceivedKey(String senderName, long publicKey) throws IOException, InterruptedException {
        Map<String, Integer> keyExchangeMap = getPublicServerKeys();

        Optional<User> sender = userRepository.findByName(senderName);
        if (!sender.isPresent()){
            return;
        }
        long exchangeKey = (long)(Math.pow(publicKey, sender.get().getPrivateKey())) % keyExchangeMap.get("p");
        sender.get().setExchangeKey(exchangeKey);

        userRepository.save(sender.get());
    }

    private void generateAndSendKeys(){
        generateKeysForUsersWithoutKeys();

        userRepository.getUsersWithoutExchangeKey().stream()
                .filter(user -> !user.getName().equals(myUserName))
                .filter(User::isActive)
                .forEach(this::sendKeyToUser);
    }

    private void generateKeysForUsersWithoutKeys(){
        Collection<User> usersWithNonGeneratedPairPrivateKeys = userRepository.getUsersWithNonGeneratedPrivateKey();

        Random random = new Random();
        usersWithNonGeneratedPairPrivateKeys.stream()
                .filter(user -> !user.getName().equals(myUserName))
                .forEach(user -> {
                    if (user.getPrivateKey() == 0) {
                        System.out.println(user.getName());
                        System.out.println(user.getPrivateKey());
                        int key = random.nextInt(10) + 1;
                        user.setPrivateKey(key);
                        userRepository.save(user);
                    }
                });
    }

    private void sendKeyToUser(User user){
        System.out.println(user.getName());
        try {
            Map<String, Integer> publicKeys = getPublicServerKeys();
            long publicKey = calculatePublicKey(user.getPrivateKey(), (long)publicKeys.get("g"), (long)publicKeys.get("p"));
            if (user.getConnectionType().equals(ConnectionType.DIRECT)){
                System.out.println("DIRECT KEY SENT " + publicKey);
                UDPLocalSynchronizer.sendPublicKey(
                        publicKey,
                        this.myUserName,
                        user.getLocalAddress(),
                        user.getLocalUdpPort(),
                        getUsersPublicKey(user));
            } else if (user.getConnectionType().equals(ConnectionType.PROXY)){
                System.out.println("PROXY KEY SENT " + publicKey);
                ProxyKeyExchanger.sendPublicKeyViaProxy(user, String.valueOf(publicKey));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Integer> getPublicServerKeys() throws IOException, InterruptedException {
        Socket serverAccessSocket = SocketHttpRequest.openServerSocket();

        String url = RequestUrl.builder()
                .path("/keyExchange")
                .build()
                .toString();

        String reqResult = SocketHttpRequest.removeHttpHeader(
                SocketHttpRequest.sendRequestAuthorized(url, serverAccessSocket));

        SocketHttpRequest.closeServerSocket(serverAccessSocket);

        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(reqResult, JsonElement.class);
        Integer p = jsonElement.getAsJsonObject().get("p").getAsInt();
        Integer g = jsonElement.getAsJsonObject().get("g").getAsInt();
        Map<String, Integer> keys = new HashMap<>();
        keys.put("p", p);
        keys.put("g", g);
        return keys;
        /*Map<String, Integer> publicKeysMap = new HashMap<>();
        Arrays.asList(//parse map from string
                reqResult
                        .replace("{", "")
                        .replace("}", "")
                        .replace("\"", "")
                        .split(",")
        )
                .forEach(entry ->
                        publicKeysMap.put(entry.split(":")[0],
                                Integer.parseInt(entry.split(":")[1]))
                );
*/

        //return publicKeysMap;
    }

    private long calculatePublicKey(long privateKey, long g, long p){
        return (long) (Math.pow(g, privateKey)) % p;
    }

    public static void main(String[] args){
        String reqResult = "{\"p\":7,\"g\":3}";
        Map<String, Integer> publicKeysMap = new HashMap<>();
        Arrays.asList(//parse map from string
                reqResult
                        .replace("{", "")
                        .replace("}", "")
                        .replace("\"", "")
                        .split(",")
        )
                .forEach(entry ->
                        publicKeysMap.put(entry.split(":")[0],
                                Integer.parseInt(entry.split(":")[1]))
                );

        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(reqResult, JsonElement.class);
        Integer p = jsonElement.getAsJsonObject().get("p").getAsInt();
        Integer g = jsonElement.getAsJsonObject().get("g").getAsInt();
        System.out.println(p);
        System.out.println(g);
        System.out.println(publicKeysMap);
    }

    private PublicKey getUsersPublicKey(User user){
        try {
            return clientApp.getCryptoManager().deserializePublicKey(user.getCryptoPublicKey());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }
}
