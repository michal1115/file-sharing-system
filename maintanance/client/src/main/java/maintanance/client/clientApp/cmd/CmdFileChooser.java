package maintanance.client.clientApp.cmd;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.List;

public class CmdFileChooser {
    private String currentDir;

    public CmdFileChooser(){
    }

    public Set<String> filesInDir(String dir){
        return Stream.of(Objects.requireNonNull(new File(dir).listFiles()))
                .map(file -> file.isDirectory() ? file.getName() + "\t"  + "[DIR]" : file.getName())
                .collect(Collectors.toSet());
    }

    private String cutLastPathElement(String path){
        String[] pathParts = currentDir.split("\\\\");
        if(pathParts.length < 2){
            return path;
        }

        List<String> partsList = Arrays.asList(pathParts);
        partsList = partsList.subList(0, pathParts.length - 1);

        StringBuilder pathBuilder = new StringBuilder();

        partsList.forEach(part -> pathBuilder.append(part).append("\\"));

        return pathBuilder.toString();
    }

    public void changeDir(String path){
        String trimmedPath = path.trim();

        if (trimmedPath.equals("..")){
            currentDir = cutLastPathElement(currentDir);
            System.out.println("Switched path to " + currentDir);
        }

        else if (trimmedPath.contains("\\")){
            File f = new File(trimmedPath);
            if(f.exists() && f.isDirectory()){
                currentDir = trimmedPath;
                if (!currentDir.endsWith("\\")){
                    currentDir += "\\";
                }
                System.out.println("Switched path to " + trimmedPath);
            }
            else{
                System.out.println("File does not exist");
            }
        }
        else{
            String fullPath = currentDir + trimmedPath;
            File f = new File(fullPath);
            if(f.exists() && f.isDirectory()){
                currentDir = fullPath;
                if (!currentDir.endsWith("\\")){
                    currentDir += "\\";
                }
                System.out.println("Switched path to " + fullPath);
            }
            else{
                System.out.println("Given path does not point to directory");
            }
        }

    }
    
    public String takeFile(String path){
        String trimmedPath = path.trim();
        
        String resultFilePath = null;
        
        if (trimmedPath.contains("\\")){
            File f = new File(trimmedPath);
            if(f.exists() && ! f.isDirectory()){
                resultFilePath = trimmedPath;
                if (resultFilePath.endsWith("\\")){
                    resultFilePath = resultFilePath.substring(0, resultFilePath.length() - 1);
                }
            }
            else{
                System.out.println("File does not exist");
            }
        }
        else{
            String fullPath = currentDir + trimmedPath;
            File f = new File(fullPath);
            if(f.exists() && !f.isDirectory()){
                resultFilePath = fullPath;
                if (resultFilePath.endsWith("\\")){
                    resultFilePath = resultFilePath.substring(0, resultFilePath.length() - 1);
                }
            }
            else{
                System.out.println("Given file does not exist");
            }
        }
        return resultFilePath;
    }
    public String search(){
        System.out.println("You can use dir/ls and cd commands.");
        System.out.println("If you are ready, write: take [file]\n");
        currentDir = System.getProperty("user.dir");
        if(! currentDir.endsWith("\\")){
            currentDir = currentDir + "\\";
        }

        boolean fileChosen = false;
        Scanner scanner = new Scanner(System.in);

        String result = null;
        while(!fileChosen){
            String command = scanner.nextLine();
            if(command.trim().equals("dir") || command.trim().equals("ls")){
                System.out.println(currentDir);
                System.out.println();
                filesInDir(currentDir).forEach(System.out::println);
            }
            if(command.trim().split(" ")[0].equals("cd")){
                changeDir(command.trim().split(" ")[1]);
            }

            if(command.trim().split(" ")[0].equals("take")){
                result = takeFile(command.trim().split(" ")[1]);
                fileChosen = true;
            }
        }
        return result;
    }
}
