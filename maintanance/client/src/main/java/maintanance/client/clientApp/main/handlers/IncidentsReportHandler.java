package maintanance.client.clientApp.main.handlers;

import maintanance.client.authentication.AuthRequest;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;

@Component
public class IncidentsReportHandler extends AbstractHandler {

    public RequestResult<String> handle(String relatedTeamName, String incident) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl(relatedTeamName))
                .put(RequestBody.create(MediaType.parse("application/binary"), incident.getBytes()))
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();

        return formRequestResult(response);
    }

    public HttpUrl getUrl(String relatedTeamName){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("teams")
                .addPathSegment(relatedTeamName)
                .addPathSegment("incidents")
                .build();
    }
}
