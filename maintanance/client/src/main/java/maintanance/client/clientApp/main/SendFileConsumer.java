package maintanance.client.clientApp.main;
import maintanance.client.clientApp.exceptions.*;

@FunctionalInterface
public interface SendFileConsumer<T>{
    void accept(T t) throws UserLocalFilePathNotSpecifiedException, KeyExchangeNotFinishedException;
}
