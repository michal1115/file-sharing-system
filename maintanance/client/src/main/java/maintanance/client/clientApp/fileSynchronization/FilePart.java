package maintanance.client.clientApp.fileSynchronization;

import lombok.*;

import java.util.Arrays;

@Getter
@Setter
@AllArgsConstructor
@Builder
@ToString(exclude = "fileContent")
public class FilePart {
    private String fileName;
    private String teamName;
    private String username;

    private int fileSize;
    private int packageFileContentSize;

    private int totalPackages;
    private int packageIndex;
    private String securityKey;

    private byte[] fileContent;


    public FilePart(String fileName, String teamName, String username, int fileSize, int packageFileContentSize, int packageIndex, String securityKey, byte[] fileContent) {
        this.fileName = fileName;
        this.teamName = teamName;
        this.username = username;
        this.fileSize = fileSize;
        this.packageFileContentSize = packageFileContentSize;
        this.totalPackages = (int)Math.ceil( (double)fileSize / (double)packageFileContentSize );
        this.packageIndex = packageIndex;
        this.securityKey = securityKey;
        this.fileContent = fileContent;
    }

    public FilePart(byte[] message){
        String messageAsString = new String(message);

        this.username = UDPLocalSynchronizer.getParameter(messageAsString, "username");
        this.fileName = UDPLocalSynchronizer.getParameter(messageAsString, "fileName");
        this.teamName = UDPLocalSynchronizer.getParameter(messageAsString, "teamName");
        this.fileSize = Integer.parseInt( UDPLocalSynchronizer.getParameter(messageAsString, "fileSize") );
        this.packageFileContentSize = Integer.parseInt( UDPLocalSynchronizer.getParameter(messageAsString, "packageFileContentSize") );
        this.totalPackages = (int)Math.ceil((double)fileSize / (double)packageFileContentSize);
        this.packageIndex = Integer.parseInt( UDPLocalSynchronizer.getParameter(messageAsString, "packageIndex") );
        this.securityKey = UDPLocalSynchronizer.getParameter(messageAsString, "securityKey");

        int position = messageAsString.indexOf("#fileContent=") + "#fileContent=".length();
        this.fileContent = getFileContent(message, packageFileContentSize);
        System.out.println("--------------------------------------------------------------");
        System.out.println("FilePart: 55");
        System.out.println(packageIndex);
        System.out.println(new String(fileContent));
        System.out.println("--------------------------------------------------------------");
    }

    public static byte[] getFileContent(byte[] message, int packageFileContentSize){
        for (int i = 0; i < message.length - "#fileContent=".length(); i++){
            if (areFollowingBytesFileContentMarker(message, i)){
                //return Arrays.copyOfRange(message, i + "#fileContent=".length(), message.length);
                return Arrays.copyOfRange(message, i + "#fileContent=".length(), packageFileContentSize + i + "#fileContent=".length());
            }
        }
        return null;
    }
    private static boolean areFollowingBytesFileContentMarker(byte[] data, int startIndex){
        String cmp = "#fileContent=";
        for (int i = 0; i < cmp.length(); i++){
            if (! ( (char)data[startIndex + i] == cmp.charAt(i)) ){
                return false;
            }
        }
        return true;
    }
}
