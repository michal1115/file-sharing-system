package maintanance.client.clientApp.fileSynchronization;

import lombok.SneakyThrows;
import maintanance.client.clientApp.exceptions.MaximumPacketMetadataOverflowException;
import maintanance.client.clientApp.keyNegotiation.KeyExchangeMsg;
import maintanance.client.clientApp.keyNegotiation.KeyMessageBuffer;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.*;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.steganography.SteganoManager;
import maintanance.client.utils.FileManager;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import maintanance.client.utils.UDPCleaner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.security.*;
import java.util.*;


//TODO name it as UDPDirectSynchronizer
public class UDPLocalSynchronizer extends Thread {

    private static DatagramSocket socket;

    public static int MAX_UDP_DATA_SIZE = 55_000;
    public static int MAX_FILE_CONTENT_SIZE_INSIDE_PACKET = 40_000;
    public static int MAX_METADATA_SIZE = MAX_UDP_DATA_SIZE - MAX_FILE_CONTENT_SIZE_INSIDE_PACKET;

    private FilePartBuffer filePartBuffer;
    private KeyMessageBuffer keyMessageBuffer;

    private RepositoryWrapper repositoryWrapper;

    private ClientApp clientApp;

    public UDPLocalSynchronizer(DatagramSocket socket, FilePartBuffer filePartBuffer, KeyMessageBuffer keyMessageBuffer, RepositoryWrapper repositoryWrapper, ClientApp clientApp) {
        this.socket = socket;
        this.filePartBuffer = filePartBuffer;
        this.keyMessageBuffer = keyMessageBuffer;
        this.repositoryWrapper = repositoryWrapper;
        this.clientApp = clientApp;
    }

    @SneakyThrows
    public void sendHidden(String username, String publicFileName, String filePath, String teamName, String receiverName,
                           String ipv4Addr, int port, long exchangeKey, PublicKey receiverPublicKey) {
        byte[] fileContent = new FileManager().getFileContent(filePath);
        int packageSize = fileContent.length;

        User sender = repositoryWrapper.getUserRepository().findByName(username).get();
        User receiver = repositoryWrapper.getUserRepository().findByName(receiverName).get();
        Team team = repositoryWrapper.getTeamRepository().findByName(teamName).get();
        File publicFile = repositoryWrapper.getFileRepository().findByNameAndTeam(publicFileName, team).get();

        SentPacket messageToHide = SentPacket.builder()
                .packetUUID(UUID.randomUUID().toString())
                .sender(sender)
                .receiver(receiver)
                .publicFile(publicFile)
                .team(team)
                .fileSize(fileContent.length)
                .packageFileContentSize(packageSize)
                .packageIndex(0)
                .fileContent(fileContent)
                .securityKey(team.getSecurityKey())
                .isDelivered(false)
                .build();

        UUID uuid = UUID.randomUUID();
        String messagePath = "tmp_" + uuid.toString();

        FileManager.writeToFile(messagePath, messageToHide.getBytes());

        Random random = new Random();
        List<String> hideThemes = SteganoManager.getAvailableThemesFromServer();

        int choice = random.nextInt(hideThemes.size());
        String theme = hideThemes.get(choice);

        String fakeTeamName = SteganoManager.getFakeTeamFromServer(theme);
        String fakeFileName = SteganoManager.getFakeFileNameFromServer(theme);

        String size;
        if (packageSize < 20_000){
            size = "small";
        }
        else if (packageSize < 100_000){
            size = "medium";
        }
        else{
            size = "big";
        }

        uuid = UUID.randomUUID();
        String pathSteganoBase = "tmp_" + uuid.toString() + ".png";
        SteganoManager.getFileFromServer(pathSteganoBase, theme, size);

        uuid = UUID.randomUUID();
        String pathHidden = "tmp_" + uuid.toString() + ".png";
        SteganoManager.hideFile(String.valueOf(exchangeKey), pathSteganoBase, pathHidden, messagePath);


        send(username, fakeFileName, pathHidden, fakeTeamName, receiverName, ipv4Addr, port, receiverPublicKey);
    }


    @SneakyThrows
    public void send(String username, String publicFileName, String filePath, String teamName, String receiverName, String ipv4Addr,
                     int port, PublicKey receiverPublicKey) {
        //get packet params:
        String packetId = UUID.randomUUID().toString();

        Optional<User> senderOptional = repositoryWrapper.getUserRepository().findByName(username);
        if (!senderOptional.isPresent()){
            throw new Exception();
        }
        User sender = senderOptional.get();

        Optional<User> receiverOptional = repositoryWrapper.getUserRepository().findByName(receiverName);
        if (!receiverOptional.isPresent()){
            throw new Exception();
        }
        User receiver = receiverOptional.get();

        Team team;
        Optional<Team> teamOptional = repositoryWrapper.getTeamRepository().findByName(teamName);
        if (!teamOptional.isPresent()){
            //hidden packet is coming
            team = new Team(teamName);
            team.setId(new Random().nextInt(100_000) + 50_000);

            //repositoryWrapper.getTeamRepository().save(team);
        } else {
            team = teamOptional.get();
        }

        File publicFile;
        Optional<File> publicFileOptional = repositoryWrapper.getFileRepository().findByNameAndTeam(publicFileName, team);
        if (!publicFileOptional.isPresent()){
            //hidden packet is coming
            publicFile = new File(publicFileName, team);
            publicFile.setId(new Random().nextInt(100_000) + 50_000);

            //repositoryWrapper.getFileRepository().save(publicFile);
        } else {
            publicFile = publicFileOptional.get();
        }

        byte[] fileContent = new FileManager().getFileContent(filePath);
        int fileSize = fileContent.length;
        ListIterator<byte[]> filePartIterator = splitFileContent(fileContent).listIterator();

        while (filePartIterator.hasNext()){
            int filePartIndex = filePartIterator.nextIndex();
            byte[] filePartContent = filePartIterator.next();
            SentPacket sentPacket = SentPacket.builder()
                    .packetUUID(UUID.randomUUID().toString())
                    .sender(sender)
                    .receiver(receiver)
                    .publicFile(publicFile)
                    .team(team)
                    .fileSize(fileSize)
                    .packageFileContentSize(filePartContent.length)
                    .packageIndex(filePartIndex)
                    .fileContent(filePartContent)
                    .securityKey(team.getSecurityKey())
                    .isDelivered(false)
                    .build();
            if (sentPacket.getMetaData().length() > MAX_METADATA_SIZE){
                throw new MaximumPacketMetadataOverflowException();
            }
            sendFilePartPacket(sentPacket, ipv4Addr, port, receiverPublicKey);
            Thread.sleep(1);
        }
    }

    private List<byte[]> splitFileContent(byte[] fileContent){
        List<byte[]> filePartsToSend = new ArrayList<>();
        int fileSize = fileContent.length;
        int filePartsCount = fileSize % MAX_FILE_CONTENT_SIZE_INSIDE_PACKET == 0
                ? fileSize / MAX_FILE_CONTENT_SIZE_INSIDE_PACKET
                : fileSize / MAX_FILE_CONTENT_SIZE_INSIDE_PACKET + 1;
        for (int i = 0; i < filePartsCount; i++){
            int packetPartStart = MAX_FILE_CONTENT_SIZE_INSIDE_PACKET * i;
            int packetPartEnd = Math.min(MAX_FILE_CONTENT_SIZE_INSIDE_PACKET * (i + 1), fileSize);
            int packetPartLength = packetPartEnd - packetPartStart;
            byte[] nextFilePart = new byte[packetPartLength];
            System.arraycopy(fileContent, packetPartStart, nextFilePart, 0, packetPartLength);
            filePartsToSend.add(nextFilePart);
        }
        return filePartsToSend;
    }

    /**
     * Sends packet according to SentPacket object which represents all the data connected with sent packet.
     *
     * @param sentPacket object from database that represents packet data
     * @param ipv4Addr receiver address
     * @param port receiver port
     * @throws IOException throws exception on socket.send error
     */
    public void sendFilePartPacket(SentPacket sentPacket, String ipv4Addr, int port, PublicKey receiverPublicKey) throws IOException {
        InetAddress address = InetAddress.getByName(ipv4Addr);
        byte[] encryptedData = encryptMessage(sentPacket.getBytes(), receiverPublicKey);

        DatagramPacket packet = new DatagramPacket(encryptedData, encryptedData.length, address, port);
        socket.send(packet);
    }

    public static void sendAcknowledgement(String sender, String packetUUID, String ipv4Addr, int port) throws IOException {
        String message = "#msgType=ACKNOWLEDGEMENT"
                + "#sender=" + sender
                + "#packetUUID=" + packetUUID
                + "#";
        InetAddress address = InetAddress.getByName(ipv4Addr);

        DatagramPacket packet = new DatagramPacket(message.getBytes(), message.getBytes().length, address, port);
        socket.send(packet);
    }

    static String getParameter(String metaData, String parameterName){
        int from = metaData.indexOf( parameterName) + parameterName.length() + 1;
        int to = metaData.indexOf( "#", from);
        if (from == -1) System.out.println("THERE IS NO: " + parameterName);
        // System.out.println("from: " + from + " to: " + to);
        return metaData.substring(from, to);
    }

    public enum MsgType{
        FILE_PART,
        KEY_NEGOTIATION,
        ACKNOWLEDGEMENT
    }

    public void sendPublicKey(long publicKey, String username, String ipv4Addr, int port, PublicKey publicCryptoKey) throws IOException {
        String msgData = "#msgType=KEY_NEGOTIATION" + "#username=" + username + "#value=" + publicKey + "#";
        byte[] msg = msgData.getBytes();
        byte[] encrypted = encryptMessage(msg, publicCryptoKey);

        InetAddress address = InetAddress.getByName(ipv4Addr);

        DatagramPacket sendPacket = new DatagramPacket(encrypted, encrypted.length, address, port);

        socket.send(sendPacket);
    }


    public void listenOnSocket() throws IOException {
        while(!interrupted()) {

            byte[] receivedBuffer = new byte[MAX_UDP_DATA_SIZE];
            DatagramPacket receivePacket = new DatagramPacket(receivedBuffer, receivedBuffer.length);
            socket.receive(receivePacket);

            byte[] decryptedMessage;
            try {
                decryptedMessage = decryptMessage(receivedBuffer);
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | NoSuchProviderException | InvalidKeyException e) {
                System.out.println("DECRYPTION EXCEPTION");
                return;
            }
            String messageAsString = new String(decryptedMessage);
            MsgType msgType = MsgType.valueOf(getParameter(messageAsString, "msgType"));

            if (msgType == MsgType.FILE_PART) {
                manageReceivedFilePartPacket(decryptedMessage);
            }
            else if (msgType == MsgType.KEY_NEGOTIATION){
                keyMessageBuffer.put(
                        new KeyExchangeMsg(
                                getParameter(messageAsString, "username"),
                                Long.parseLong(getParameter(messageAsString, "value")))
                );
            }
            else if (msgType == MsgType.ACKNOWLEDGEMENT){
                manageReceivedAcknowledgement(decryptedMessage);
            }
        }
    }

    private void manageReceivedFilePartPacket(byte[] receiveBuffer){
        String messageAsString = new String(receiveBuffer);
        String packetUUID = getParameter(messageAsString, "packetUUID");

        Optional<ReceivedPacket> receivedPacketOptional = repositoryWrapper.getReceivedPacketRepository()
                .findByUUID(packetUUID);
        if (receivedPacketOptional.isPresent()){
            ReceivedPacket receivedPacket = receivedPacketOptional.get();
            receivedPacket.setIsAckDelivered(false);//sender did not obtain ack
            //repositoryWrapper.getReceivedPacketRepository().save(receivedPacket);
        } else {
            repositoryWrapper.getUserRepository().findByName(getParameter(messageAsString, "username"))
                    .ifPresent(sender -> {
                        //repositoryWrapper.getReceivedPacketRepository().save(new ReceivedPacket(packetUUID, false, sender));
                        filePartBuffer.put(new FilePart(receiveBuffer));//put file part on buffer, this is new part of file
                    });
        }
    }

    private void manageReceivedAcknowledgement(byte[] receiveBuffer){
        String messageAsString = new String(receiveBuffer);
        String packetUUID = getParameter(messageAsString, "packetUUID");

        Optional<SentPacket> sentPacket = repositoryWrapper.getSentPacketRepository()
                .findByUUID(packetUUID);
        if (sentPacket.isPresent()){
            sentPacket.get().setIsDelivered(true);
            repositoryWrapper.getSentPacketRepository().save(sentPacket.get());
        } else {
            //TODO manage this:
            System.out.println("UNKNOWN ACK WAS RECEIVED");
        }
    }

    private byte[] encryptMessage(byte[] message, PublicKey receiverPublicCryptoKey){
        try {
            return clientApp.getCryptoManager().encryptMessage(message, receiverPublicCryptoKey);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] decryptMessage(byte[] message) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException {
        return clientApp.getCryptoManager().decryptMessage(message, ClientApp.cryptoPrivateKey);
    }

    public void run()
    {
        try {
            listenOnSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
