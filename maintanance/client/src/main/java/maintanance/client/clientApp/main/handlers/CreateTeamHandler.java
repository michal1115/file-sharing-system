package maintanance.client.clientApp.main.handlers;

import maintanance.client.authentication.AuthRequest;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;

@Component
public class CreateTeamHandler extends AbstractHandler {

    public RequestResult<String> handle(String teamName) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl(teamName))
                .put(getEmptyRequestBody())
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();

        return formRequestResult(response);
    }

    public HttpUrl getUrl(String teamName){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("teams")
                .addPathSegment(teamName)
                .build();
    }
}
