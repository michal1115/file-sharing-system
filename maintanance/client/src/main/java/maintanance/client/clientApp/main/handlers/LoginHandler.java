package maintanance.client.clientApp.main.handlers;

import maintanance.client.authentication.AuthRequest;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.cryptography.asymmetric.ECCAsymmetricManager;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyPair;

@Component
public class LoginHandler extends AbstractHandler{

    @Autowired
    private ECCAsymmetricManager eccAsymmetricManager;

    public LoginHandler() {
    }

    public RequestResult<String> handle(String username, String password, KeyPair cryptoKeys) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl())
                .post(formAuthRequest(username, password, "", cryptoKeys))
                .build();
        Response response = client.newCall(request).execute();

        return formRequestResult(response);
    }

    public HttpUrl getUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("login")
                .build();
    }

    private RequestBody formAuthRequest(String username, String password, String email, KeyPair cryptoKeys){
        byte[] rawAuthRequest = AuthRequest.builder()
                .username(username)
                .password(password)
                .email(email)
                .localAddress(getLocalOutgoingAddress().getHostAddress())
                .localUdpPort(Integer.parseInt(properties.getProperty("localPort")))
                .cryptoPublicKey(eccAsymmetricManager.serializePublicKey(cryptoKeys.getPublic()))
                .build()
                .toJson()
                .getBytes();
        return RequestBody.create(MediaType.parse("application/json"), rawAuthRequest);
    }
}
