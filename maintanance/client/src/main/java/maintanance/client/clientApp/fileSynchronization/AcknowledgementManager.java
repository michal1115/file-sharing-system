package maintanance.client.clientApp.fileSynchronization;

import lombok.SneakyThrows;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.utils.DynamicPropertiesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class AcknowledgementManager extends Thread{

    /*private DynamicPropertiesManager properties;

    private int sendInterval;

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    @Autowired
    private ClientApp clientApp;

    @SneakyThrows
    public AcknowledgementManager(){
        properties = new DynamicPropertiesManager("dynamic.properties");
        sendInterval =
                Integer.parseInt(properties.getProperty("acknowledgement_resend_interval_seconds"));
    }

    @PostConstruct
    public void postConstruct(){
        //SWITCHED OFF
        //this.start();
    }

    @Override
    public void run() {
        while(true){
            if (clientApp.isLogged()) {
                sendAcknowledgements();
                sendNotAcknowledgedFileParts();
            }

            try {
                Thread.sleep(sendInterval * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendAcknowledgements(){
        repositoryWrapper.getReceivedPacketRepository()
                .findNotAcknowledged()
                .forEach(receivedPacket -> {
                    String username = clientApp.getUsername();
                    String ipv4Addr = receivedPacket.getSender().getIpAddr();
                    Integer port = receivedPacket.getSender().getPort();

                    try {
                        UDPLocalSynchronizer.sendAcknowledgement(username, receivedPacket.getPacketUUID(), ipv4Addr, port);
                        receivedPacket.setIsAckDelivered(true);
                        repositoryWrapper.getReceivedPacketRepository().save(receivedPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void sendNotAcknowledgedFileParts(){
        repositoryWrapper.getSentPacketRepository()
                .findNotDelivered()
                .forEach(sentPacket -> {
                    String ipv4Addr = sentPacket.getReceiver().getIpAddr();
                    Integer port = sentPacket.getReceiver().getPort();

                    try {
                        UDPLocalSynchronizer.sendFilePartPacket(sentPacket, ipv4Addr, port, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }*/
}
