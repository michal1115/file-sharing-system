package maintanance.client.clientApp.fileSynchronization;

import lombok.SneakyThrows;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.Team;
import maintanance.client.proto.proxy.messages.FileProxyMessage;
import maintanance.client.clientApp.exceptions.*;
import maintanance.client.model.entities.File;
import maintanance.client.model.entities.FileVersion;
import maintanance.client.model.entities.User;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.steganography.SteganoManager;
import maintanance.client.utils.ConnectionType;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.FileManager;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class FilePartManager extends Thread {
    //this class is used for storing packages and notifying if file can be completely retained from packages
    private int receivedFilesMonitorBreak = 200;

    private FilePartBuffer filePartBuffer;
    private Set<FileCompletionStatus> fileCompletionStatusSet = new HashSet<>();
    private RepositoryWrapper repositoryWrapper;
    private ClientApp clientApp;

    private UDPLocalSynchronizer UDPLocalSynchronizer;

    private DynamicPropertiesManager properties;

    private long lastSynchronizationTime = 0;

    private long synchronizationInterval;

    public FilePartManager(FilePartBuffer filePartBuffer, UDPLocalSynchronizer UDPLocalSynchronizer, RepositoryWrapper repositoryWrapper, ClientApp clientApp) throws IOException {
        properties = new DynamicPropertiesManager("dynamic.properties");
        synchronizationInterval = 1000 * Long.parseLong(properties.getProperty("proxy_files_download_interval"));

        this.filePartBuffer = filePartBuffer;
        this.UDPLocalSynchronizer = UDPLocalSynchronizer;
        this.repositoryWrapper = repositoryWrapper;
        this.clientApp = clientApp;
    }

    @SneakyThrows
    @Override
    public void run(){
        while(!interrupted()){
            while (!filePartBuffer.isEmpty()){
                FilePart nextFilePart = filePartBuffer.take();

                FileCompletionStatus fileCompletionStatus = null;
                Optional<FileCompletionStatus> fileCompletionStatusOptional = fileCompletionStatusSet.stream()//find one from set
                        .filter(status -> status.equals(new FileCompletionStatus(nextFilePart)))//do team name, file and user equal
                        .findFirst();

                if (!fileCompletionStatusOptional.isPresent()){
                    fileCompletionStatus = new FileCompletionStatus(nextFilePart);
                    fileCompletionStatusSet.add(fileCompletionStatus);
                } else {
                    fileCompletionStatus = fileCompletionStatusOptional.get();
                    fileCompletionStatus.addFilePart(nextFilePart);
                }

                if (fileCompletionStatus.isFileCompleted()){
                    System.out.println("----------------------------------------------------------");
                    System.out.println("FILE DOWNLOAD COMPLETED");
                    fileCompletionStatusSet.remove(fileCompletionStatus);

                    Optional<File> relatedPublicFile = repositoryWrapper.getFileRepository()
                            .findByPublicNameAndTeamName(fileCompletionStatus.getPublicFileName(), fileCompletionStatus.getTeamName());
                    if (relatedPublicFile.isPresent()) {
                        System.out.println("NO STEGANO");
                        //direct, "non important" file was sent
                        Optional<FileVersion> relatedFileVersion = repositoryWrapper.getFileVersionRepository()
                                .findUsersFileVersion(
                                        fileCompletionStatus.getPublicFileName(),
                                        fileCompletionStatus.getUsername(),
                                        fileCompletionStatus.getTeamName());
                        if (!relatedFileVersion.isPresent()){
                            throw new UserLocalFilePathNotSpecifiedException();
                        }
                        Team relatedTeam = relatedFileVersion.get().getRelatedFile().getConnectedTeam();
                        if (fileCompletionStatus.doAllFilePartsHaveValidSecurityKey(relatedTeam.getSecurityKey())
                                || fileCompletionStatus.doAllFilePartsHaveValidSecurityKey(relatedTeam.getPreviousSecurityKey())){
                            System.out.println("CONTENT:");
                            System.out.println(new String(fileCompletionStatus.getFileContent()));

                            relatedFileVersion.get().setWasDisplayed(false);
                            repositoryWrapper.getFileVersionRepository().save(relatedFileVersion.get());
                            FileManager.writeToFile(relatedFileVersion.get().getFilePath(), fileCompletionStatus.getFileContent());
                        } else {
                            System.out.println("INCIDENT");
                            System.out.println(relatedTeam);
                            clientApp.reportIncident(
                                    relatedTeam.getTeamName(),
                                    "Invalid security key from user " + fileCompletionStatus.getUsername());
                        }
                    } else {
                        System.out.println("HIDDEN");
                        //invalid package or IMPORTANT FILE hidden in this file
                        String senderName = fileCompletionStatus.getUsername();
                        Optional<Long> keyOpt = repositoryWrapper.getUserRepository().getExchangeKeyByName(senderName);

                        if (keyOpt.isPresent()){
                            if (keyOpt.get() == 0){
                                throw new KeyExchangeNotFinishedException();
                            }
                            String hiddenFilePath = "tmp_" + UUID.randomUUID().toString();
                            FileManager.writeToFile(hiddenFilePath, fileCompletionStatus.getFileContent());

                            String outputFilePath = "tmp_" + UUID.randomUUID().toString();
                            boolean dumpedHiddenFile =
                                    SteganoManager.retrieveFile(keyOpt.get().toString(), hiddenFilePath, outputFilePath);

                            if (dumpedHiddenFile){
                                byte[] hiddenMessage = FileManager.getFileContent(outputFilePath);
                                filePartBuffer.put(new FilePart(hiddenMessage));
                            }
                        }

                    }
                    System.out.println("----------------------------------------------------------");
                }
            }
            synchronizeFilesFromProxyIfNecessary();
        }
    }

    private void synchronizeFilesFromProxyIfNecessary() throws IOException, InterruptedException {
        if (shouldDownloadFilesFromProxy()){
            downloadFilesFromProxy();
        }
    }

    private boolean shouldDownloadFilesFromProxy(){
        return System.currentTimeMillis() - lastSynchronizationTime > synchronizationInterval;
    }

    private void downloadFilesFromProxy() throws IOException, InterruptedException {
        ProxyFileSynchronizer.putPendingFilesToFilePartBuffer(filePartBuffer, repositoryWrapper);
        lastSynchronizationTime = System.currentTimeMillis();
    }

    public void sendFileContent(File file, String myUserName) throws UserLocalFilePathNotSpecifiedException {
        //find users file path:
        Optional<FileVersion> myFileVersionOptional = file.getFileVersions().stream()
                .filter(fileVersion -> fileVersion.getVersionOwner().getName().equals(myUserName))
                .findFirst();
        if (!myFileVersionOptional.isPresent()){
            throw new UserLocalFilePathNotSpecifiedException();
        }
        String myLocalFilePath = myFileVersionOptional.get().getFilePath();

        //send file content from found file path to all users
        file.getConnectedTeam().getParticipants().stream()
                .filter(participant -> !participant.getName().equals(myUserName))
                .forEach(participant -> {
                            if (participant.getConnectionType().equals(ConnectionType.DIRECT)) {
                                UDPLocalSynchronizer.send(
                                        myUserName,
                                        file.getPublicName(),
                                        myLocalFilePath,
                                        file.getConnectedTeam().getTeamName(),
                                        participant.getName(),
                                        participant.getLocalAddress(),
                                        participant.getLocalUdpPort(),
                                        getUsersPublicKey(participant)
                                );
                            } else if (participant.getConnectionType().equals(ConnectionType.PROXY)){
                                try {
                                    FileProxyMessage receiverDetails = FileProxyMessage.builder()
                                            .user(participant.getName())
                                            .teamName(file.getConnectedTeam().getTeamName())
                                            .fileName(file.getPublicName())
                                            .build();

                                    ProxyFileSynchronizer.sendFileContentToProxy(myLocalFilePath, receiverDetails);
                                } catch (IOException | InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                );
    }

    public void sendFileContentHidden(File file, String myUserName) throws UserLocalFilePathNotSpecifiedException, KeyExchangeNotFinishedException {
        //find users file path:
        Optional<FileVersion> myFileVersionOptional = file.getFileVersions().stream()
                .filter(fileVersion -> fileVersion.getVersionOwner().getName().equals(myUserName))
                .findFirst();
        if (!myFileVersionOptional.isPresent()){
            throw new UserLocalFilePathNotSpecifiedException();
        }
        String myLocalFilePath = myFileVersionOptional.get().getFilePath();

        //requireAllExchangeKeysNegotiated(file, myUserName); -- odkomentować jeśli znowu będzie mechanizm rzucający się gdy KTOKOLWIEK nie wymmienił klucza

        //send file content from found file path to all users
        file.getConnectedTeam().getParticipants().stream()
                .filter(participant -> !participant.getName().equals(myUserName))
                .filter(User::isActive)//do not send important files via server
                .filter(User::wasPublicKeyNegotiated)
                .forEach(participant -> {
                    if (participant.getConnectionType().equals(ConnectionType.PROXY)){

                    } else if (participant.getConnectionType().equals(ConnectionType.DIRECT)) {
                        UDPLocalSynchronizer.sendHidden(
                                myUserName,
                                file.getPublicName(),
                                myLocalFilePath,
                                file.getConnectedTeam().getTeamName(),
                                participant.getName(),
                                participant.getLocalAddress(),
                                participant.getLocalUdpPort(),
                                participant.getExchangeKey(),
                                getUsersPublicKey(participant)
                        );
                    }
                });
    }

    private PublicKey getUsersPublicKey(User user){
        try {
            return clientApp.getCryptoManager().deserializePublicKey(user.getCryptoPublicKey());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    private void requireAllExchangeKeysNegotiated(File file, String myUserName) throws KeyExchangeNotFinishedException {
        boolean notSpecifiedPublicKeys = file.getConnectedTeam().getParticipants().stream()
                .filter(participant -> !participant.getName().equals(myUserName))
                .filter(User::isActive)//don't check if inactive users have generated their key, the important message won't go through server anyway
                .anyMatch(participant -> participant.getExchangeKey() == 0);

        if (notSpecifiedPublicKeys){
            throw new KeyExchangeNotFinishedException();
        }
    }
}
