package maintanance.client.clientApp.main.handlers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import maintanance.client.clientApp.keyNegotiation.PendingKeyRecord;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.Invitation;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.model.entities.User;
import maintanance.client.model.repositories.TeamRepository;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetInvitationsHandler extends AbstractHandler{

    @Autowired
    private TeamRepository teamRepository;

    public RequestResult<List<Invitation>> handle() throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl())
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        RequestResult<String> result = formRequestResult(response);

        return new RequestResult<>(result.getStatusMessage(), parseBody(result.getData()));
    }

    public HttpUrl getUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("users")
                .addPathSegment(ClientApp.username)
                .addPathSegment("invitations")
                .build();
    }

    private List<Invitation> parseBody(String body){
        Gson gson = new Gson();
        return IterableUtils.toList(gson.fromJson(body, JsonArray.class)).stream()
                .map(JsonElement::getAsJsonObject)
                .map(this::parseSingleInvitation)
                .collect(Collectors.toList());
        /*Type invitationListType = new TypeToken<List<Invitation>>(){}.getType();
        return gson.fromJson(body, invitationListType);*/
    }

    private Invitation parseSingleInvitation(JsonObject invitationServerSideObject){
        Gson gson = new Gson();
        return Invitation.builder()
                .id(invitationServerSideObject.get("id").getAsInt())
                .sender(gson.fromJson(invitationServerSideObject.get("sender"), User.class))
                .receiver(gson.fromJson(invitationServerSideObject.get("receiver"), User.class))
                .relatedTeamName(invitationServerSideObject.get("relatedTeam").getAsJsonObject()
                        .get("teamName").getAsString())
                .build();
    }
}
