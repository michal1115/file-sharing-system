package maintanance.client.clientApp.fileSynchronization;

import lombok.Getter;
import lombok.Setter;
import maintanance.client.clientApp.exceptions.*;
import maintanance.client.clientApp.keyNegotiation.KeyExchangeManager;
import maintanance.client.clientApp.keyNegotiation.KeyMessageBuffer;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.File;
import maintanance.client.model.wrappers.RepositoryWrapper;

import java.io.IOException;
import java.net.DatagramSocket;

@Getter
@Setter
public class FileSynchronizer {
    private FilePartManager filePartManager;
    private KeyExchangeManager keyExchangeManager;
    private UDPLocalSynchronizer UDPLocalSynchronizer;

    public FileSynchronizer(DatagramSocket udpAccessSocket, String username, RepositoryWrapper repositoryWrapper, ClientApp clientApp) throws IOException {
        FilePartBuffer filePartBuffer = new FilePartBuffer();
        KeyMessageBuffer keyMessageBuffer = new KeyMessageBuffer();

        this.UDPLocalSynchronizer = new UDPLocalSynchronizer(udpAccessSocket, filePartBuffer, keyMessageBuffer, repositoryWrapper, clientApp);
        this.filePartManager = new FilePartManager(filePartBuffer, UDPLocalSynchronizer, repositoryWrapper, clientApp);
        this.keyExchangeManager = new KeyExchangeManager(repositoryWrapper.getUserRepository(), UDPLocalSynchronizer, username, keyMessageBuffer, clientApp);

        this.UDPLocalSynchronizer.start();//start listening and receiving files
        this.filePartManager.start();//start gathering packages and after all files parts are received, write to file
        this.keyExchangeManager.start();//start processing keyExchange messages
    }

    public void sendFileContent(File file, String myUserName) throws UserLocalFilePathNotSpecifiedException {
        filePartManager.sendFileContent(file, myUserName);
    }

    public void sendFileContentHidden(File file, String myUserName) throws UserLocalFilePathNotSpecifiedException, KeyExchangeNotFinishedException {
        filePartManager.sendFileContentHidden(file, myUserName);
    }

    public void interrupt(){
        UDPLocalSynchronizer.interrupt();
        filePartManager.interrupt();
        keyExchangeManager.interrupt();
    }
}
