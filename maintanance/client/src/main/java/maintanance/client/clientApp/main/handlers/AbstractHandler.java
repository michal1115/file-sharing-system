package maintanance.client.clientApp.main.handlers;

import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Handlery przyjmują argumenty do requesta i zwracają rezultat zapytania w postaci [StatusMessage, zwróconeBody]
 * Ma to na celu odseporowanie logiki pobierania danych i ich parsowania od tego jak pobrane dane z serwera mają
 * wpłynąć na stan klienta (ClientApp)
 */
public abstract class AbstractHandler {

    public static String ERROR_REASON_HEADER_KEY = "error-reason";
    protected static DynamicPropertiesManager properties;
    protected static String serverHostname;
    protected static int serverPort;
    static {
        try {
            properties = new DynamicPropertiesManager("dynamic.properties");
            serverHostname = properties.getProperty("serverAddr");
            serverPort = Integer.parseInt(properties.getProperty("serverPort"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean DEBUG_MODE = false;

    protected int getCode(String rawHttpMessage){
        return SocketHttpRequest.getCode(rawHttpMessage);
    }

    protected String getBody(String rawHttpMessage){
        return SocketHttpRequest.removeHttpHeader(rawHttpMessage);
    }

    protected StatusMessage getErrorReason(String rawHttpMessage){
        return SocketHttpRequest.getRequestStatus(rawHttpMessage);
    }

    protected InetAddress getLocalOutgoingAddress(){
        try(final DatagramSocket socket = new DatagramSocket()){
            InetAddress defaultGoogleDNSAddress = (InetAddress.getByName("8.8.8.8"));
            socket.connect(defaultGoogleDNSAddress, 0);
            return socket.getLocalAddress();
        } catch (SocketException | UnknownHostException e) {
            throw new RuntimeException("ERROR - CONNECTING TO GOOGLE DNS IN ORDER TO RETRIEVE DEFAULT ADDRESS FAILED");
        }
    }

    protected RequestResult<String> formRawRequestResult(String rawHttpMessage){
        int code = getCode(rawHttpMessage);
        String body = getBody(rawHttpMessage);
        if (code == 200){
            return new RequestResult<>(StatusMessage.OK, body);
        }
        StatusMessage errorReason = getErrorReason(rawHttpMessage);
        System.out.println(errorReason);
        return new RequestResult<>(errorReason, body);
    }

    protected RequestResult<String> formRequestResult(Response response) throws IOException {
        int code = response.code();
        String body = response.body().string();
        if (code == 200){
            return new RequestResult<>(StatusMessage.OK, body);
        }
        System.out.println(response.code());
        System.out.println(response.headers());
        StatusMessage errorReason = StatusMessage.valueOf(response.header(ERROR_REASON_HEADER_KEY));
        System.out.println(errorReason);
        return new RequestResult<>(errorReason, body);
    }

    protected RequestBody getEmptyRequestBody(){
        return RequestBody.create(MediaType.parse("application/binary"), "{\"a\": \"b\"}".getBytes());
    }
}
