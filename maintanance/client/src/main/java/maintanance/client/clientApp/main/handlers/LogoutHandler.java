package maintanance.client.clientApp.main.handlers;

import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;

@Component
public class LogoutHandler extends AbstractHandler{

    public RequestResult<?> handle() throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl())
                .post(getEmptyRequestBody())
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        System.out.println(request.method());
        Response response = client.newCall(request).execute();

        return formRequestResult(response);
    }

    public HttpUrl getUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("logOut")
                .build();
    }
}
