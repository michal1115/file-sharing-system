package maintanance.client.clientApp.fileSynchronization;

import java.util.LinkedList;
import java.util.List;

public class FilePartBuffer {
    private List<FilePart> fileParts = new LinkedList<>();

    public void put(FilePart filePart){
        synchronized (fileParts) {
            fileParts.add(filePart);
        }
    }
    public FilePart take(){
        synchronized (fileParts) {
            return fileParts.remove(0);
        }
    }
    public boolean isEmpty(){
        synchronized (fileParts) {
            return fileParts.isEmpty();
        }
    }
}
