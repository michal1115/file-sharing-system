package maintanance.client.clientApp.keyNegotiation;

import maintanance.client.clientApp.fileSynchronization.FilePart;

import java.util.LinkedList;
import java.util.List;

public class KeyMessageBuffer {
    private List<KeyExchangeMsg> msgs = new LinkedList<>();

    public void put(KeyExchangeMsg keyExchangeMsg){
        synchronized (msgs) {
            msgs.add(keyExchangeMsg);
        }
    }
    public KeyExchangeMsg take(){
        synchronized (msgs) {
            return msgs.remove(0);
        }
    }
    public boolean isEmpty(){
        synchronized (msgs) {
            return msgs.isEmpty();
        }
    }
}
