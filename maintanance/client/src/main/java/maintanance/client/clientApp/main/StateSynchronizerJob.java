package maintanance.client.clientApp.main;

import maintanance.client.clientApp.fileSynchronization.ProxyFileSynchronizer;
import maintanance.client.cryptography.asymmetric.ECCAsymmetricManager;
import maintanance.client.model.entities.*;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.utils.FileManager;
import maintanance.client.utils.RequestUrl;
import okhttp3.*;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class StateSynchronizerJob {

    private String generalFileVersionDirectoryLocation = "sharingFiles";

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    public void perform(List<User> usersList, List<Team> teamsList, List<Invitation> invitations, String myName) throws IOException, InterruptedException {
        saveNewUsers(usersList);

        saveCurrentPublicFilesToDatabase(teamsList);
        repositoryWrapper.getTeamRepository().saveAll(teamsList);
        saveCurrentPublicFilesToDatabaseWithTeams(teamsList);

        shiftSecurityKeysIfTerminated(teamsList);
        repositoryWrapper.getTeamRepository().saveAll(teamsList);

        saveCurrentPublicFilesToDatabase(teamsList);
        saveNewFileVersions(teamsList, myName);

        /*System.out.println("TEAMS:");
        IterableUtils.toList(repositoryWrapper.getTeamRepository().findAll()).stream()
                .map(Team::getId)
                .forEach(System.out::println);*/

        saveNewInvitationsToRepository(invitations);
    }

    private void saveNewUsers(List<User> usersList){
        usersList.forEach(userFromServer -> {
            findExistingUserRecord(userFromServer).ifPresent(existingUserRecord ->
                    updateDownloadedUserAccordingToExistingValues(existingUserRecord, userFromServer));
            saveUser(userFromServer);
        });
    }

    private Optional<User> findExistingUserRecord(User userRecordDownloadedFromServer){
        return repositoryWrapper.getUserRepository().findByName(userRecordDownloadedFromServer.getName());
    }

    private void updateDownloadedUserAccordingToExistingValues(User existingUserRecord, User userRecordDownloadedFromServer){
        if (!wasUserDisconnected(existingUserRecord, userRecordDownloadedFromServer)){
            userRecordDownloadedFromServer.setPrivateKey(existingUserRecord.getPrivateKey());
            userRecordDownloadedFromServer.setExchangeKey(existingUserRecord.getExchangeKey());
        }
    }

    private boolean wasUserDisconnected(User existingUserRecord, User userRecordDownloadedFromServer){
        return existingUserRecord.isActive() && !userRecordDownloadedFromServer.isActive();
    }

    private void saveUser(User userRecordDownloadedFromServer){
        repositoryWrapper.getUserRepository().save(userRecordDownloadedFromServer);
    }

    private void saveCurrentPublicFilesToDatabase(List<Team> teamsList){
        teamsList.forEach(team -> {
            team.getSharedFiles().forEach(file -> {
                //file.setConnectedTeam(team);
                repositoryWrapper.getFileRepository().save(file);
            });
        });
    }

    private void saveCurrentPublicFilesToDatabaseWithTeams(List<Team> teamsList){
        teamsList.forEach(team -> {
            team.getSharedFiles().forEach(file -> {
                file.setConnectedTeam(team);
                repositoryWrapper.getFileRepository().save(file);
            });
        });
    }

    private void saveNewFileVersions(List<Team> teamsList, String myName){
        teamsList.forEach(team -> {
            Set<File> sharedFiles = team.getSharedFiles();
            sharedFiles.forEach(file -> {
                Set<User> fileOwners = team.getParticipants().stream().filter(user -> !user.getName().equals(myName)).collect(Collectors.toSet());
                fileOwners.forEach(owner -> saveNewFileVersionIfNotExist(team, file, owner));
            });
        });
    }

    private void saveNewFileVersionIfNotExist(Team team, File file, User participant) {
        Path generalFileDir = Paths.get(generalFileVersionDirectoryLocation);
        createFileIfNotExists(generalFileDir, true);

        Path teamDirFilePath = Paths.get(generalFileVersionDirectoryLocation, team.getTeamName());
        createFileIfNotExists(teamDirFilePath, true);

        Path publicFileDirFilePath = Paths.get(generalFileVersionDirectoryLocation, team.getTeamName(), file.getPublicName());
        createFileIfNotExists(publicFileDirFilePath, true);

        Path userFileVersionPath = Paths.get(generalFileVersionDirectoryLocation, team.getTeamName(), file.getPublicName(), participant.getName());
        createFileIfNotExists(userFileVersionPath, false);

        Optional<FileVersion> fileVersion = repositoryWrapper.getFileVersionRepository()
                .findFileVersionByUserAndPublicFile(file, participant);
        if (!fileVersion.isPresent()){
            FileVersion newFileVersion = FileVersion.builder()
                    .versionOwner(participant)
                    .relatedFile(file)
                    .filePath(userFileVersionPath.toString())
                    .build();
            repositoryWrapper.getFileVersionRepository().save(newFileVersion);
            //repositoryWrapper.getFileRepository().save(file);
            //repositoryWrapper.getTeamRepository().save(team);
            //repositoryWrapper.getUserRepository().save(participant);
        }
    }

    private void createFileIfNotExists(Path path, boolean isDirectory) {
        try {
            java.io.File file = path.toFile();
            if (!file.exists()){
                if (isDirectory){
                    file.mkdir();
                } else {
                    file.createNewFile();
                }
            }
        } catch (IOException e) {
            System.out.println("FILE CANNOT BE CREATED ");
            e.printStackTrace();
        }
    }

    private void shiftSecurityKeysIfTerminated(List<Team> teamsList){
        teamsList.forEach(this::shiftSecurityKeyIfTerminated);
    }

    private void shiftSecurityKeyIfTerminated(Team newTeamState){
        Optional<Team> previousTeamState = repositoryWrapper.getTeamRepository().findByName(newTeamState.getTeamName());
        if (previousTeamState.isPresent()){
            String previousSecurityKey = previousTeamState.get().getSecurityKey();
            if (!newTeamState.getSecurityKey().equals(previousSecurityKey)){
                newTeamState.setPreviousSecurityKey(previousSecurityKey);
            }
        }
    }

    private void saveNewInvitationsToRepository(List<Invitation> newInvitations){
        //newInvitations.forEach(this::saveTeamRelatedWithInvitationToRepositoryIfNotExists);
        Collection<Invitation> existingInvitations = IterableUtils.toList(
                repositoryWrapper.getInvitationRepository().findAll());
        newInvitations.stream()
                .filter(invitation ->
                        existingInvitations.stream().map(Invitation::getId)
                                .noneMatch(id -> id.equals(invitation.getId())))
                .forEach(repositoryWrapper.getInvitationRepository()::save);
    }

    /*private void saveTeamRelatedWithInvitationToRepositoryIfNotExists(Invitation invitation){
        Team relatedTeam = invitation.getRelatedTeam();
        Optional<Team> alreadyExistingTeam = repositoryWrapper.getTeamRepository()
                .findByName(relatedTeam.getTeamName());
        if (!alreadyExistingTeam.isPresent()){
            repositoryWrapper.getTeamRepository().save(relatedTeam);
        }
    }*/

    public static void main(String[] args) throws IOException {
    }
}