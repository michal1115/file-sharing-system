package maintanance.client.clientApp.main.handlers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.User;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

@Component
public class AddFileToSharedHandler extends AbstractHandler{

    public RequestResult<String> handle(String publicFileName, String teamName) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl(publicFileName, teamName))
                .put(getEmptyRequestBody())
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        return formRequestResult(response);
    }

    public HttpUrl getUrl(String publicFileName, String teamName){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("teams")
                .addPathSegment(teamName)
                .addPathSegment("files")
                .addPathSegment(publicFileName)
                .build();
    }
}
