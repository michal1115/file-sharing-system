package maintanance.client.clientApp.keyNegotiation;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;

import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaTray;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class ProxyKeyExchanger {

    private static DynamicPropertiesManager properties;
    static {
        try {
            properties = new DynamicPropertiesManager("dynamic.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<PendingKeyRecord> downloadKeysFromProxy() throws IOException, InterruptedException {
        /*Socket serverAccessSocket = SocketHttpRequest.openServerSocket();

        String url = RequestUrl.builder()
                .path("/getKeys")
                .build()
                .toString();

        String rawResult = SocketHttpRequest.sendRequestAuthorized(url, serverAccessSocket);
        String reqResult = SocketHttpRequest.removeHttpHeader(rawResult);

        SocketHttpRequest.closeServerSocket(serverAccessSocket);*/
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getKeysUrl())
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        String reqResult = response.body().string();
        if (reqResult.isEmpty()){
            return new LinkedList<>();
        }
        LinkedList<PendingKeyRecord> keysToReturn = new LinkedList<>();
        Gson gson = new Gson();
        JsonArray receivedKeysList = gson.fromJson(reqResult, JsonArray.class);
        receivedKeysList.forEach(key -> {
            JsonObject jsonValuesMap = key.getAsJsonObject();
            PendingKeyRecord keyRecord = PendingKeyRecord.builder()
                    .senderName(jsonValuesMap
                            .get("sender").getAsJsonObject()
                            .get("name").getAsString())
                    .receiverName(jsonValuesMap
                            .get("receiver").getAsJsonObject()
                            .get("name").getAsString())
                    .key(jsonValuesMap
                            .get("key").getAsString())
                    .build();
            keysToReturn.add(keyRecord);
        });
        return keysToReturn;
    }

    public static void sendPublicKeyViaProxy(User receiver, String key) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(putKeyUrl(receiver.getName()))
                .put(RequestBody.create(MediaType.parse("application/binary"), key.getBytes()))
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
    }

    /*public static void sendPublicKeyViaProxy(User receiver, String key) throws IOException, InterruptedException {
        Socket serverAccessSocket = SocketHttpRequest.openServerSocket();

        String url = RequestUrl.builder()
                .path("/putKeys")
                .param("receiver", receiver.getName())
                .build()
                .toString();

        String reqResult = SocketHttpRequest.removeHttpHeader(
                SocketHttpRequest.sendRequestAuthorized(url, SocketHttpRequest.jwt, key.getBytes(), serverAccessSocket));
        SocketHttpRequest.closeServerSocket(serverAccessSocket);
    }*/

    /*public RequestResult<List<Team>> handle() throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl())
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        RequestResult<String> requestResult = formRequestResult(response);
        return new RequestResult<>(requestResult.getStatusMessage(), parseBody(requestResult.getData()));
    }*/


    public static HttpUrl getKeysUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("serverAddr"))
                .port(Integer.parseInt(properties.getProperty("serverPort")))
                .addPathSegment("proxyKeys")
                .build();
    }

    public static HttpUrl putKeyUrl(String receiverName){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("serverAddr"))
                .port(Integer.parseInt(properties.getProperty("serverPort")))
                .addPathSegment("proxyKeys")
                .addPathSegment(receiverName)
                .build();
    }
}
