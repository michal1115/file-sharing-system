package maintanance.client.clientApp.fileSynchronization;
import lombok.Getter;
import lombok.Setter;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.IntStream;

@Setter
@Getter
public class FileCompletionStatus {
    private Map<Integer, byte[]> fileContentParts = new HashMap<>();
    private List<FilePart> fileParts = new LinkedList<>();

    private int totalPackages;
    private int totalFileSize;

    private String teamName;
    private String publicFileName;
    private String username;

    public FileCompletionStatus(FilePart filePart){
        this.teamName = filePart.getTeamName();
        this.publicFileName = filePart.getFileName();
        this.username = filePart.getUsername();

        this.totalFileSize = filePart.getFileSize();
        this.totalPackages = filePart.getTotalPackages();
        this.fileContentParts.put(filePart.getPackageIndex(), filePart.getFileContent());
    }

    public boolean isFileCompleted(){
        return IntStream.range(0, totalPackages)
                .allMatch(packetId -> fileContentParts.containsKey(packetId));//do all file indexes occur in fileContentsParts map
    }

    public void addFilePart(FilePart filePart){
        fileParts.add(filePart);
        fileContentParts.put(filePart.getPackageIndex(), filePart.getFileContent());
    }

    public boolean doAllFilePartsHaveValidSecurityKey(String securityKey){
        return fileParts.stream().allMatch(filePart -> filePart.getSecurityKey().equals(securityKey));
    }

    public byte[] getFileContent(){
        if (! isFileCompleted()){
            return null;
        }
        int totalPacketsLength = IntStream
                .range(0, totalPackages)
                .map(packageId -> fileContentParts.get(packageId).length)
                .reduce(Integer::sum)
                .getAsInt();

        byte[] fullPacketsContent = new byte[totalPacketsLength];
        ByteBuffer buff = ByteBuffer.wrap(fullPacketsContent);

        IntStream.range(0, totalPackages)
                .forEach(packetId -> buff.put(fileContentParts.get(packetId)));


        return Arrays.copyOfRange(buff.array(), 0, totalFileSize);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileCompletionStatus)) return false;
        FileCompletionStatus that = (FileCompletionStatus) o;
        return  Objects.equals(teamName, that.teamName) &&
                Objects.equals(publicFileName, that.publicFileName) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamName, publicFileName, username);
    }
}
