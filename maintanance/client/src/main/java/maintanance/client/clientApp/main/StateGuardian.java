package maintanance.client.clientApp.main;

import maintanance.client.clientApp.exceptions.PublicFileDoesNotExistException;
import maintanance.client.clientApp.exceptions.TeamDoesNotExistException;
import maintanance.client.clientApp.exceptions.UserDoesNotBelongToTeamException;
import maintanance.client.clientApp.exceptions.UserDoesNotExistException;
import maintanance.client.model.wrappers.RepositoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateGuardian {

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    public void requireUserIsTeamMember(String username, String teamName) throws UserDoesNotExistException, TeamDoesNotExistException, UserDoesNotBelongToTeamException {
        requireUserExistence(username);
        requireTeamExistence(teamName);
        if (!repositoryWrapper.getUserRepository().findByName(username).isPresent()){
            throw new UserDoesNotBelongToTeamException();
        }
    }

    public void requirePublicFileExistence(String publicFileName, String teamName) throws TeamDoesNotExistException, PublicFileDoesNotExistException {
        requireTeamExistence(teamName);
        if (!repositoryWrapper.getFileRepository().findByPublicNameAndTeamName(publicFileName, teamName).isPresent()){
            throw new PublicFileDoesNotExistException();
        }
    }

    public void requireTeamExistence(String teamName) throws TeamDoesNotExistException {
        if (!repositoryWrapper.getTeamRepository().findByName(teamName).isPresent()){
            throw new TeamDoesNotExistException();
        }
    }

    public void requireUserExistence(String userName) throws UserDoesNotExistException {
        if (!repositoryWrapper.getUserRepository().findByName(userName).isPresent()){
            throw new UserDoesNotExistException();
        }
    }
}
