package maintanance.client.clientApp.keyNegotiation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import maintanance.client.model.entities.User;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PendingKeyRecord {
    private String key;
    private String senderName;
    private String receiverName;
}