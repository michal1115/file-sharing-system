package maintanance.client.clientApp.main.handlers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.Team;
import maintanance.client.model.entities.User;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

@Component
public class MyTeamsDetailsHandler extends AbstractHandler{

    public RequestResult<List<Team>> handle() throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl())
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        RequestResult<String> requestResult = formRequestResult(response);
        return new RequestResult<>(requestResult.getStatusMessage(), parseBody(requestResult.getData()));
    }


    public HttpUrl getUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("teams")
                .build();
    }

    private List<Team> parseBody(String body){
        Gson gson = new Gson();
        Type teamsListType = new TypeToken<List<Team>>(){}.getType();
        return gson.fromJson(body, teamsListType);
    }
}
