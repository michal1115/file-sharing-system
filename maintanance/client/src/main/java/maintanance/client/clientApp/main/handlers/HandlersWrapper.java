package maintanance.client.clientApp.main.handlers;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HandlersWrapper {

    @Autowired
    private RegisterHandler registerHandler;

    @Autowired
    private LoginHandler loginHandler;

    @Autowired
    private IncidentsReportHandler incidentsReportHandler;

    @Autowired
    private IncidentsGetHandler incidentsGetHandler;

    @Autowired
    private UsersListHandler usersListHandler;

    @Autowired
    private TeamsListHandler teamsListHandler;

    @Autowired
    private MyTeamsDetailsHandler myTeamsDetailsHandler;

    @Autowired
    private CreateTeamHandler createTeamHandler;

    @Autowired
    private AddFileToSharedHandler addFileToSharedHandler;

    @Autowired
    private AddUserToTeamHandler addUserToTeamHandler;

    @Autowired
    private AnswerToInvitationHandler answerToInvitationHandler;

    @Autowired
    private GetInvitationsHandler getInvitationsHandler;

    @Autowired
    private LogoutHandler logoutHandler;
}
