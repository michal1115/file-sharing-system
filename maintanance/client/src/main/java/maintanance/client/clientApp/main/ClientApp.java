package maintanance.client.clientApp.main;

import com.google.gson.Gson;
import maintanance.client.authentication.AuthResponse;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.dto.StatusMessage;
import maintanance.client.clientApp.exceptions.*;
import maintanance.client.clientApp.fileSynchronization.FileSynchronizer;
import maintanance.client.clientApp.main.handlers.HandlersWrapper;
import maintanance.client.cryptography.asymmetric.ECCAsymmetricManager;
import maintanance.client.model.entities.*;
import maintanance.client.model.queries.UserRepositoryQueries;
import maintanance.client.model.repositories.*;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.utils.SocketHttpRequest;
import maintanance.client.utils.DynamicPropertiesManager;
import maintanance.client.utils.FileManager;
import org.aspectj.weaver.ast.Var;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.io.IOException;
import java.net.*;
import java.security.*;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ClientApp {

    public static String LAST_LOGGED_USER_DB_VAR_KEY = "last_logged_user";

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private DataSource dataSource;

    @Autowired
    HandlersWrapper handlersWrapper;

    @Autowired
    private FileLocationCacheRepository fileLocationCacheRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private FileVersionRepository fileVersionRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ReceivedPacketRepository receivedPacketRepository;

    @Autowired
    private SentPacketRepository sentPacketRepository;

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    @Autowired
    private VariableRepository variableRepository;

    @Autowired
    private StateSynchronizerJob stateSynchronizerJob;

    @Autowired
    private StateGuardian stateGuardian;

    @Autowired
    private ECCAsymmetricManager cryptoManager;

    private DynamicPropertiesManager properties;

    private FileSynchronizer fileSynchronizer;

    DatagramSocket accessSocket;

    private Gson gson = new Gson();

    private boolean logged = false;
    public static String username;
    private String password;
    private String email;

    public static String jwt;

    public static PrivateKey cryptoPrivateKey;

    private User myUserRecord = null;
    private StateSynchronizer stateSynchronizer;

    public ClientApp() throws IOException {
        properties = new DynamicPropertiesManager("dynamic.properties");
    }

    public RequestResult<String> register(String username, String password, String email) throws IOException, InterruptedException {
        if (logged){
            return new RequestResult<>(StatusMessage.ALREADY_LOGGED_IN, null);
        }
        KeyPair cryptoKeys = generateNewCryptoKeys();
        RequestResult<String> requestResult = handlersWrapper.getRegisterHandler().handle(username, password, email, cryptoKeys);
        if (wasRequestSuccessful(requestResult)){
            AuthResponse authResponse = gson.fromJson(requestResult.getData(), AuthResponse.class);
            jwt = authResponse.getJwt();
            cryptoPrivateKey = cryptoKeys.getPrivate();
            SocketHttpRequest.setJwt(authResponse.getJwt());
            initializeStateSynchronization(username, password);
        }
        return requestResult;
    }

    public RequestResult<String> login(String username, String password) throws IOException, InterruptedException {
        if (logged){
            return new RequestResult<>(StatusMessage.ALREADY_LOGGED_IN, null);
        }
        KeyPair cryptoKeys = generateNewCryptoKeys();
        RequestResult<String> requestResult = handlersWrapper.getLoginHandler().handle(username, password, cryptoKeys);
        if (wasRequestSuccessful(requestResult)){
            AuthResponse authResponse = gson.fromJson(requestResult.getData(), AuthResponse.class);
            jwt = authResponse.getJwt();
            cryptoPrivateKey = cryptoKeys.getPrivate();
            SocketHttpRequest.setJwt(authResponse.getJwt());
            initializeStateSynchronization(username, password);
        }
        return requestResult;
    }

    private KeyPair generateNewCryptoKeys(){
        try {
            return cryptoManager.generateKeyPair();
        } catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }
    }

    private void initializeStateSynchronization(String username, String password) throws IOException, InterruptedException {
        accessSocket = openUdpSocket();

        ClientApp.username = username;
        this.password = password;
        logged = true;

        boolean userContextChanged = isLoggedInAsSameUserAsBefore(username);
        if (!userContextChanged) {
            changeDbContextToUser(username);
        }
        synchronizeState();
        myUserRecord = userRepository.findByName(username).get();
        if (!userContextChanged){
            synchronizeFileVersionsWithCache(username);
        }

        fileSynchronizer = new FileSynchronizer(accessSocket, username, repositoryWrapper, this);

        int synchronizationInterval = 1000 * Integer.parseInt(properties.getProperty("synchronization_interval"));
        this.stateSynchronizer = new StateSynchronizer(this, synchronizationInterval);
        stateSynchronizer.start();
    }

    private boolean isLoggedInAsSameUserAsBefore(String username){
        Optional<Variable> lastLoggedUser = variableRepository.findByKey(LAST_LOGGED_USER_DB_VAR_KEY);
        return lastLoggedUser.isPresent() && lastLoggedUser.get().getValue().equals(username);
    }

    private void changeDbContextToUser(String username) {
        Optional<Variable> lastLoggedUser = variableRepository.findByKey(LAST_LOGGED_USER_DB_VAR_KEY);
        if (!lastLoggedUser.isPresent()) {
            Variable currentlyLoggedUser = Variable.builder()
                    .key(LAST_LOGGED_USER_DB_VAR_KEY)
                    .value(username)
                    .id(1)
                    .build();
            variableRepository.save(currentlyLoggedUser);
            return;
        }
        String lastLoggedUsername = lastLoggedUser.get().getValue();

        System.out.println("---------------------------------------------------------------------------");
        System.out.println("LAST LOGGED IN:");
        System.out.println(lastLoggedUsername);
        if (!username.equals(lastLoggedUsername)){
            System.out.println("PERFORMING SWITCHING DB CONTEXT");
            //persistLastLoggedUsersTableContents(lastLoggedUsername);
            clearDb();
            //setCurrentUsersDatabaseContextFromBackup(username);
        }
        System.out.println("---------------------------------------------------------------------------");

        lastLoggedUser.get().setValue(username);
        variableRepository.save(lastLoggedUser.get());
    }

    private void persistLastLoggedUsersTableContents(String lastLoggedUsername) throws SQLException {
        repositoryWrapper.getReceivedPacketRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getSentPacketRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getFileVersionRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getFileRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getUserRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getTeamRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
        repositoryWrapper.getInvitationRepository().persistOtherUsersTableContent(dataSource, lastLoggedUsername);
    }

    private void clearDb(){
        teamRepository.findAll().forEach(team -> {
                team.setParticipants(null);
                teamRepository.save(team);
        });
        fileRepository.findAll().forEach(file -> {
            file.setConnectedTeam(null);
            fileRepository.save(file);
        });
        fileVersionRepository.findAll().forEach(fileVersion -> {
            fileVersion.setRelatedFile(null);
            fileVersion.setVersionOwner(null);
            fileVersionRepository.save(fileVersion);
        });
        receivedPacketRepository.findAll().forEach(receivedPacket -> {
            receivedPacket.setSender(null);
            receivedPacketRepository.save(receivedPacket);
        });
        sentPacketRepository.findAll().forEach(sentPacket -> {
            sentPacket.setReceiver(null);
            sentPacket.setSender(null);
            sentPacket.setPublicFile(null);
            sentPacket.setTeam(null);
            sentPacketRepository.save(sentPacket);
        });
        repositoryWrapper.getInvitationRepository().findAll().forEach(invitation -> {
            invitation.setReceiver(null);
            invitation.setSender(null);
            invitation.setRelatedTeamName(null);
            repositoryWrapper.getInvitationRepository().save(invitation);
        });
        repositoryWrapper.getReceivedPacketRepository().deleteAll();
        repositoryWrapper.getSentPacketRepository().deleteAll();
        repositoryWrapper.getFileVersionRepository().deleteAll();
        repositoryWrapper.getFileRepository().deleteAll();
        repositoryWrapper.getUserRepository().deleteAll();
        repositoryWrapper.getTeamRepository().deleteAll();
        repositoryWrapper.getInvitationRepository().deleteAll();
    }

    private void setCurrentUsersDatabaseContextFromBackup(String username) throws SQLException {
        repositoryWrapper.getUserRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getTeamRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getFileRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getFileVersionRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getReceivedPacketRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getSentPacketRepository().persistCurrentUserTableContent(dataSource, username);
        repositoryWrapper.getInvitationRepository().persistCurrentUserTableContent(dataSource, username);
    }

    private void synchronizeFileVersionsWithCache(String username){
        fileLocationCacheRepository.findByOwner(username).forEach(cacheEntry -> {
            Optional<User> relatedUser = userRepository.findByName(cacheEntry.getUsername());
            Optional<Team> relatedTeam = teamRepository.findByName(cacheEntry.getTeamName());
            Optional<File> relatedFile = fileRepository
                    .findByPublicNameAndTeamName(cacheEntry.getFilePublicName(), cacheEntry.getTeamName());
            if (relatedUser.isPresent() && relatedTeam.isPresent() && relatedFile.isPresent()){
                FileVersion newFileVersion = FileVersion.builder()
                        .filePath(cacheEntry.getFilePath())
                        .relatedFile(relatedFile.get())
                        .versionOwner(relatedUser.get())
                        .build();
                System.out.println();
                System.out.println("DEBUG: CACHE-CONTROL");
                System.out.println("USER:" + cacheEntry.getUsername());
                System.out.println("FILE: " + cacheEntry.getFilePublicName());
                System.out.println("TEAM: " + cacheEntry.getTeamName());
                System.out.println(cacheEntry.getFilePath());
                System.out.println("LOADED FROM CACHE");
                System.out.println();
                fileVersionRepository.save(newFileVersion);
            }
        });
    }

    public RequestResult<?> reportIncident(String teamName, String incident) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getIncidentsReportHandler().handle(teamName, incident);
    }

    public RequestResult<Set<String>> getIncidents(String teamName) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getIncidentsGetHandler().handle(teamName);
    }

    public RequestResult<List<User>> usersList() throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getUsersListHandler().handle();
    }

    public RequestResult<List<String>> teamsList() throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getTeamsListHandler().handle();
    }

    public RequestResult<List<Team>> myTeamsDetails() throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getMyTeamsDetailsHandler().handle();
    }

    public RequestResult<String> createTeam(String teamName) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getCreateTeamHandler().handle(teamName);
    }

    public RequestResult<String> addFileToShared(String localFilePath, String publicFileName, String teamName) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        //optimization purpose, return "false" if user did not accept invitation to this team
        Optional<Team> relatedTeam = teamRepository.findByName(teamName);
        if (!relatedTeam.isPresent()){
            return new RequestResult<>(StatusMessage.YOU_ARE_NOT_TEAM_MEMBER, null);
        }

        RequestResult<String> requestResult = handlersWrapper.getAddFileToSharedHandler().handle(publicFileName, teamName);
        if (wasRequestSuccessful(requestResult)){
            synchronizeState();

            File publicFile = fileRepository.findByPublicNameAndTeamName(publicFileName, teamName).get();
            FileVersion fileLocalVersion = new FileVersion(localFilePath, publicFile, myUserRecord);

            fileRepository.save(publicFile);
            userRepository.save(myUserRecord);
            teamRepository.save(relatedTeam.get());

            synchronizeState();

            fileVersionRepository.save(fileLocalVersion);
            cacheFileLocations(publicFileName, teamName, localFilePath);
        }
        return requestResult;
    }


    public RequestResult<String> addUserToTeam(String teamName, String userToAdd) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        //optimization purpose, return "false" if user did not accept invitation to this team
        Optional<Team> relatedTeam = teamRepository.findByName(teamName);
        if (!relatedTeam.isPresent()){
            return new RequestResult<>(StatusMessage.YOU_ARE_NOT_TEAM_MEMBER, null);
        }
        return handlersWrapper.getAddUserToTeamHandler().handle(teamName, userToAdd);

    }
    public RequestResult<List<Invitation>> getInvitations() throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return handlersWrapper.getGetInvitationsHandler().handle();
    }

    public RequestResult<String> answerToInvitation(String teamName, boolean decision) throws IOException, InterruptedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        RequestResult<String> requestResult = handlersWrapper.getAnswerToInvitationHandler().handle(teamName, decision);
        if (wasRequestSuccessful(requestResult)){
            repositoryWrapper.getInvitationRepository().findInvitationByReceiverNameAndTeamName(username, teamName)
                    .ifPresent(repositoryWrapper.getInvitationRepository()::delete);
            if (decision) {
                synchronizeState();
            }
        }
        return requestResult;
    }

    public List<Map<String, String>> getMyFilesLocations() throws NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return StreamSupport.stream(fileVersionRepository.findAll().spliterator(), false)
                .filter(fileVersion -> fileVersion
                            .getVersionOwner()
                            .getName()
                            .equals(username)
                )
                .map(fileVersion -> {
                    Map<String, String> filesLocations = new HashMap<>();
                    System.out.println(fileVersion);
                    System.out.println(fileVersion.getRelatedFile());
                    System.out.println(fileVersion.getRelatedFile());
                    System.out.println(fileVersion.getRelatedFile().getConnectedTeam());
                    System.out.println(fileVersion.getRelatedFile().getConnectedTeam().getTeamName());
                    filesLocations.put("teamName", fileVersion.getRelatedFile().getConnectedTeam().getTeamName());
                    filesLocations.put("filePublicName", fileVersion.getRelatedFile().getPublicName());
                    filesLocations.put("filePath", fileVersion.getFilePath());
                    return filesLocations;
                })
                .collect(Collectors.toList());
    }

    public List<Map<String, String>> getNotLocatedFiles() throws NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        return StreamSupport.stream(
                fileRepository.findAll().spliterator(),
                false)
                .filter(file ->
                    file.getFileVersions().stream()
                            .noneMatch(fileVersion -> fileVersion
                                    .getVersionOwner()
                                    .getName()
                                    .equals(username)
                            )//there is no LocalFileVersion of currently logged user
                )
                .map(file -> {
                    Map<String, String> nonLocatedFiles = new HashMap<>();
                    nonLocatedFiles.put("teamName", file.getConnectedTeam().getTeamName());
                    nonLocatedFiles.put("filePublicName", file.getPublicName());
                    return nonLocatedFiles;
                })
                .collect(Collectors.toList());
    }

    public void locateFile(String filePublicName, String teamName, String path) throws TeamDoesNotExistException, PublicFileDoesNotExistException, UserDoesNotBelongToTeamException, UserDoesNotExistException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        stateGuardian.requirePublicFileExistence(filePublicName, teamName);
        stateGuardian.requireUserIsTeamMember(username, teamName);

        FileVersion newFileVersion = fileVersionRepository.findUsersFileVersion(filePublicName, username, teamName)
                .map(fileVersion -> {
                    fileVersion.setFilePath(path);
                    return fileVersion;
                })
                .orElse(createNewCurrentUserFileVersion(filePublicName, teamName, path));
        fileVersionRepository.save(newFileVersion);
        cacheFileLocations(filePublicName, teamName, path);
        refreshUser();
    }

    private void cacheFileLocations(String filePublicName, String teamName, String path){
        Optional<FileLocationCache> cachedFileLocation =
                fileLocationCacheRepository.findByOwnerFilePublicNameAndTeam(username, teamName, filePublicName);
        if (cachedFileLocation.isPresent()){
            cachedFileLocation.get().setFilePath(path);
            fileLocationCacheRepository.save(cachedFileLocation.get());
        } else {
            fileLocationCacheRepository.save(
                    FileLocationCache.builder()
                            .username(username)
                            .filePath(path)
                            .teamName(teamName)
                            .filePublicName(filePublicName)
                            .build()
            );
        }
    }

    private FileVersion createNewCurrentUserFileVersion(String filePublicName, String teamName, String path) throws TeamDoesNotExistException, PublicFileDoesNotExistException, UserDoesNotBelongToTeamException, UserDoesNotExistException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        stateGuardian.requirePublicFileExistence(filePublicName, teamName);
        stateGuardian.requireUserIsTeamMember(username, teamName);

        Optional<File> publicFile = fileRepository.findByPublicNameAndTeamName(filePublicName, teamName);
        Optional<User> currentUser = userRepository.findByName(username);
        return FileVersion.builder()
                .relatedFile(publicFile.get())
                .versionOwner(currentUser.get())
                .filePath(path)
                .build();
    }

    public void replaceFileVersion(String teamName, String publicFileName, String otherUser) throws TeamDoesNotExistException, PublicFileDoesNotExistException, FileVersionDoesNotExistException, UserLocalFilePathNotSpecifiedException, IOException, UserDoesNotBelongToTeamException, UserDoesNotExistException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        stateGuardian.requireUserIsTeamMember(username, teamName);
        stateGuardian.requireUserIsTeamMember(otherUser, teamName);
        stateGuardian.requirePublicFileExistence(publicFileName, teamName);

        Optional<FileVersion> otherUserFileVersion =
                fileVersionRepository.findUsersFileVersion(publicFileName, otherUser, teamName);
        if (!otherUserFileVersion.isPresent()){
            throw new FileVersionDoesNotExistException();
        }

        Optional<FileVersion> currentUserFileVersion =
                fileVersionRepository.findUsersFileVersion(publicFileName, username, teamName);
        if (!currentUserFileVersion.isPresent()){
            throw new UserLocalFilePathNotSpecifiedException();
        }

        byte[] otherUserFileContent = FileManager.getFileContent(otherUserFileVersion.get().getFilePath());
        FileManager.writeToFile(currentUserFileVersion.get().getFilePath(), otherUserFileContent);
    }

    public void sendFileStateNotHidden(String teamName, String publicFileName) throws UserLocalFilePathNotSpecifiedException, KeyExchangeNotFinishedException, NotLoggedInException {
        sendFileState(teamName, publicFileName, file -> fileSynchronizer.sendFileContent(file, username));
    }

    public void sendFileStateHidden(String teamName, String publicFileName) throws UserLocalFilePathNotSpecifiedException, KeyExchangeNotFinishedException, NotLoggedInException {
        sendFileState(teamName, publicFileName, file -> fileSynchronizer.sendFileContentHidden(file, username));
    }

    private void sendFileState(String teamName, String publicFileName, SendFileConsumer<File> sendFileOperation) throws KeyExchangeNotFinishedException, UserLocalFilePathNotSpecifiedException, NotLoggedInException {
        if (!logged){
            throw new NotLoggedInException();
        }
        System.out.println(fileRepository);
        Optional<File> fileToBeSent = fileRepository.findByPublicNameAndTeamName(publicFileName, teamName);
        System.out.println(fileToBeSent);
        if (fileToBeSent.isPresent()){
            sendFileOperation.accept(fileToBeSent.get());
        }
    }

    private DatagramSocket openUdpSocket() throws IOException {
        return new DatagramSocket(Integer.parseInt(properties.getProperty("localPort")));
    }

    public void synchronizeState() throws IOException, InterruptedException {
        try {
            stateSynchronizerJob.perform(usersList().getData(), myTeamsDetails().getData(), getInvitations().getData(),
                    username);
        } catch (NotLoggedInException e) {
            throw new RuntimeException(e);
        }

    }

    public void logout() throws NotLoggedInException, IOException, InterruptedException {
        if (!logged){
            throw new NotLoggedInException();
        }

        stopAllThreadJobs();

        handlersWrapper.getLogoutHandler().handle();
        accessSocket.close();

        ClientApp.username = null;
        this.password = null;
        this.logged = false;
        jwt = null;
        cryptoPrivateKey = null;

        this.myUserRecord = null;

        //fileVersionRepository.deleteAll();
        //fileRepository.deleteAll();
        //teamRepository.deleteAll();
        //userRepository.deleteAll();
        performDbChangeOnLogout();
    }

    private void performDbChangeOnLogout(){
        clearExchangeSymmetricKeysData();
    }

    private void clearExchangeSymmetricKeysData(){
        repositoryWrapper.getUserRepository().findAll().forEach(user -> {
            user.setExchangeKey(0);
            user.setPrivateKey(0);
            repositoryWrapper.getUserRepository().save(user);
        });
    }

    private boolean wasRequestSuccessful(RequestResult<?> requestResult){
        return requestResult.getStatusMessage().equals(StatusMessage.OK);
    }

    private void stopAllThreadJobs(){
        stateSynchronizer.interrupt();
        fileSynchronizer.interrupt();
    }

    private void refreshUser(){
        this.myUserRecord = userRepository.findByName(username).get();
    }

    private InetAddress getLocalOutgoingAddress(){
        try(final DatagramSocket socket = new DatagramSocket()){
            InetAddress defaultGoogleDNSAddress = (InetAddress.getByName("8.8.8.8"));
            socket.connect(defaultGoogleDNSAddress, 0);
            return socket.getLocalAddress();
        } catch (SocketException | UnknownHostException e) {
            throw new RuntimeException("ERROR - CONNECTING TO GOOGLE DNS IN ORDER TO RETRIEVE DEFAULT ADDR FAILED");
        }
    }

    public ECCAsymmetricManager getCryptoManager(){
        return cryptoManager;
    }

    public String getUsername(){
        return this.username;
    }
    public boolean isLogged(){
        return this.logged;
    }
}
