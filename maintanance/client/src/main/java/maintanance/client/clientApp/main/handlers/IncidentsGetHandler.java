package maintanance.client.clientApp.main.handlers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.utils.RequestUrl;
import maintanance.client.utils.SocketHttpRequest;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;


@Component
@Getter
public class IncidentsGetHandler extends AbstractHandler {

    public RequestResult<Set<String>> handle(String relatedTeamName) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getUrl(relatedTeamName))
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        RequestResult<String> rawRequestResult = formRequestResult(response);
        return new RequestResult<>(rawRequestResult.getStatusMessage(), parseBody(rawRequestResult.getData()));
    }

    public HttpUrl getUrl(String relatedTeamName){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverHostname)
                .port(serverPort)
                .addPathSegment("teams")
                .addPathSegment(relatedTeamName)
                .addPathSegment("incidents")
                .build();
    }

    private Set<String> parseBody(String body){
        Gson gson = new Gson();
        Type stringSetType = new TypeToken<HashSet<String>>(){}.getType();
        return gson.fromJson(body, stringSetType);
    }
}
