package maintanance.client.clientApp.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@Builder
@ToString
public class RequestResult<T> {
    private StatusMessage statusMessage;
    private T data;
}
