package maintanance.client.clientApp.cmd;

import lombok.SneakyThrows;
import maintanance.client.clientApp.dto.RequestResult;
import maintanance.client.clientApp.exceptions.*;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.Invitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;


@Component
public class UserListener extends Thread{

    private Map<String, UserAction> regexMap = new HashMap<>();
    {
        regexMap.put("[\\s]*register[\\s]+[\\w]+[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",            UserAction.REGISTER);//register username password email
        regexMap.put("[\\s]*login[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",                           UserAction.LOGIN);//login username password
        regexMap.put("[\\s]*users_list[\\s]*",                                              UserAction.USERS_LIST);//users_list
        regexMap.put("[\\s]*teams_list[\\s]*",                                              UserAction.TEAMS_LIST);//teams_list
        regexMap.put("[\\s]*my_teams_details[\\s]*",                                        UserAction.MY_TEAMS_DETAILS);//my_teams_details
        regexMap.put("[\\s]*create_team[\\s]+[\\w]+[\\s]*",                                 UserAction.CREATE_TEAM);//create_team team_name
        regexMap.put("[\\s]*share[\\s]*",                                                   UserAction.ADD_FILE_TO_SHARED);//share
        regexMap.put("[\\s]*add_user_to_team[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",                UserAction.ADD_USER_TO_TEAM);//add_user_to_group team_name username
        regexMap.put("[\\s]*invitations[\\s]*",                                             UserAction.INVITATIONS);//invitations
        regexMap.put("[\\s]*answer_to_invitation[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",            UserAction.ANSWER_TO_INVITATION);//answer_to_invitation team_name decision
        regexMap.put("[\\s]*print_file_space[\\s]*",                                        UserAction.PRINT_FILE_SPACE);
        regexMap.put("[\\s]*my_file_locations[\\s]*",                                       UserAction.MY_FILE_LOCATIONS);
        regexMap.put("[\\s]*non_located_files[\\s]*",                                       UserAction.NON_LOCATED_FILES);
        regexMap.put("[\\s]*locate_file[\\s]*",                                             UserAction.LOCATE_FILE);//locate_file #map public file name to local path
        regexMap.put("[\\s]*send_file_state[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",                 UserAction.SEND_FILE_STATE);//send_file_state team_name public_file_name
        regexMap.put("[\\s]*send_file_state_hidden[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",          UserAction.SEND_FILE_STATE_HIDDEN);//send_file_state_hidden team_name public_file_name
        regexMap.put("[\\s]*replace_file_version[\\s]+[\\w]+[\\s]+[\\w]+[\\s]+[\\w]+[\\s]*",UserAction.REPLACE_FILE_VERSION);//replace_file_version team_name public_file_name user_name
        regexMap.put("[\\s]*report_incident[\\s]+[\\w]+[\\s]+[\\w]+",                       UserAction.REPORT_INCIDENT);//report_incidents team_name incident
        regexMap.put("[\\s]*get_incidents[\\s]+[\\w]+",                                     UserAction.GET_INCIDENTS);//get_incidents team_name
        regexMap.put("[\\s]*logout[\\s]*",                                                  UserAction.LOGOUT);
    }

    @Autowired
    private ClientApp clientApp;

    @SneakyThrows
    @Override
    public void run(){
        while(true){
            Scanner scanner = new Scanner(System.in);
            String userInput = scanner.nextLine().trim();
            Optional<UserAction> userAction = getUserAction(userInput);
            try {
                handleUserAction(userInput, userAction);
            } catch (NotLoggedInException e){
                System.out.println("You are not logged in!");
            }

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Optional<UserAction> getUserAction(String userInput){
        return regexMap.entrySet().stream()
                        .filter(entry -> userInput.matches(entry.getKey()))
                        .map(Map.Entry::getValue)
                        .findFirst();
    }

    private void handleUserAction(String userInput, Optional<UserAction> userAction) throws InterruptedException, NotLoggedInException, IOException, UserDoesNotExistException, TeamDoesNotExistException, FileVersionDoesNotExistException, PublicFileDoesNotExistException, UserLocalFilePathNotSpecifiedException, UserDoesNotBelongToTeamException, KeyExchangeNotFinishedException {
        if (userAction.isPresent()){
            if(userAction.get() == UserAction.REGISTER){
                String username = userInput.split(" ")[1];
                String password = userInput.split(" ")[2];
                String email = userInput.split(" ")[3];

                RequestResult<String> res = clientApp.register(username, password, email);
                System.out.println(res);
            }

            else if(userAction.get() == UserAction.LOGIN){
                String username = userInput.split(" ")[1];
                String password = userInput.split(" ")[2];

                RequestResult<String> res = clientApp.login(username, password);
                System.out.println(res);
            }

            else if(userAction.get() == UserAction.USERS_LIST){
                System.out.println();
                System.out.println(clientApp.usersList());
                clientApp.usersList().getData()
                        .forEach(user ->
                                System.out.println(
                                        user.getName()
                                                + " "
                                                + user.getIpAddr()
                                                + ":"
                                                + user.getPort()
                                                + " local: "
                                                + user.getLocalAddress()
                                                + ":"
                                                + user.getLocalUdpPort()
                                                + " ech: "
                                                + user.getExchangeKey()
                                                + " active: "
                                                + user.isActive()
                                )
                        );
                System.out.println();
            }
            else if(userAction.get() == UserAction.TEAMS_LIST){
                System.out.println(clientApp.teamsList());
            }
            else if (userAction.get() == UserAction.MY_TEAMS_DETAILS){
                System.out.println();
                System.out.println(clientApp.myTeamsDetails());
                clientApp.myTeamsDetails().getData().forEach(team ->{
                    System.out.println(team.getTeamName());
                    System.out.println("PARTICIPANTS:");
                    team.getParticipants().forEach(p -> System.out.println("\t" + p.getName()));
                    System.out.println("FILES:");
                    team.getSharedFiles().forEach(f -> System.out.println("\t" + f.getPublicName()));
                    System.out.println();
                });
                System.out.println();
            }
            else if (userAction.get() == UserAction.CREATE_TEAM){
                String team_name = userInput.split(" ")[1];

                System.out.println(clientApp.createTeam(team_name));
            }
            else if (userAction.get() == UserAction.ADD_FILE_TO_SHARED){
                Scanner scanner = new Scanner(System.in);
                CmdFileChooser fileChooser = new CmdFileChooser();
                String chosenFile = fileChooser.search();

                System.out.println("Choose file public name");
                String filePublicName = scanner.nextLine().trim();

                System.out.println("Choose team name");
                String teamName = scanner.nextLine().trim();

                System.out.println(clientApp.addFileToShared(chosenFile, filePublicName, teamName));
            }
            else if (userAction.get() == UserAction.ADD_USER_TO_TEAM){
                String team_name = userInput.split(" ")[1];
                String username = userInput.split(" ")[2];

                System.out.println(clientApp.addUserToTeam(team_name, username));
            }
            else if (userAction.get() == UserAction.INVITATIONS){
                RequestResult<List<Invitation>> invitationSet = clientApp.getInvitations();
                System.out.println(invitationSet);
                invitationSet.getData().stream()
                        .map(Invitation::getRelatedTeamName)
                        .forEach(System.out::println);
            }
            else if (userAction.get() == UserAction.ANSWER_TO_INVITATION){
                String team_name = userInput.split(" ")[1];
                boolean decision = Boolean.parseBoolean(userInput.split(" ")[2]);

                System.out.println(clientApp.answerToInvitation(team_name, decision));
            }
            else if (userAction.get() == UserAction.PRINT_FILE_SPACE){
                System.out.println("NOT IMPLEMENTED");
            }
            else if (userAction.get() == UserAction.MY_FILE_LOCATIONS){
                System.out.println(clientApp.getMyFilesLocations());
            }
            else if (userAction.get() == UserAction.NON_LOCATED_FILES){
                System.out.println(clientApp.getNotLocatedFiles());
            }
            else if (userAction.get() == UserAction.LOCATE_FILE){
                Scanner scanner = new Scanner(System.in);
                System.out.println("Which team?:");
                String teamName = scanner.nextLine().trim();

                System.out.println("Which public file?:");
                String filePublicName = scanner.nextLine().trim();

                System.out.println("Provide file that will contain given file data");
                CmdFileChooser fileChooser = new CmdFileChooser();
                String chosenFile = fileChooser.search();

                clientApp.locateFile(filePublicName, teamName, chosenFile);
            }
            else if (userAction.get() == UserAction.SEND_FILE_STATE){
                String team_name = userInput.split(" ")[1];
                String public_file_name = userInput.split(" ")[2];
                try {
                    clientApp.sendFileStateNotHidden(team_name, public_file_name);
                }catch (Exception e){
                    System.out.println("Error while sending file version");
                }
            }
            else if (userAction.get() == UserAction.SEND_FILE_STATE_HIDDEN){
                String team_name = userInput.split(" ")[1];
                String public_file_name = userInput.split(" ")[2];
                clientApp.sendFileStateHidden(team_name, public_file_name);
            }
            else if (userAction.get() == UserAction.REPLACE_FILE_VERSION){
                String team_name = userInput.split(" ")[1];
                String public_file_name = userInput.split(" ")[2];
                String username = userInput.split(" ")[3];

                clientApp.replaceFileVersion(team_name, public_file_name, username);
            }
            else if (userAction.get() == UserAction.REPORT_INCIDENT){
                String team_name = userInput.split(" ")[1];
                String incident = userInput.split(" ")[2];

                System.out.println(clientApp.reportIncident(team_name, incident));
            }
            else if (userAction.get() == UserAction.GET_INCIDENTS){
                String team_name = userInput.split(" ")[1];

                System.out.println(clientApp.getIncidents(team_name));
            }
            else if (userAction.get() == UserAction.LOGOUT){
                try {
                    clientApp.logout();
                } catch( NotLoggedInException e){
                    System.out.println("You are not logged in!");
                }
            }
        }
        else{
            System.out.println("Invalid command");
        }
    }

    @PostConstruct
    private void go(){
        this.start();
    }
}
