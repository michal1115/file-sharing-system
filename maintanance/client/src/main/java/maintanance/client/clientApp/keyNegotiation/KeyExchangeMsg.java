package maintanance.client.clientApp.keyNegotiation;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@ToString
public class KeyExchangeMsg {
    private String username;
    private long publicKey;
}
