package maintanance.client.clientApp.fileSynchronization;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import maintanance.client.clientApp.main.ClientApp;
import maintanance.client.model.entities.File;
import maintanance.client.model.entities.FileVersion;
import maintanance.client.model.entities.User;
import maintanance.client.model.wrappers.RepositoryWrapper;
import maintanance.client.proto.proxy.messages.FileProxyMessage;
import maintanance.client.utils.*;
import okhttp3.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProxyFileSynchronizer {

    private static DynamicPropertiesManager properties;
    static {
        try {
            properties = new DynamicPropertiesManager("dynamic.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<FileProxyMessage> getPendingProxyFiles() throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getProxyFilesListUrl(ClientApp.username))
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        //System.out.println(url);
        String reqResult = response.body().string();

        List<FileProxyMessage> messages = new ArrayList<>();

        Gson gson = new Gson();
        JsonArray rawMessages = gson.fromJson(reqResult, JsonArray.class);
        rawMessages.forEach(message -> {
            String ownerName = message.getAsJsonObject()
                    .get("sender").getAsJsonObject()
                    .get("name").getAsString();
            String teamName = message.getAsJsonObject()
                    .get("associatedTeam").getAsJsonObject()
                    .get("teamName").getAsString();
            String fileName = message.getAsJsonObject()
                    .get("associatedFile").getAsJsonObject()
                    .get("publicName").getAsString();

            messages.add(FileProxyMessage.builder().user(ownerName).teamName(teamName).fileName(fileName).build());
        });
        return messages;
    }

    public static byte[] getFileContentViaProxy(FileProxyMessage fileProxyMessage) throws IOException, InterruptedException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(fileProxyUrl(ClientApp.username, fileProxyMessage.getTeamName(), fileProxyMessage.getFileName(), fileProxyMessage.getUser()))
                .get()
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        //System.out.println(url);
        byte[] reqResult = response.body().bytes();
        if (reqResult == null){
            return null;
        }
        return reqResult;
    }

    public static void sendFileContentToProxy(String filePath, FileProxyMessage fileProxyMessage) throws IOException, InterruptedException {
        byte[] fileContentToSend = FileManager.getFileContent(filePath);
        System.out.println(new String(fileContentToSend));

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(fileProxyUrl(fileProxyMessage.getUser(), fileProxyMessage.getTeamName(), fileProxyMessage.getFileName(), ClientApp.username))
                .put(RequestBody.create(MediaType.parse("application/binary"), fileContentToSend))
                .addHeader("Authorization", "Bearer " + ClientApp.jwt)
                .build();
        Response response = client.newCall(request).execute();
        //System.out.println(url);
        String reqResult = response.body().string();
        System.out.println("WAS SENT");
    }

    public static void sendNonExistingFileErrorToProxyServer(String fileName, String teamName) {
        //TODO implement both here and on server
    }

    public static void sendNonExistingUserErrorToProxyServer(String userName) {
        //TODO implement both here and on server
    }

    public static void sendInvalidSecurityKeyErrorToProxyServer(String teamName, String userName){
        //TODO implement both here and on server
    }

    public static void putPendingFilesToFilePartBuffer(FilePartBuffer filePartBuffer, RepositoryWrapper repositoryWrapper) throws IOException, InterruptedException {
        getPendingProxyFiles().forEach(pendingFileMessage -> {
            try {
                System.out.println("-----------------------------------------");
                System.out.println("FILE DOWNLOAD FROM PROXY:");
                System.out.println(pendingFileMessage.getFileName());
                System.out.println(pendingFileMessage.getTeamName());
                System.out.println(pendingFileMessage.getUser());
                System.out.println("-----------------------------------------");
                putDownloadedFileOnFilePartBuffer(pendingFileMessage, filePartBuffer, repositoryWrapper);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    private static void putDownloadedFileOnFilePartBuffer(FileProxyMessage message, FilePartBuffer filePartBuffer, RepositoryWrapper repositoryWrapper) throws IOException, InterruptedException {
        byte[] fileContent = getFileContentViaProxy(message);
        File publicFile = getRelatedFile(message, repositoryWrapper);
        User owner = getFileOwner(message, repositoryWrapper);
        FileVersion relatedFileVersion = getRelatedFileVersion(publicFile, owner, repositoryWrapper);
        filePartBuffer.put(createFilePart(fileContent, publicFile, owner, relatedFileVersion));
    }

    private static File getRelatedFile(FileProxyMessage proxyMessage, RepositoryWrapper repositoryWrapper){
        return repositoryWrapper.getFileRepository()
                .findByPublicNameAndTeamName(proxyMessage.getFileName(), proxyMessage.getTeamName())
                .orElseThrow(RuntimeException::new);
    }

    private static User getFileOwner(FileProxyMessage proxyMessage, RepositoryWrapper repositoryWrapper){
        return repositoryWrapper.getUserRepository().findByName(proxyMessage.getUser())
                .orElseThrow(RuntimeException::new);
    }

    private static FileVersion getRelatedFileVersion(File file, User owner, RepositoryWrapper repositoryWrapper){
        return repositoryWrapper.getFileVersionRepository()
                .findFileVersionByUserAndPublicFile(file, owner)
                .orElseThrow(RuntimeException::new);
    }

    private static FilePart createFilePart(byte[] content, File file, User owner, FileVersion fileVersion){
        return FilePart.builder()
                .fileContent(content)
                .fileName(file.getPublicName())
                .fileSize(content.length)
                .packageFileContentSize(content.length)
                .packageIndex(0)
                .securityKey(file.getConnectedTeam().getSecurityKey())//its downloaded from server so no its valid
                .teamName(file.getConnectedTeam().getTeamName())
                .totalPackages(1)
                .username(owner.getName())
                .build();
    }

    public static HttpUrl getProxyFilesListUrl(String receiver){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("serverAddr"))
                .port(Integer.parseInt(properties.getProperty("serverPort")))
                .addPathSegment("fileProxy")
                .addPathSegment("users")
                .addPathSegment(receiver)
                .build();
    }

    public static HttpUrl fileProxyUrl(String receiver, String team, String file, String sender){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("serverAddr"))
                .port(Integer.parseInt(properties.getProperty("serverPort")))
                .addPathSegment("fileProxy")
                .addPathSegment("users")
                .addPathSegment(receiver)
                .addPathSegment("teams")
                .addPathSegment(team)
                .addPathSegment("files")
                .addPathSegment(file)
                .addPathSegment("versions")
                .addPathSegment(sender)
                .build();
    }
}
