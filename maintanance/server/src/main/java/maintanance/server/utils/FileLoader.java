package maintanance.server.utils;

import java.io.*;

public class FileLoader {
    public static String getFileContent(String filePath) throws IOException {
        File file = new File(filePath);
        BufferedReader fileReader = new BufferedReader(new FileReader(file));
        StringBuilder resultBuilder = new StringBuilder();

        String nextLine = fileReader.readLine();
        while (nextLine != null){
            resultBuilder.append(nextLine).append("\n");
            nextLine = fileReader.readLine();
        }
        return resultBuilder.toString();

    }
    public static void main(String[] args) throws IOException {
        System.out.println(FileLoader.getFileContent("C:\\Users\\michal\\test.txt.txt"));
    }

}
