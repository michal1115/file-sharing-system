package maintanance.server.utils;


import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.Properties;

public class DynamicPropertiesManager {

    private Resource resource;

    private OutputStream fileOutputStream;
    private InputStream fileInputStream;

    private Properties properties;

    public DynamicPropertiesManager(String filePath) throws IOException {
        properties = new Properties();
        resource = new ClassPathResource(filePath);
    }

    @SneakyThrows
    public String getProperty(String key){
        if (fileInputStream == null){
            fileOutputStream = null;
            fileInputStream = resource.getInputStream();
            properties.load(fileInputStream);
        }
        return properties.getProperty(key);
    }
}
