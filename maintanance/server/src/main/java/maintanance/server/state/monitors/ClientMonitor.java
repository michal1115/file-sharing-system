package maintanance.server.state.monitors;

import lombok.SneakyThrows;
import maintanance.server.model.entities.User;
import maintanance.server.model.repositories.MessageRepository;
import maintanance.server.model.repositories.UserRepository;
import maintanance.server.utils.DynamicPropertiesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;

/**
 * Monitors users inactivity time and marks them as inactive if inactive time is exceeded
 */
@Component
public class ClientMonitor extends Thread{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    private long timeSeconds;
    Collection<User> inactiveUsers;

    private int max_inactive_time;
    DynamicPropertiesManager properties;

    @SneakyThrows
    public ClientMonitor() throws IOException {
        this.properties = new DynamicPropertiesManager("dynamic.properties");
        max_inactive_time = Integer.parseInt(this.properties.getProperty("max_inactive_time"));
    }

    private void checkUsersActivity(){
        timeSeconds = System.currentTimeMillis() / 1000;
        Collection<User> inactiveUsers = userRepository
                .findInactiveUsers(timeSeconds, max_inactive_time);

        //if user is inactive for too long, set him as inactive and set his address and port as server udp socket
        inactiveUsers
                .forEach(user -> {
                    user.setActive(false);
                    user.setIpAddress(properties.getProperty("localAddress"));
                    user.setPort(Integer.parseInt(properties.getProperty("localPort")));
                    userRepository.save(user);
                });
    }

    @Override
    public void run(){
        while(true){
            checkUsersActivity();
            try {
                Thread.sleep(getCheckUsersInactivityInterval());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private long getCheckUsersInactivityInterval(){
        return 1000 * max_inactive_time / 4;
    }

    @PostConstruct
    private void go(){
        this.start();
    }
}
