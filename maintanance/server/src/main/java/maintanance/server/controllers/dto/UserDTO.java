package maintanance.server.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import maintanance.server.model.entities.User;
import org.springframework.web.bind.annotation.GetMapping;

@Setter
@Getter
public class UserDTO {

    private ConnectionType connectionType;
    private Integer Id;
    private String name;
    private String ipAddr;
    private Integer port;
    private String localAddress;
    private Integer localUdpPort;
    private long lastActiveTime;
    private Boolean active;
    private String cryptoPublicKey;

    public UserDTO(User user, ConnectionType connectionType){
        Id = user.getId();
        name = user.getName();
        ipAddr = user.getIpAddress();
        port = user.getPort();
        localAddress = user.getLocalAddress();
        localUdpPort = user.getLocalUdpPort();
        lastActiveTime = user.getLastActiveTime();
        active = user.getActive();
        cryptoPublicKey = user.getCryptoPublicKey();

        this.connectionType = connectionType;
    }
}
