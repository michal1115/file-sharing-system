package maintanance.server.controllers.dto;

public enum ConnectionType {
    PROXY,
    DIRECT,
    HOLE_PUNCHING
}
