package maintanance.server.controllers.dto.providers;

import maintanance.server.controllers.dto.ConnectionType;
import maintanance.server.model.entities.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ConnectionTypeProvider {
    private boolean TEST_PROXY = false;

    public ConnectionType resolveConnectionType(User initiator, User acceptor){
        if (TEST_PROXY){
            return ConnectionType.PROXY;
        }
        if (!acceptor.getActive()){
            return ConnectionType.PROXY;
        }
        if (bothUsersFromSameNetwork(initiator, acceptor) || bothUsersFromLocalNetwork(initiator, acceptor)) {
            return ConnectionType.DIRECT;
        }
        return ConnectionType.PROXY;
    }

    private boolean bothUsersFromSameNetwork(User initiator, User acceptor){
        return initiator.getIpAddress().equals(acceptor.getIpAddress());
    }

    private boolean bothUsersFromLocalNetwork(User initiator, User acceptor){
        return userAvailableLocally(initiator) && userAvailableLocally(acceptor);
    }

    private boolean userAvailableLocally(User user){
        return user.getIpAddress().equals(user.getLocalAddress());
    }
}
