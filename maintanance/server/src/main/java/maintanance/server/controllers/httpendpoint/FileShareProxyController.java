package maintanance.server.controllers.httpendpoint;

import maintanance.server.authentication.JwtTokenUtilService;
import maintanance.server.authentication.UserAuthenticator;
import maintanance.server.controllers.dto.StatusMessage;
import maintanance.server.model.entities.File;
import maintanance.server.model.entities.Message;
import maintanance.server.model.entities.Team;
import maintanance.server.model.entities.User;
import maintanance.server.model.wrappers.RepositoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static maintanance.server.controllers.httpendpoint.SystemStateController.ERROR_REASON_HEADER_KEY;

@RestController
public class FileShareProxyController {
    @Autowired
    private RepositoryWrapper repositoryWrapper;

    @Autowired
    private UserAuthenticator authenticationProvider;

    @Autowired
    private JwtTokenUtilService tokenService;

    @PutMapping("/fileProxy/users/{receiver}/teams/{team}/files/{file}/versions/{sender}")
    public ResponseEntity<String> uploadFileAction(
            @PathVariable String receiver,
            @PathVariable String team,
            @PathVariable String file,
            @PathVariable String sender,
            @RequestBody byte[] fileContent,
            HttpServletRequest request
    ){
        System.out.println("#PutContent");
        System.out.println(new String(fileContent));
        User senderObj = getUserFromAuthorizationHeader(request);
        synchronizeLastActiveTimeForUser(senderObj);
        if (!senderObj.getName().equals(sender)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_NAME_IN_JWT_NOT_MATCH))
                    .build();
        }


        Optional<Team> relatedTeam = repositoryWrapper.getTeamRepository().findByName(team);
        if (!relatedTeam.isPresent() || !relatedTeam.get().getParticipants().contains(senderObj)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.YOU_ARE_NOT_TEAM_MEMBER))
                    .build();
        }

        Optional<File> relatedFile = relatedTeam.get().getSharedFiles().stream()
                .filter(fileObj -> fileObj.getPublicName().equals(file))
                .findFirst();
        if (!relatedFile.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.FILE_IS_NOT_SHARED_IN_TEAM))
                    .build();
        }

        Optional<User> receiverObj = relatedTeam.get().getParticipants().stream()
                .filter(user -> user.getName().equals(receiver))
                .findFirst();
        if (!receiverObj.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.RECEIVER_DOES_NOT_BELONG_TO_TEAM))
                    .build();
        }

        debugMessage(sender, receiver, "#putContent");

        Optional<Message> message = repositoryWrapper.getMessageRepository().findByTeamFileSenderAndReceiver(
                relatedTeam.get(), relatedFile.get(), senderObj, receiverObj.get());
        if (message.isPresent()){
            message.get().setContent(fileContent);
            repositoryWrapper.getMessageRepository().save(message.get());
        } else {
            Message messageToAdd = Message.builder()
                    .sender(senderObj)
                    .receiver(receiverObj.get())
                    .associatedFile(relatedFile.get())
                    .associatedTeam(relatedTeam.get())
                    .content(fileContent)
                    .build();
            repositoryWrapper.getMessageRepository().save(messageToAdd);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/fileProxy/users/{receiver}/teams/{team}/files/{file}/versions/{sender}")
    public ResponseEntity<byte[]> downloadFileAction(
            @PathVariable String receiver,
            @PathVariable String team,
            @PathVariable String file,
            @PathVariable String sender,
            HttpServletRequest request
    ){
        System.out.println("#GetContent");
        User receiverObj = getUserFromAuthorizationHeader(request);
        synchronizeLastActiveTimeForUser(receiverObj);
        if (!receiverObj.getName().equals(receiver)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_NAME_IN_JWT_NOT_MATCH))
                    .build();
        }


        Optional<User> senderObj = repositoryWrapper.getUserRepository().findByName(sender);
        if (!senderObj.isPresent()) {
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_DOES_NOT_EXIST))
                    .build();
        }

        Optional<Team> relatedTeam = repositoryWrapper.getTeamRepository().findByName(team);
        if (!relatedTeam.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                    .build();
        }

        Optional<File> relatedFile = repositoryWrapper.getFileRepository()
                .findByNameAndTeam(file, relatedTeam.get());
        if (!relatedFile.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.FILE_IS_NOT_SHARED_IN_TEAM))
                    .build();
        }
        debugMessage(receiver, sender, "#getContent");

        Optional<Message> message = repositoryWrapper.getMessageRepository().findByTeamFileSenderAndReceiver(
                relatedTeam.get(), relatedFile.get(), senderObj.get(), receiverObj);

        return message
                .map(msg -> {
                    System.out.println(Arrays.toString(msg.getContent()));
                    repositoryWrapper.getMessageRepository().delete(msg);
                    return ResponseEntity.ok(msg.getContent());
                }).orElseGet(() -> ResponseEntity.badRequest()
                        .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.FILE_MESSAGE_DOES_NOT_EXIST))
                        .build());
    }

    @GetMapping("/fileProxy/users/{receiver}")
    public ResponseEntity<List<Message>> getPendingFilesNamesAction(
            @PathVariable String receiver,
            HttpServletRequest request
    ){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeLastActiveTimeForUser(user);
        if (!user.getName().equals(receiver)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_NAME_IN_JWT_NOT_MATCH))
                    .build();
        }

        List<Message> messages = new ArrayList<>(repositoryWrapper.getMessageRepository().findReceivedMessages(user));
        return ResponseEntity.ok(messages);
    }

    private void synchronizeLastActiveTimeForUser(User user){
        user.setLastActiveTime(System.currentTimeMillis() / 1000);
        repositoryWrapper.getUserRepository().save(user);
    }

    private User getUserFromAuthorizationHeader(HttpServletRequest request) throws SystemStateController.UserDoesNotExistException {
        String username = getUsernameFromAuthorizationHeader(request);
        Optional<User> user = repositoryWrapper.getUserRepository().findByName(username);
        if (!user.isPresent()){
            throw new SystemStateController.UserDoesNotExistException();
        }
        return user.get();
    }

    private String getUsernameFromAuthorizationHeader(HttpServletRequest request){
        String jwt = request.getHeader("Authorization").split(" ")[1];
        String username = tokenService.resolveUsernameFromToken(jwt);
        return username;
    }

    private void debugMessage(String sender, String receiver, String method){
        System.out.println("#" + method + " : " + sender + " -> " + receiver);
    }
}
