package maintanance.server.controllers.dto.providers;

import lombok.Getter;
import lombok.Setter;
import maintanance.server.model.entities.Invitation;
import maintanance.server.model.entities.Team;
import maintanance.server.model.entities.User;

import javax.persistence.ManyToOne;

@Getter
@Setter
public class InvitationDTO {
    private Integer id;

    private User sender;

    private User receiver;

    private Team relatedTeam;

    public InvitationDTO(Invitation invitation){
        this.id = invitation.getId();
        this.sender = invitation.getSender();
        this.receiver = invitation.getReceiver();
        this.relatedTeam = createTeamWithJustName(invitation.getRelatedTeam());
    }

    private Team createTeamWithJustName(Team relatedTeam){
        return Team.builder()
                .id(relatedTeam.getId())
                .teamName(relatedTeam.getTeamName())
                .build();
    }
}
