package maintanance.server.controllers.httpendpoint;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import maintanance.server.authentication.*;
import maintanance.server.controllers.dto.StatusMessage;
import maintanance.server.controllers.dto.providers.ConnectionTypeProvider;
import maintanance.server.controllers.dto.UserDTO;
import maintanance.server.controllers.dto.providers.InvitationDTO;
import maintanance.server.model.entities.Team;
import maintanance.server.model.entities.Invitation;
import maintanance.server.model.entities.User;
import maintanance.server.model.entities.File;
import maintanance.server.model.repositories.FileRepository;
import maintanance.server.model.repositories.TeamRepository;
import maintanance.server.model.repositories.InvitationRepository;
import maintanance.server.model.repositories.UserRepository;
import maintanance.server.utils.DynamicPropertiesManager;
import maintanance.server.utils.FileLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class SystemStateController {
    DynamicPropertiesManager properties;
    @SneakyThrows
    public SystemStateController(){
        this.properties = new DynamicPropertiesManager("dynamic.properties");
    }

    private final long peerKeyGenerationInterval = 50000;

    public static String ERROR_REASON_HEADER_KEY = "error-reason";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private InvitationRepository  invitationRepository;

    @Autowired
    private JwtTokenUtilService jwtTokenUtilService;

    @Autowired
    private UserAuthenticator authenticationProvider;

    @Autowired
    private ConnectionTypeProvider connectionTypeProvider;

    @PostMapping("/register")
    public ResponseEntity<AuthResponse> register(
            @RequestBody AuthRequest authRequestDetails,
            HttpServletRequest request
    ){
        String username = authRequestDetails.getUsername();
        Optional<User> userWithProvidedUsername = userRepository.findByName(username);
        if (userWithProvidedUsername.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USERNAME_RESERVED))
                    .build();
        }

        String email = authRequestDetails.getEmail();
        Optional<User> userWithProvidedEmail = userRepository.findByEmail(email);
        if (userWithProvidedEmail.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.EMAIL_RESERVED))
                    .build();
        }

        String password = authRequestDetails.getPassword();
        String encodedPassword = new BCryptPasswordEncoder().encode(password);

        User newUserRecord = User.builder()
                .name(username)
                .password(encodedPassword)
                .email(email)
                .active(true)
                .ipAddress(getAddressFromRequest(request))
                .port(request.getRemotePort())
                .localAddress(authRequestDetails.getLocalAddress())
                .localUdpPort(authRequestDetails.getLocalUdpPort())
                .cryptoPublicKey(authRequestDetails.getCryptoPublicKey())
                .build();
        userRepository.save(newUserRecord);
        return createResponseWithTokenFromUser(newUserRecord);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(
            @RequestBody AuthLoginRequest authLoginRequestDetails,
            HttpServletRequest request
    ){
        String username = authLoginRequestDetails.getUsername();
        String password = authLoginRequestDetails.getPassword();
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            authenticationProvider.authenticate(token);
        } catch (BadCredentialsException e) {
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USERNAME_AND_PASSWORD_INVALID))
                    .build();
        }
        Optional<User> user = userRepository.findByName(username);
        if (user.get().getActive()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USER_IS_ALREADY_LOGGED_IN))
                    .build();
        }

        user.get().setPort(request.getRemotePort());//set new user access port
        user.get().setIpAddress(getAddressFromRequest(request));//set new user access ip addr
        user.get().setLocalAddress(authLoginRequestDetails.getLocalAddress());
        user.get().setLocalUdpPort(authLoginRequestDetails.getLocalUdpPort());
        user.get().setLastActiveTime(System.currentTimeMillis() / 1000);
        user.get().setActive(true);
        user.get().setCryptoPublicKey(authLoginRequestDetails.getCryptoPublicKey());

        userRepository.save(user.get());
        return createResponseWithTokenFromUser(user.get());
    }

    public ResponseEntity<AuthResponse> createResponseWithTokenFromUser(UserDetails details) {
        final String jwt = jwtTokenUtilService.generateToken(details);
        final AuthResponse response = new AuthResponse(jwt);
        return ResponseEntity.ok(response);
    }

    private String getAddressFromRequest(HttpServletRequest request){
        return request.getRemoteAddr().equals("0:0:0:0:0:0:0:1") ? "localhost" : request.getRemoteAddr();
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logoutAction(HttpServletRequest request){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);
        user.setActive(false);
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    //SPRING SECURITY:
    //logout Post cannot be created due to existing one!!!
    //must be called differently, for example logOut
    @PostMapping("/logOut")
    public ResponseEntity<?> ehh(HttpServletRequest request){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);
        user.setActive(false);
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @PutMapping("/teams/{teamName}/incidents")
    public ResponseEntity<?> reportIncident(
            @PathVariable String teamName,
            @RequestBody String incidentDetails,
            HttpServletRequest request
    ){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        Optional<Team> foundTeam = user.getTeams().stream()
                .filter(team -> team.getTeamName().equals(teamName))
                .findFirst();
        if (foundTeam.isPresent()){
            foundTeam.get().getIncidentReports().add(incidentDetails);
            teamRepository.save(foundTeam.get());
            return ResponseEntity.ok(true);
        }
        return ResponseEntity.badRequest()
                .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                .build();
    }

    @SneakyThrows
    @GetMapping("/teams/{teamName}/incidents")
    public ResponseEntity<Set<String>> reportIncident(
            @PathVariable String teamName,
            HttpServletRequest request
    ){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        Optional<Team> foundTeam = user.getTeams().stream()
                .filter(team -> team.getTeamName().equals(teamName))
                .findFirst();

        return foundTeam.map(team -> ResponseEntity.ok(team.getIncidentReports()))
                .orElseGet(() ->  ResponseEntity.badRequest()
                                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                                    .build());
    }

    @SneakyThrows
    @PutMapping("/teams/{teamName}/files/{publicFileName}")
    public ResponseEntity<String> shareFile(
            @PathVariable String teamName,
            @PathVariable String publicFileName,
            HttpServletRequest request
    ){
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        Optional<Team> associatedTeam = teamRepository.findByName(teamName);
        if(!associatedTeam.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                    .build();
        }
        if (!associatedTeam.get().getParticipants().contains(user)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.YOU_ARE_NOT_TEAM_MEMBER))
                    .build();
        }

        Optional<File> alreadyExistingFile = fileRepository.findByNameAndTeam(publicFileName, associatedTeam.get());
        if (alreadyExistingFile.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.PUBLIC_FILE_ALREADY_EXISTS))
                    .build();
        } else {
            File file = new File(publicFileName, associatedTeam.get());
            fileRepository.save(file);
            return ResponseEntity.ok().build();
        }
    }

    @SneakyThrows
    @PutMapping("teams/{teamName}")
    public ResponseEntity<String> createTeam(
            @PathVariable String teamName,
            HttpServletRequest request
    ) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        Optional<Team> alreadyExistingTeam = teamRepository.findByName(teamName);

        if (alreadyExistingTeam.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_ALREADY_EXISTS))
                    .build();
        }

        Team newTeam = Team.builder()
                .teamName(teamName)
                .securityKey(UUID.randomUUID().toString())
                .build();
        newTeam.addParticipant(user);//TODO maybe not needed
        teamRepository.save(newTeam);//TODO maybe not needed
        user.addTeam(newTeam);
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @PutMapping("teams/{teamName}/invitations/{receiverName}")
    public ResponseEntity<String> addUserToTeam(
            @PathVariable String teamName,
            @PathVariable String receiverName,
            HttpServletRequest request
    ) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        if (user.getName().equals(receiverName)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.CANNOT_ADD_YOURSELF_TO_TEAM))
                    .build();
        }

        Optional<User> receiver = userRepository.findByName(receiverName);
        if (!receiver.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USER_DOES_NOT_EXIST))
                    .build();
        }

        Optional<Team> relatedTeam = teamRepository.findByName(teamName);
        if (!relatedTeam.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                    .build();
        }

        if (relatedTeam.get().getParticipants().contains(receiver.get())){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USER_ALREADY_EXISTS_IN_THAT_TEAM))
                    .build();
        }

        Optional<Invitation> alreadyExistingInvitation = invitationRepository
                .findByReceiverAndRelatedTeam(receiver.get(), relatedTeam.get());
        if (alreadyExistingInvitation.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.INVITATION_ALREADY_DONE))
                    .build();
        }

        Invitation newInvitation = Invitation.builder()
                .receiver(receiver.get())
                .sender(user)
                .relatedTeam(relatedTeam.get())
                .build();
        invitationRepository.save(newInvitation);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @GetMapping("users/{username}/invitations")
    public ResponseEntity<Collection<InvitationDTO>> getInvitations(
            @PathVariable String username,
            HttpServletRequest request) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);
        if (!user.getName().equals(username)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_NAME_IN_JWT_NOT_MATCH))
                    .build();
        }
        Collection<InvitationDTO> invitations = invitationRepository.findByReceiver(user).stream()
                .map(InvitationDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(invitations);
    }

    @SneakyThrows
    @DeleteMapping("teams/{teamName}/invitations/{receiverName}")
    public ResponseEntity<String> answerToInvitation(
            @PathVariable String teamName,
            @PathVariable String receiverName,
            @RequestParam(value="decision") boolean decision,
            HttpServletRequest request
    ) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);
        if (!user.getName().equals(receiverName)){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.SENDER_NAME_IN_JWT_NOT_MATCH))
                    .build();
        }

        Optional<Team> relatedTeam = teamRepository.findByName(teamName);
        if (!relatedTeam.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.TEAM_DOES_NOT_EXIST))
                    .build();
        }

        Optional<Invitation> invitation = invitationRepository.findByReceiverAndRelatedTeam(user, relatedTeam.get());
        if(!invitation.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USER_WAS_NOT_INVITED))
                    .build();
        }

        invitationRepository.delete(invitation.get());
        if (!decision){
            return ResponseEntity.ok().build();
        }

        relatedTeam.get().addParticipant(user);
        teamRepository.save(relatedTeam.get());
        user.addTeam(relatedTeam.get());
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    //overriden by spring security
    //DO NOT REFACTOR!
    @SneakyThrows
    @GetMapping("usersList")
    public ResponseEntity<Collection<UserDTO>> getUsersList(
            HttpServletRequest request
    ) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        List<UserDTO> users = new LinkedList<>();
        userRepository.findAll().forEach(otherUser ->
                users.add(new UserDTO(otherUser, connectionTypeProvider.resolveConnectionType(user, otherUser))));
        return ResponseEntity.ok(users);
    }

    @SneakyThrows
    @GetMapping("teamsNames")
    public ResponseEntity<List<String>> getTeamsList(
            HttpServletRequest request
    ) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        return ResponseEntity.ok(StreamSupport.stream(
                teamRepository.findAll().spliterator(), false)
                    .map(Team::getTeamName)
                    .collect(Collectors.toList()));
    }

    @SneakyThrows
    @GetMapping("teams")
    public Set<Team> getMyTeamsDetails(HttpServletRequest request) {
        User user = getUserFromAuthorizationHeader(request);
        synchronizeUser(user, request);

        Set<Team> teamsToReturn;
        synchronized (teamRepository){
            teamsToReturn = user.getTeams();
            generateKeys();
        }
        return teamsToReturn;
    }

    private void generateKeys(){
        teamRepository.findAll().forEach(team -> {
            if (System.currentTimeMillis() - team.getLastSecurityKeyGenerationTime() > peerKeyGenerationInterval){
                team.setSecurityKey(UUID.randomUUID().toString());
                team.setLastSecurityKeyGenerationTime(System.currentTimeMillis());
                teamRepository.save(team);
            }
        });
    }

    @GetMapping(value="keyExchange")
    public Map<String, Integer> keyExchangeAction(){
        Map<String, Integer> result = new HashMap<>();
        result.put("g", Integer.parseInt(this.properties.getProperty("g_key_exchange_val")));
        result.put("p", Integer.parseInt(this.properties.getProperty("p_key_exchange_val")));
        return result;
    }

    @GetMapping(value="test")
    public ResponseEntity<String> testEndpoint(
            @RequestHeader String h,
            @RequestParam String code,
            HttpServletRequest request
    ){
        System.out.println(request.getRemotePort());
        if (code.equals("1")){
            return ResponseEntity.badRequest().body("err");
        }
        return ResponseEntity.ok("success" + h);
    }

    @PostMapping(value="test")
    public ResponseEntity<String> testEndpointa(
            @RequestHeader String h,
            @RequestParam String code,
            HttpServletRequest request
    ){
        System.out.println(request.getRemotePort());
        if (code.equals("1")){
            return ResponseEntity.badRequest().body("err");
        }
        return ResponseEntity.ok("success" + h);
    }

    private void synchronizeUser(User user, HttpServletRequest request){
        user.setLastActiveTime(System.currentTimeMillis() / 1000);
        user.setActive(true);
        user.setIpAddress(getAddressFromRequest(request));
        user.setPort(request.getRemotePort());
        userRepository.save(user);
    }

    private User getUserFromAuthorizationHeader(HttpServletRequest request) throws UserDoesNotExistException{
        String username = getUsernameFromAuthorizationHeader(request);
        Optional<User> user = userRepository.findByName(username);
        if (!user.isPresent()){
            throw new UserDoesNotExistException();
        }
        return user.get();
    }

    private String getUsernameFromAuthorizationHeader(HttpServletRequest request){
        String jwt = request.getHeader("Authorization").split(" ")[1];
        return jwtTokenUtilService.resolveUsernameFromToken(jwt);
    }

    static class UserDoesNotExistException extends RuntimeException{}
}

