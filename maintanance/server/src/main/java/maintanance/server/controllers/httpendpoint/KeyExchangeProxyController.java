package maintanance.server.controllers.httpendpoint;

import maintanance.server.authentication.JwtTokenUtilService;
import maintanance.server.authentication.UserAuthenticator;
import maintanance.server.controllers.dto.PendingKeyRecord;
import maintanance.server.controllers.dto.StatusMessage;
import maintanance.server.model.entities.User;
import maintanance.server.model.wrappers.RepositoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static maintanance.server.controllers.httpendpoint.SystemStateController.ERROR_REASON_HEADER_KEY;

@RestController
public class KeyExchangeProxyController {

    private Map<String, List<PendingKeyRecord>> pendingKeys = new HashMap<>();

    @Autowired
    private RepositoryWrapper repositoryWrapper;

    @Autowired
    private UserAuthenticator authenticationProvider;

    @Autowired
    private JwtTokenUtilService tokenService;

    @PutMapping("/proxyKeys/{receiverName}")
    public ResponseEntity<String> putKey(
            @PathVariable String receiverName,
            @RequestBody String key,
            HttpServletRequest request
    ) {
        User sender = getUserFromAuthorizationHeader(request);
        synchronizeLastActiveTimeForUser(sender);

        Optional<User> receiver = repositoryWrapper.getUserRepository().findByName(receiverName);
        if (!receiver.isPresent()){
            return ResponseEntity.badRequest()
                    .header(ERROR_REASON_HEADER_KEY, String.valueOf(StatusMessage.USER_DOES_NOT_EXIST))
                    .build();
        }

        if (!pendingKeys.containsKey(receiver.get())){
            pendingKeys.put(receiver.get().getName(), new LinkedList<>());
        }

        pendingKeys.get(receiver.get().getName()).add(
                PendingKeyRecord.builder()
                    .sender(sender)
                    .receiver(receiver.get())
                    .key(key)
                    .build()
        );
        return ResponseEntity.ok().build();
    }

    @GetMapping("/proxyKeys")
    public ResponseEntity<List<PendingKeyRecord>> putKey(
            HttpServletRequest request
    ) {
        User sender = getUserFromAuthorizationHeader(request);
        synchronizeLastActiveTimeForUser(sender);

        ResponseEntity<List<PendingKeyRecord>> result =  ResponseEntity.ok(pendingKeys.get(sender.getName()));
        pendingKeys.remove(sender.getName());
        return result;
    }

    private void synchronizeLastActiveTimeForUser(User user){
        user.setLastActiveTime(System.currentTimeMillis() / 1000);
        repositoryWrapper.getUserRepository().save(user);
    }

    private User getUserFromAuthorizationHeader(HttpServletRequest request) throws SystemStateController.UserDoesNotExistException {
        String username = getUsernameFromAuthorizationHeader(request);
        Optional<User> user = repositoryWrapper.getUserRepository().findByName(username);
        if (!user.isPresent()){
            throw new SystemStateController.UserDoesNotExistException();
        }
        return user.get();
    }

    private String getUsernameFromAuthorizationHeader(HttpServletRequest request){
        String jwt = request.getHeader("Authorization").split(" ")[1];
        String username = tokenService.resolveUsernameFromToken(jwt);
        return username;
    }
}
