package maintanance.server.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import maintanance.server.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PendingKeyRecord {
    private String key;
    private User sender;
    private User receiver;
}
