package maintanance.server.controllers.httpendpoint;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import maintanance.server.utils.FileLoader;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
public class SteganoResourceController {
    @SneakyThrows
    @GetMapping(value = "randomPicture/{theme}/{size}", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] randomResourceAction(
            @PathVariable String theme,
            @PathVariable String size
    ){
        String separator = java.io.File.separator;
        java.io.File pictureDir =
                new java.io.File("." + separator + "steganography_files" + separator + theme + separator + size);
        if (!pictureDir.isDirectory()){
            return null;
        }
        java.io.File[] files = pictureDir.listFiles();

        Random random = new Random();
        int decision = random.nextInt(files.length);

        Path path = files[decision].toPath();
        return Files.readAllBytes(path);
    }

    @SneakyThrows
    @GetMapping(value = "randomTeam/{theme}")
    public String randomResourceTeamNameAction(
            @PathVariable String theme
    ){
        String separator = java.io.File.separator;
        String fileContent =
                FileLoader.getFileContent("." + separator + "steganography_files" + separator + theme + separator + "teams.json");
        Gson gson = new Gson();

        List<String> fakeTeamNames = Arrays.asList(gson.fromJson(fileContent, String[].class));
        Random random = new Random();

        int choice = random.nextInt(fakeTeamNames.size());
        return fakeTeamNames.get(choice);
    }

    @SneakyThrows
    @GetMapping(value = "randomFileName/{theme}")
    public String randomResourceNameAction(
            @PathVariable String theme
    ){
        String separator = java.io.File.separator;
        String fileContent =
                FileLoader.getFileContent("." + separator + "steganography_files" + separator + theme + separator + "files.json");
        List<String> fakeFileNames = Arrays.asList(new Gson().fromJson(fileContent, String[].class));
        Random random = new Random();

        int choice = random.nextInt(fakeFileNames.size());
        return fakeFileNames.get(choice);
    }

    @GetMapping(value="themes")
    public List<String> getThemesAction(){
        java.io.File steganographyResourceDir = new java.io.File("steganography_files");
        return Arrays.stream(Objects.requireNonNull(steganographyResourceDir.listFiles()))
                .map(java.io.File::getName)
                .collect(Collectors.toList());
    }
}
