package maintanance.server.model.repositories;

import maintanance.server.model.entities.File;
import maintanance.server.model.entities.Message;
import maintanance.server.model.entities.Team;
import maintanance.server.model.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {

    @Query(value =  "SELECT m as message "      +
                    "FROM Message m "           +
                    "WHERE m.receiver = :receiver")
    Collection<Message> findReceivedMessages(
            @Param("receiver") User receiver
    );


    @Query(value =  "SELECT m as message "      +
            "FROM Message m "           +
            "WHERE m.sender = :sender " +
            "and m.receiver = :receiver " +
            "and m.associatedTeam = :team " +
            "and m.associatedFile = :file")
    Optional<Message> findByTeamFileSenderAndReceiver(
            @Param("team") Team team,
            @Param("file") File file,
            @Param("sender") User receiver,
            @Param("receiver") User sender
    );

}
