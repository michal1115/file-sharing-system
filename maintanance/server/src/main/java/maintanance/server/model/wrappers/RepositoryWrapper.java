package maintanance.server.model.wrappers;

import lombok.Getter;
import lombok.Setter;
import maintanance.server.model.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class RepositoryWrapper {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InvitationRepository invitationRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;
}
