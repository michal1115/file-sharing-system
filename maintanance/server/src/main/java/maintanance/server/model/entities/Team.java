package maintanance.server.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Builder
@AllArgsConstructor
@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String teamName;

    private String securityKey;

    private long lastSecurityKeyGenerationTime;

    @OneToMany(mappedBy = "connectedTeam")
    @Builder.Default
    private Set<File> sharedFiles = new HashSet<>();

    @ManyToMany(mappedBy = "teams")
    @Builder.Default
    private Set<User> participants = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "relatedTeam")
    @Builder.Default
    private Set<Invitation> invitations = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "associatedTeam")
    @Builder.Default
    private Set<Message> associatedMessages = new HashSet<>();

    @ElementCollection
    @Builder.Default
    private Set<String> incidentReports = new HashSet<>();

    public Team(String teamName){
        this.teamName = teamName;
    }

    public Team(){
    }

    public void addSharedFile(File file){
        sharedFiles.add(file);
    }

    public void addParticipant(User user){
        System.out.println(participants);
        participants.add(user);
    }

    public void addInvitation(Invitation invitation){
        this.invitations.add(invitation);
    }
}
