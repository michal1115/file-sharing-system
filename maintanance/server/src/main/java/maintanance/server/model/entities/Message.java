package maintanance.server.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.security.GeneralSecurityException;
import java.sql.Blob;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private User sender;

    @ManyToOne
    private User receiver;

    @ManyToOne
    private Team associatedTeam;

    @ManyToOne
    private File associatedFile;

    @Lob
    @JsonIgnore
    private byte[] content;
}
