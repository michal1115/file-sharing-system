package maintanance.server.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String publicName;

    @JsonIgnore
    @ManyToOne
    private Team connectedTeam;

    @JsonIgnore
    @OneToMany(mappedBy = "associatedFile")
    private Set<Message> associatedMessages = new HashSet<>();

    public File(String publicName, Team connectedTeam){
        this.publicName = publicName;
        this.connectedTeam = connectedTeam;
    }

    public File(){}
}
