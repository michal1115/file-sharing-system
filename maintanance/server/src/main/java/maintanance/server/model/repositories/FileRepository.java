package maintanance.server.model.repositories;

import maintanance.server.model.entities.File;
import maintanance.server.model.entities.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepository extends CrudRepository<File, Integer> {

    @Query(value = "SELECT f " +
            "FROM File f " +
            "WHERE f.publicName = :name " +
            "AND f.connectedTeam = :team")
    Optional<File> findByNameAndTeam(
            @Param("name") String name,
            @Param("team") Team team
    );
}
