package maintanance.server.model.repositories;

import maintanance.server.model.entities.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer> {
    @Query(value = "SELECT t as team " +
            "FROM Team t " +
            "WHERE t.teamName = :name")
    Optional<Team> findByName(
            @Param("name") String name
    );
}
