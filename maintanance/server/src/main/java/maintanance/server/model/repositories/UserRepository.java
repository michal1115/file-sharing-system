package maintanance.server.model.repositories;

import maintanance.server.model.entities.User;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Query(value =  "SELECT u as user "     +
                    "FROM User as u "       +
                    "WHERE u.name = :name")
    Optional<User> findByName(
            @Param("name") String name
    );

    @Query(value =  "SELECT u as user "     +
            "FROM User as u "       +
            "WHERE u.email = :email")
    Optional<User> findByEmail(
            @Param("email") String email
    );

    @Query(value =  "SELECT u as user "     +
                    "FROM User as u "       +
                    "WHERE u.active = true ")
    Collection<User> findActiveUsers();

    @Query(value =  "SELECT u as user "     +
                    "FROM User as u "       +
                    "WHERE :time - u.lastActiveTime > :maxInactiveTime and u.active = true")
    Collection<User> findInactiveUsers(
            @Param("time") long time,
            @Param("maxInactiveTime") long maxInactiveTime
    );
}
