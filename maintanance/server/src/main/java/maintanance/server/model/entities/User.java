package maintanance.server.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.OptimisticLock;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@ToString(exclude = {"email", "password", "active", "messages"})
@JsonIgnoreProperties(value = { "enabled", "authorities", "username", "accountNonExpired", "credentialsNonExpired", "accountNonLocked" })
@Entity
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String name;

    @NotNull
    @JsonIgnore
    private String email;

    @NotNull
    @JsonIgnore
    private String password;

    @NotNull
    private String ipAddress;

    @NotNull
    private Integer port;

    @NotNull
    private String localAddress;

    @NotNull
    private Integer localUdpPort;

    @NotNull
    private Boolean active = false;

    private long lastActiveTime = System.currentTimeMillis() / 1000;

    @Lob
    private String cryptoPublicKey;

    @JsonIgnore
    @ManyToMany
    private Set<Team> teams = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "sender")
    private Set<Invitation> issuedInvitations = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "receiver")
    private Set<Invitation> receivedInvitations = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "sender")
    private Set<Message> messagesSentByUser = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "receiver")
    private Set<Message> messagesSentToUser = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy="receiver", fetch = FetchType.EAGER)
    private Set<Message> messages = new HashSet<>();//message buffers for inactive users


    public User(String name,  String password, String email, String ipAddr, int port) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.ipAddress = ipAddr;
        this.port = port;
        this.active = false;
    }

    public User(){}

    public void addIssuedInvitation(Invitation invitation){
        issuedInvitations.add(invitation);
    }

    public void addReceivedInvitation(Invitation invitation){
        receivedInvitations.add(invitation);
    }

    public void addTeam(Team team){
        teams.add(team);
    }

    //Spring security:
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(() -> "server-pool-auth");
        return grantedAuthorities;
    }

    @Override
    public String getPassword(){
        return password;
    }

    @Override
    public String getUsername(){
        return name;
    }

    @Override
    public boolean isAccountNonExpired(){
        return true;
    }

    @Override
    public boolean isAccountNonLocked(){
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired(){
        return true;
    }

    @Override
    public boolean isEnabled(){
        return true;
    }
}
