package maintanance.server.model.repositories;

import maintanance.server.model.entities.Invitation;
import maintanance.server.model.entities.Team;
import maintanance.server.model.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface InvitationRepository extends CrudRepository<Invitation, Integer> {

    @Query(value="SELECT i FROM Invitation i " +
            "WHERE i.receiver = :receiver " +
            "AND i.relatedTeam = :relatedTeam")
    Optional<Invitation> findByReceiverAndRelatedTeam(
            @Param(value="receiver") User receiver,
            @Param(value="relatedTeam") Team relatedTeam);

    @Query(value = "SELECT i FROM Invitation i " +
            "WHERE i.receiver = :receiver")
    Collection<Invitation> findByReceiver(
            @Param(value="receiver") User receiver);
}
