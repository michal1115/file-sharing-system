package maintanance.server.authentication;

import maintanance.server.model.entities.User;
import maintanance.server.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAuthenticator implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken)authentication;

        String username = (String)token.getPrincipal();
        Optional<User> user = userRepository.findByName(username);
        if (!user.isPresent()){
            throw new BadCredentialsException("user does not exist");
        }
        UserDetails userDetails = user.get();

        String password = (String)token.getCredentials();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        if(!encoder.matches(password, userDetails.getPassword())){
            throw new BadCredentialsException("Bad credentials for user " + username);
        }
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication){
        return false;
    }
}
