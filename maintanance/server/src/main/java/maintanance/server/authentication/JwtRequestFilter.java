package maintanance.server.authentication;

import maintanance.server.model.entities.User;
import maintanance.server.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOError;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenUtilService jwtTokenUtilService;

    private static final String AUTH_HEADER_FIELD_NAME = "Authorization";
    private final String SPECIAL_HEADER_PREFIX = "Bearer ";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException
    {
        final String header = request.getHeader(AUTH_HEADER_FIELD_NAME);

        String jwt = null;
        String username = null;

        if (isValidHeader(header)){
            jwt = extractToken(header);
            username = jwtTokenUtilService.resolveUsernameFromToken(jwt);
        }

        if (isUsernameSpecified(username)){
            authenticate(request, jwt, username);
        }
        filterChain.doFilter(request, response);
    }

    private boolean isValidHeader(String header){
        return (!Objects.isNull(header)) && (header.startsWith(SPECIAL_HEADER_PREFIX));
    }

    private String extractToken(String header){
        return header.substring(SPECIAL_HEADER_PREFIX.length());
    }

    private boolean isUsernameSpecified(String username){
        return username != null;
    }

    private void authenticate(HttpServletRequest request, String jwt, String username){
        Optional<User> user = userRepository.findByName(username);
        if (!user.isPresent()){
            throw new BadCredentialsException("given user does not exist");
        }
        UserDetails userDetails = user.get();

        if (notAlreadyAuthenticated() && jwtTokenUtilService.validateToken(jwt, userDetails)){
            createTokenAndSetInSecurityContext(request, userDetails);
        }

    }

    private void createTokenAndSetInSecurityContext(HttpServletRequest request, UserDetails userDetails){
        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        WebAuthenticationDetails details = new WebAuthenticationDetailsSource().buildDetails(request);
        token.setDetails(details);
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    private boolean notAlreadyAuthenticated(){
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }



}
