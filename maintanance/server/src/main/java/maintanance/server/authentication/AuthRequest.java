package maintanance.server.authentication;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class AuthRequest {
    private String username;
    private String password;
    private String email;
    private String localAddress;
    private int localUdpPort;
    private String cryptoPublicKey;
}
