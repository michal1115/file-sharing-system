package maintanance.server.authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtTokenUtilService {

    final private static int SECOND_IN_MILLIS = 1000;
    final private static int MINUTE_IN_MILLIS = 60 * SECOND_IN_MILLIS;
    final private static int HOUR_IN_MILLIS = 60 * MINUTE_IN_MILLIS;
    final private static long DAY_IN_MILLIS = 24 * HOUR_IN_MILLIS;
    final private static long MONTH_IN_MILLIS = 30 * DAY_IN_MILLIS;

    //final private static int JWT_VALID_TIME = 1 * HOUR_IN_MILLIS + 30 * MINUTE_IN_MILLIS;
    final private static long JWT_VALID_TIME = 5 * MONTH_IN_MILLIS;

    @Value("${jwt.secret}")
    private String SECRET;

    public String resolveUsernameFromToken(String token){
        return resoleClaimFromToken(token, Claims::getSubject);
    }

    public Date resolveExpirationDateFromToken(String token){
        return resoleClaimFromToken(token, Claims::getExpiration);
    }

    public boolean isTokenExpired(String token){
        final Date expiration = resolveExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public <T> T resoleClaimFromToken(String token, Function<Claims, T> claimsResolver){
        return claimsResolver.apply(resolveAllClaimsFromToken(token));
    }

    private Claims resolveAllClaimsFromToken(String token){
        return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
    }

    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims = new HashMap<String, Object>();
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_VALID_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public boolean validateToken(String token, UserDetails userDetails){
        String userName = resolveUsernameFromToken(token);
        return userName.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }
}
