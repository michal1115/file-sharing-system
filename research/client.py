import socket
from scapy.all import *

ip_addr = "185.243.55.183"
ip_addr_local = "192.168.1.14"
port = 10000

p1 = IP(dst=ip_addr, src="192.168.1.14") / UDP(dport=port, sport=10000) / Raw("abc")
p2 = IP(dst=ip_addr, src="192.168.1.14") / UDP(dport=port + 1, sport=10000) / Raw("abc")
send(p1)
send(p2)
#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#sock.sendto(b'HELLO', (ip_addr, port))

#while True:
#    data, addr = sock.recvfrom(1024)
#    print(data.decode('utf-8'))