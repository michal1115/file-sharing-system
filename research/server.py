import socket
import threading
import sys

ip_addr = "192.168.1.14"
port = 10000

if len(sys.argv) > 1:
    port = int(sys.argv[1])

FINISH = False
def end_prog():
    global FINISH
    FINISH = True

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((ip_addr, port))

senders = []

def print_addr_tab():
    print(3 * (25*'-' + '\n'))
    global addr_tab
    for idx, sender in enumerate(senders):
        print(str(idx) + ". " + str(sender))

def send_to_client():
    print_addr_tab()
    sender_id = int(input("Provide index:"))
    sender = senders[sender_id]

    message = bytearray(input("Message:"), "utf8")

    sock.sendto(message, (sender[0], sender[1]))

def send_to_client_direct():
    print_addr_tab()

    sender = (input("Address:"), int(input("Port:")))
    print(sender)
    message = bytearray(input("Message:"), "utf8")

    sock.sendto(message, sender)


def listen_on_socket():
    global senders
    while not FINISH:
        data, addr = sock.recvfrom(1024)
        print("Received " + data.decode('utf8') + str(addr))
        if addr not in senders:
            senders.append(addr)


def listen_for_user():
    actions = {
        "q": end_prog,
        "p": print_addr_tab,
        "s": send_to_client,
        "sd": send_to_client_direct

    }

    while not FINISH:
        user_input = input()
        if user_input in actions.keys():
            actions[user_input]()
        else:
            print("Invalid command")

if __name__ == "__main__":
    thread_sock = threading.Thread(target=listen_on_socket)
    thread_user_input = threading.Thread(target=listen_for_user)
    thread_sock.start()
    thread_user_input.start()