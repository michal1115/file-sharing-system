import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;
import java.util.function.Consumer;

public class Main {

    private static String FINISH_MESSAGE = "FIN";
    private static int accessPort = 60000;

    private static Map<String, String> keys = new HashMap<>();
    private static Map<String, SocketAddress> clientAccessAddresses = new HashMap<String, SocketAddress>();
    private static Map<String, SockOperating> connections = new HashMap<>();


    private enum Management{
        REGISTER("REGISTER", Main::manageRegister, (cli) -> System.out.println("REGISTER: " + cli)),
        READY("READY", Main::manageReady, (cli) -> System.out.println("READY: " + cli));

        private String messageType;
        private Consumer<SockOperating> action;
        private Consumer<String> log;

        Management(String messageType, Consumer<SockOperating> action, Consumer<String> log){
            this.messageType = messageType;
            this.action = action;
            this.log = log;
        }

        public String getMessageType() {
            return messageType;
        }

        public Consumer<SockOperating> getAction() {
            return action;
        }

        public Consumer<String> getLog() {
            return log;
        }
    }

    private static boolean auth(String username, String key){
        return key.equals(keys.get(username));
    }

    private static void manageConnection(SockOperating connection) {
        try {
            Management management = Management.valueOf(connection.readLine());
            management.log.accept(connection.socket.getRemoteSocketAddress().toString());
            management.action.accept(connection);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void manageRegister(SockOperating connection){
        try {
            String username = connection.readLine();
            clientAccessAddresses.put(username, connection.socket.getRemoteSocketAddress());

            String generatedKey = UUID.randomUUID().toString();
            keys.put(username, generatedKey);

            connection.writeLine(generatedKey);
            connection.writeLine(String.valueOf(accessPort));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void manageReady(SockOperating connection){
        try {
            String username = connection.readLine();
            String key = connection.readLine();
            if (!auth(username, key)){
                connection.writeLine("false");
                return;
            }

            Socket accessSocket = new Socket();
            accessSocket.connect(clientAccessAddresses.get(username));
            connection.writeLine("true");

            System.out.println("CONNECTED to " + accessSocket.getRemoteSocketAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void runServer(String address, int port){
        try {
            ServerSocket s = new ServerSocket();
            s.setReuseAddress(true);
            s.bind(new InetSocketAddress(address, port));
            while (true) {
                Socket sock = s.accept();
                sock.setReuseAddress(true);
                SockOperating connection = new SockOperating(sock);
                /*System.out.println("------------------------------\n"
                        + sock.getLocalSocketAddress() + "\n"
                        + sock.getRemoteSocketAddress() + "\n"
                        + "-----------------------------" + "\n");*/
                new Thread(() -> manageConnection(connection)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    //CLIENT:

    private static String getUsername(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("USERNAME: ");
        return scanner.next();
    }

    public static void runClient(String sourceAddr, int sourcePort, String addr, int port) throws IOException {
        String username = getUsername();

        Socket shareSock = new Socket();
        shareSock.setReuseAddress(true);
        shareSock.bind(new InetSocketAddress(sourceAddr, sourcePort));
        shareSock.connect(new InetSocketAddress(addr, port));
        SockOperating sockOperating = new SockOperating(shareSock);

        sockOperating.writeLine("REGISTER");
        sockOperating.writeLine(username);
        String key = sockOperating.readLine();

        int portToSend = Integer.parseInt(sockOperating.readLine());
        openHole(sourceAddr, sourcePort, addr, portToSend);

        sockOperating.getSocket().close();

        ServerSocket serverShareSocket = new ServerSocket();
        serverShareSocket.setReuseAddress(true);
        serverShareSocket.bind(new InetSocketAddress(sourceAddr, sourcePort));

        new Thread(() -> {
            try {
                while (true) {
                    serverShareSocket.accept();
                    System.out.println("from cli accepted");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        Socket readySocket = new Socket();
        readySocket.connect(new InetSocketAddress(addr, port));
        SockOperating connection = new SockOperating(readySocket);

        connection.writeLine("READY");
        connection.writeLine(username);
        connection.writeLine(key);
        System.out.println(connection.readLine());
        connection.getSocket().close();
    }

    public static void openHole(String sourceAddress, int sourcePort, String address, int port) throws IOException {
        DatagramSocket udpSocket = new DatagramSocket(sourcePort, InetAddress.getByName(sourceAddress));
        byte[] data = "test".getBytes();
        DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(address), port);
        udpSocket.send(packet);
    }

    //OTHERS:
    public static void runServerThread(String address, int port){
        new Thread(() -> runServer(address, port)).start();
    }

    public static void runClientThread(String sourceAddr, int sourcePort, String address, int port){
        new Thread(() -> {
            try {
                Socket shareSock = new Socket();
                shareSock.setReuseAddress(true);
                shareSock.bind(new InetSocketAddress(sourceAddr, sourcePort));
                shareSock.connect(new InetSocketAddress(address, port));
                SockOperating sockOperating = new SockOperating(shareSock);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        String sourceAddr = ConfigProvider.getAddress("client");
        int sourcePort = ConfigProvider.getPort("client");
        String addr = ConfigProvider.getAddress("server");
        int port = ConfigProvider.getPort("server");

        Runnable server = () -> runServer(addr, port);
        Runnable client = () -> {
            try {
                runClient(sourceAddr, sourcePort, addr, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        if(args.length > 0){
            String op = args[0];
            if (op.toLowerCase().equals("server")){
                server.run();
            }
            else if (op.toLowerCase().equals("client")){
                client.run();
            }
        } else {
            new Thread(server).start();
            client.run();
        }
        /*runServerThread("localhost", 10000);
        runServerThread("localhost", 10001);
        runServerThread("localhost", 10002);
        runClientThread("localhost", 9000, "localhost", 10000);
        runClientThread("localhost", 10000, "localhost", 10001);*/
    }

    static class SockOperating{
        private Socket socket;
        private PrintWriter writer;
        private BufferedReader reader;

        public SockOperating(Socket socket, PrintWriter writer, BufferedReader reader) {
            this.socket = socket;
            this.writer = writer;
            this.reader = reader;
        }

        public SockOperating(Socket socket) throws IOException {
            this.socket = socket;
            this.writer = new PrintWriter(socket.getOutputStream(), true);
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        public Socket getSocket() {
            return socket;
        }

        public void setSocket(Socket socket) {
            this.socket = socket;
        }

        public PrintWriter getWriter() {
            return writer;
        }

        public void setWriter(PrintWriter writer) {
            this.writer = writer;
        }

        public BufferedReader getReader() {
            return reader;
        }

        public void setReader(BufferedReader reader) {
            this.reader = reader;
        }

        public String readLine() throws IOException {
            String input = reader.readLine();
            while (input == null){
                input = reader.readLine();
            }
            return input;
        }

        public void writeLine(String line){
            writer.write(line + System.lineSeparator());
            writer.flush();
        }
    }
}
