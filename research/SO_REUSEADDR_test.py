import socket
import threading
import time

io_lock = threading.Semaphore(1)

def print_async(message):
    io_lock.acquire()
    print(message)
    io_lock.release()

def serv(address, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((address, port))
    s.listen()
    while True:
        connection, client_address = s.accept()
        message = (25 * '-') + '\n' + str(connection.getsockname()) + '\n' + str(connection.getpeername()) + '\n' + (25 * '-') + '\n'
        threading.Thread(target=print_async, args=(message,)).start()
        print(connection.recv(20))

def cli(my_address, my_port, other_address, other_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((my_address, my_port))
    s.connect((other_address, other_port))
    s.send(b"hello")

def run_serv(address, port):
    thread = threading.Thread(target=serv, args=(address, port,))
    thread.start()

def run_cli(my_address, my_port, other_address, other_port):
    thread = threading.Thread(target=cli, args=(my_address, my_port, other_address, other_port))
    thread.start()

def main():
    run_serv("localhost", 10000)
    run_serv("localhost", 10001)
    run_serv("localhost", 10002)

    time.sleep(1)

    run_cli("localhost", 9000, "localhost", 10000)
    run_cli("localhost", 10000, "localhost", 10001)
    run_cli("localhost", 10001, "localhost", 10002)
    run_cli("localhost", 10002, "localhost", 10000)

if __name__ == "__main__":
    main()