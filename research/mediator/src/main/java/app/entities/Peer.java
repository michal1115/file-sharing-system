package app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Peer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String key;

    @Column(length=10000)
    private String sdp;

    public Peer(String key, String sdp) {
        this.key = key;
        this.sdp = sdp;
    }

    public Peer(){

    }
}
