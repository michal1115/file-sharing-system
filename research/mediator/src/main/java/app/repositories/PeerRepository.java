package app.repositories;

import app.entities.Peer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PeerRepository extends CrudRepository<Peer, Integer> {
    @Query(value =  "SELECT p "
            + "FROM Peer p "
            + "WHERE p.key = :key")
    Optional<Peer> findByKey(
            @Param("key") String key
    );
}
