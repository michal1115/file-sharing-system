package app.controllers;

import app.entities.Peer;
import app.repositories.PeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class Controller {

    @Autowired
    private PeerRepository peerRepository;

    @PostMapping("/peer")
    public boolean registerPeerAction(
            @RequestParam(value="key") String key,
            @RequestBody String sdp
    ){
        System.out.println("SDP:");
        System.out.println(sdp);
        Peer newPeer = peerRepository.findByKey(key)
                .map(peer -> {
                    peer.setSdp(sdp);
                    return peer;
                })
                .orElse(new Peer(key, sdp));

        peerRepository.save(newPeer);
        return true;
    }

    @GetMapping("/peer")
    public ResponseEntity<String> getPeerSdbAction(
            @RequestParam(value="key") String key
    ){
        Optional<Peer> peer = peerRepository.findByKey(key);
        return peer
                .map(value -> ResponseEntity.ok(value.getSdp()))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = "test", produces = "application/json")
    public String getPeerSdbAction(){
        System.out.println("ok");
        return "ok";
    }
}
