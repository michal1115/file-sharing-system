import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ConfigProvider {
    private static String PATH = "config.txt";

    public static Map<String, String> addresses = new HashMap<>();
    public static Map<String, Integer> ports = new HashMap<>();

    static {
        try {
            load();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void load() throws FileNotFoundException {
        File file = new File(PATH);
        Scanner scanner = new Scanner(file);

        while(scanner.hasNext()){
            String line = scanner.nextLine();

            String user = line.split(" ")[0];
            String address = line.split(" ")[1].split(":")[0];
            int port = Integer.parseInt(line.split(" ")[1].split(":")[1]);

            addresses.put(user, address);
            ports.put(user, port);
        }
    }

    public static String getAddress(String user){
        return addresses.get(user);
    }

    public static int getPort(String user){
        return ports.get(user);
    }
}
