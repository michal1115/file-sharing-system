package server.connection.messages.handlers;

import client.connection.manager.ClientConnectionManager;
import client.connection.server.state.ConnectionState;
import exceptions.P2PNotConnectedException;
import proto.messages.data.HelloMessage;
import proto.messages.data.HelloResponseMessage;
import server.connection.manager.ConnectionManager;
import server.connection.peer.state.PeerConnectionState;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

//special handler that requires also sender address, because user is not saved at this time
public class HelloHandlerServer extends ServerMessageHandler<HelloMessage> {

    @Override
    public void processMessage(InetSocketAddress senderAddress, ConnectionManager connectionManager, HelloMessage message) throws IOException, P2PNotConnectedException {
        boolean isUsernameAlreadyTaken = connectionManager.getUsersWithConnectionDetails()
                .containsKey(message.getSenderUsername());
        HelloResponseMessage helloResponse = HelloResponseMessage.builder()
                .isUsernamePermitted(isUsernameAlreadyTaken)
                .receiverUsername(message.getSenderUsername())
                .senderUsername(message.getReceiverUsername())
                .serverConnectionKey(UUID.randomUUID().toString())
                .build();

        connectionManager.sendDirect(senderAddress, helloResponse);
    }

    @Override
    public Set<PeerConnectionState> permittedConnectionStates() {
        return new HashSet<>(Arrays.asList(ConnectionState.DISCONNECTED));
    }
}
