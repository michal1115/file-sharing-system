package server.connection.messages.handlers;

import client.connection.server.state.ConnectionState;
import exceptions.P2PNotConnectedException;
import proto.messages.data.Message;
import server.connection.manager.ConnectionManager;
import server.connection.peer.state.PeerConnectionState;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Set;

/**
 * Transformations of connection state. Every message received triggers call of suitable ConnectionStateFunction
 */
public abstract class ServerMessageHandler<MsgType extends Message> {

    public void handleMessage(InetSocketAddress senderAddress, ConnectionManager connectionManager, MsgType message) throws IOException, P2PNotConnectedException {
        if (!permittedConnectionStates().contains(connectionManager.getUsersWithConnectionDetails().get(message.))){
            System.out.println("Invalid state exception");
            return;
        }
        processMessage(senderAddress, connectionManager, message);
    }

    private ConnectionState getConnectionState(String username, ConnectionManager connectionManager){
        return connectionManager.getUsersWithConnectionDetails().get(username).getConnectionState();
    }

    public abstract void processMessage(InetSocketAddress senderAddress, ConnectionManager connectionManager, MsgType message) throws IOException, P2PNotConnectedException;
    public abstract Set<PeerConnectionState> permittedConnectionStates();
}
