package server.connection.messages.dispatchers;

import proto.messages.data.HelloResponseMessage;
import proto.messages.data.Message;
import server.connection.messages.handlers.HelloHandlerServer;
import server.connection.messages.handlers.ServerMessageHandler;

import java.util.Map;

public class HandlersDispatcher {
    private static Map<Class<?>, ServerMessageHandler> messageHandlerMap;
    static {
        messageHandlerMap.put(HelloResponseMessage.class, new HelloHandlerServer());
    }

    public static ServerMessageHandler dispatchMessage(Message message){
        return messageHandlerMap.get(message.getClass());
    }
}
