package server.connection.manager;

import exceptions.P2PNotConnectedException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import proto.messages.data.Message;
import server.connection.peer.state.UserConnectionDetails;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class ConnectionManager {
    public static final int MAX_PACKET_SIZE = 65_535;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected final DatagramSocket datagramSocket;
    private Map<String, UserConnectionDetails> usersWithConnectionDetails = new HashMap<>();

    public ConnectionManager(int localPort) throws SocketException {
        this.datagramSocket = new DatagramSocket(localPort);
    }

    public void send(String username, Message message) throws IOException, P2PNotConnectedException {
        if (!isPeerConnected(username)){
            throw new P2PNotConnectedException();
        }

        byte[] dataToSend = message.getRawMessage();
        InetAddress address = usersWithConnectionDetails.get(username).getRemoteAddress();
        int port = usersWithConnectionDetails.get(username).getRemotePort();

        DatagramPacket datagramPacket = new DatagramPacket(dataToSend, dataToSend.length, address, port);
        synchronized (datagramSocket){
            datagramSocket.send(datagramPacket);
        }
    }

    public void sendDirect(InetSocketAddress socketAddress, Message message) throws IOException {

        byte[] dataToSend = message.getRawMessage();

        DatagramPacket datagramPacket =
                new DatagramPacket(dataToSend, dataToSend.length, socketAddress.getAddress(), socketAddress.getPort());
        synchronized (datagramSocket){
            datagramSocket.send(datagramPacket);
        }
    }

    public boolean isPeerConnected(String username){
        return usersWithConnectionDetails.containsKey(username)
                && usersWithConnectionDetails.get(username).getConnectionState().equals(PeerConnectionState.CONNECTED);
    }
}
