package server.connection.reliability;

import server.connection.manager.ConnectionManager;
import exceptions.MalformedMessageException;
import exceptions.P2PNotConnectedException;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import proto.messages.data.Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

@Getter
@Setter
public class ReliabilityProvider extends Thread{
    public static final int RESEND_INTERVAL = 1500;//TODO ADD TO CONFIG
    private ConnectionManager connectionManager;
    private BlockingQueue<Message> receivedMessagesBuffer = new LinkedBlockingDeque<Message>();
    private BlockingQueue<Message> sentMessagesBuffer = new LinkedBlockingDeque<Message>();

    private List<Message> messagesToAcknowledge = new ArrayList<>();
    private List<Message> messagesNotAcknowledged = new ArrayList<>();
    private List<String> uniqueMessageUUIDs = new ArrayList<>();

    public ReliabilityProvider(ConnectionManager connectionManager) throws IOException, MalformedMessageException {
        this.connectionManager = connectionManager;
        loadPersistedMessageData();
    }

    @SneakyThrows
    @Override
    public void run(){
        while (true){
            Thread.sleep(RESEND_INTERVAL);
            moveMessagesFromBuffers();
            sendAllAcknowledgement();
            resendAllUnacknowledgedMessages();
        }
    }

    private void sendAllAcknowledgement() throws IOException, P2PNotConnectedException {
        for (Message message : messagesToAcknowledge){
            connectionManager.send(message.getSenderUsername(), message);
        }
    }

    private void resendAllUnacknowledgedMessages() throws IOException, P2PNotConnectedException{
        for (Message message : messagesNotAcknowledged){
            connectionManager.send(message.getReceiverUsername(), message);
        }
    }

    private void loadPersistedMessageData() throws IOException, MalformedMessageException {
        messagesToAcknowledge = MessagePersistProvider.getReceivedMessages();
        messagesNotAcknowledged = MessagePersistProvider.getSentMessages();
        uniqueMessageUUIDs = MessagePersistProvider.getUniqueMessageUUIDs();
    }

    private void moveMessagesFromBuffers() throws IOException {
        while(!receivedMessagesBuffer.isEmpty()){
            Message newMessageToAcknowledge = receivedMessagesBuffer.remove();
            if(!uniqueMessageUUIDs.contains(newMessageToAcknowledge.getUuid())){
                MessagePersistProvider.persistUniqueMessage(newMessageToAcknowledge);
            }
            messagesToAcknowledge.add(newMessageToAcknowledge);
            MessagePersistProvider.persistReceivedMessage(newMessageToAcknowledge);
        }
        while(!sentMessagesBuffer.isEmpty()){
            Message newMessageToWaitForAcknowledgement = receivedMessagesBuffer.remove();
            messagesNotAcknowledged.add(sentMessagesBuffer.remove());
            MessagePersistProvider.persistSentMessage(newMessageToWaitForAcknowledgement);
        }
    }
}
