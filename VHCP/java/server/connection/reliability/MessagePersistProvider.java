package server.connection.reliability;

import server.connection.manager.ConnectionManager;
import exceptions.MalformedMessageException;
import proto.messages.data.DataMessage;
import proto.messages.data.Message;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MessagePersistProvider {
    private static final String SENT_MESSAGES_DIR = ".\\waitingForAck";
    private static final String RECEIVED_MESSAGES_DIR = ".\\toAck";
    private static final String UNIQUE_MESSAGES_REGISTRY = ".\\msgRegister.txt";

    public static void persistUniqueMessage(Message message) throws IOException {
        Files.write(
                Paths.get(UNIQUE_MESSAGES_REGISTRY),
                (Arrays.toString(message.getUuid().getBytes()) + System.lineSeparator()).getBytes(),
                StandardOpenOption.APPEND);
    }

    public static List<String> getUniqueMessageUUIDs() throws IOException {
        String messageUuids = new String(readFileContent(new File(UNIQUE_MESSAGES_REGISTRY)));
        return Arrays.asList(messageUuids.split(System.lineSeparator()));
    }

    public static void persistSentMessage(Message message) throws IOException {
        persistMessage(message, SENT_MESSAGES_DIR);
    }

    public static void persistReceivedMessage(Message message) throws IOException {
        persistMessage(message, RECEIVED_MESSAGES_DIR);
    }

    public static List<Message> getSentMessages() throws IOException, MalformedMessageException {
        return getPersistedMessages(SENT_MESSAGES_DIR);
    }

    public static List<Message> getReceivedMessages() throws IOException, MalformedMessageException {
        return getPersistedMessages(RECEIVED_MESSAGES_DIR);
    }

    private static boolean deleteSentMessage(Message message) {
        return deletePersistedMessage(SENT_MESSAGES_DIR, message);
    }

    private static boolean deleteReceivedMessage(Message message) {
        return deletePersistedMessage(RECEIVED_MESSAGES_DIR, message);
    }

    private static void persistMessage(Message message, String directory) throws IOException {
        createDirectoryIfNecessarily(directory);
        File messagePersistFile = new File(directory + File.separator + message.getUuid());
        FileOutputStream fos = new FileOutputStream(messagePersistFile);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        bos.write(message.getRawMessage());
        bos.flush();
        bos.close();
    }

    private static List<Message> getPersistedMessages(String directory) throws IOException, MalformedMessageException {
        List<File> files = Files.walk(Paths.get(directory))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());
        List<Message> result = new ArrayList<>();
        for (File file : files){
            result.add(Message.fromRawMessage(readFileContent(file)));
        }
        return result;
    }

    private static boolean deletePersistedMessage(String directory, Message message) {
        File file = new File(directory + File.separator + (message.getUuid()));
        return file.delete();
    }

    private static void createDirectoryIfNecessarily(String path){
        File file = new File(path);
        file.mkdir();
    }

    private static byte[] readFileContent(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] fileContent = new byte[ConnectionManager.MAX_PACKET_SIZE];
        int readByte;
        int currId = 0;
        while ((readByte = fis.read()) != -1) {
            fileContent[currId] = (byte)readByte;
            currId ++;
        }
        fis.close();
        return fileContent;
    }

    public static void main(String[] args) throws IOException, MalformedMessageException {
        /*Message msg = AckMessage.builder()
                .acknowledgedUUID("uuid")
                .senderUsername("user")
                .serverConnectionKey("key")
                .build();*/
        Message msg = DataMessage.builder()
                .serverConnectionKey("key")
                .senderUsername("user")
                .dataLength(1)
                .peerKey("key")
                .packagesInMessageCount(2)
                .contentType("binary")
                .packageInMessageSerialNumber(23)
                .fullDataMessageUuid("uuid")
                .data(new byte[] { 1, 2, 3, 4, 5})
                .build();
        persistSentMessage(msg);
        System.out.println(getSentMessages());
    }
}
