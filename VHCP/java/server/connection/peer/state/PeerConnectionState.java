package server.connection.peer.state;

public enum PeerConnectionState {
    CONNECTED,
    INITIALIZING_CONNECTION,
    DISCONNECTED
}
