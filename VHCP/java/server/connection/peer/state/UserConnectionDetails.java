package server.connection.peer.state;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.net.InetAddress;

/**
 * Class represents all the data associated with connection with other peer.
 */
@Getter
@Setter
@Builder
public class UserConnectionDetails {
    private PeerConnectionState connectionState;
    private NatBehindPeer natBehindPeer;
    private InetAddress remoteAddress;
    private int remotePort;
    private InetAddress localAddress;
    private int localPort;
    private String connectionKey;
}
