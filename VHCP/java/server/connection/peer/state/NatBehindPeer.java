package server.connection.peer.state;

public enum NatBehindPeer {
    NO_NAT,
    FULL_CONE,
    ADDRESS_RESTRICTED,
    PORT_RESTRICTED,
    SYMMETRIC
}
