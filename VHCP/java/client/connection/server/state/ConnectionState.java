package client.connection.server.state;

public enum ConnectionState {
    DISCONNECTED,
    CONNECTED
}
