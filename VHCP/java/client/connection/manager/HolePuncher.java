package client.connection.manager;

import exceptions.P2PNotConnectedException;
import lombok.SneakyThrows;
import proto.messages.data.HolePunchPingMessage;
import server.connection.peer.state.PeerConnectionState;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Thread used to ping server and all peers that are currently connected with client
 */
public class HolePuncher extends Thread{
    public static final int PING_INTERVAL = 500;//TODO ADD TO CONFIG
    private final ClientConnectionManager connectionManager;

    public HolePuncher(ClientConnectionManager connectionManager){
        this.connectionManager = connectionManager;
    }

    @SneakyThrows
    @Override
    public void run(){
        while(true){
            sleep();
            for (String username : getConnectedUsersNames()){
                sendPing(username);
            }
            if (connectionManager.isConnectedToServer()) {
                sendPingToServer();
            }
        }
    }

    private List<String> getConnectedUsersNames(){
        return connectionManager.getUsersWithConnectionDetails().entrySet()
                .stream()
                .filter(entry -> entry.getValue().getConnectionState().equals(PeerConnectionState.CONNECTED))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private void sleep(){
        try {
            Thread.sleep(PING_INTERVAL);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sendPing(String username) throws IOException, P2PNotConnectedException {
        HolePunchPingMessage message = HolePunchPingMessage.builder()
                .senderUsername(connectionManager.getMyUsername())
                .serverConnectionKey(connectionManager.getServerConnectionKey())
                .peerKey(connectionManager.getUsersWithConnectionDetails().get(username).getPeerKey())
                .build();
        connectionManager.send(username, message);
    }

    private void sendPingToServer() throws IOException, P2PNotConnectedException {
        HolePunchPingMessage message = HolePunchPingMessage.builder()
                .senderUsername(connectionManager.getMyUsername())
                .serverConnectionKey(connectionManager.getServerConnectionKey())
                .build();
        connectionManager.sendToServer(message);
    }
}
