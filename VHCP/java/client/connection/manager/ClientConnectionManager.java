package client.connection.manager;

import client.connection.peer.state.PeerConnectionDetails;
import client.connection.server.state.ConnectionState;
import exceptions.P2PNotConnectedException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import proto.messages.data.Message;
import server.connection.peer.state.PeerConnectionState;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class ClientConnectionManager  {
    public static InetAddress serverAddress;//TODO add to config
    public static final int serverPort = 10000;// TODO add to config

    private String serverConnectionKey;
    private ConnectionState withServerConnectionState;
    private String myUsername;
    public static final int MAX_PACKET_SIZE = 65_535;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected final DatagramSocket datagramSocket;

    private Map<String, PeerConnectionDetails> usersWithConnectionDetails = new HashMap<>();

    public ClientConnectionManager(int localPort) throws SocketException {
        this.datagramSocket = new DatagramSocket(localPort);
    }

    public void send(String username, Message message) throws IOException, P2PNotConnectedException {
        if (!isPeerConnected(username)){
            throw new P2PNotConnectedException();
        }

        byte[] dataToSend = message.getRawMessage();
        InetAddress address = usersWithConnectionDetails.get(username).getRemoteAddress();
        int port = usersWithConnectionDetails.get(username).getRemotePort();

        DatagramPacket datagramPacket = new DatagramPacket(dataToSend, dataToSend.length, address, port);
        synchronized (datagramSocket){
            datagramSocket.send(datagramPacket);
        }
    }

    public boolean isPeerConnected(String username){
        return usersWithConnectionDetails.containsKey(username)
                && usersWithConnectionDetails.get(username).getConnectionState().equals(PeerConnectionState.CONNECTED);
    }

    public boolean isConnectedToServer(){
        return getWithServerConnectionState().equals(ConnectionState.CONNECTED);
    }

    public void sendToServer(Message message) throws IOException {
        byte[] dataToSend = message.getRawMessage();

        DatagramPacket datagramPacket = new DatagramPacket(dataToSend, dataToSend.length, serverAddress, serverPort);
        synchronized (datagramSocket){
            datagramSocket.send(datagramPacket);
        }
    }
}
