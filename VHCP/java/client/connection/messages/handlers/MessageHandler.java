package client.connection.messages.handlers;

import client.connection.manager.ClientConnectionManager;
import client.connection.server.state.ConnectionState;
import server.connection.manager.ConnectionManager;
import proto.messages.data.Message;

import java.util.Set;

/**
 * Transformations of connection state. Every message received triggers call of suitable ConnectionStateFunction
 */
public abstract class MessageHandler<MsgType extends Message> {

    public void handleMessage(ClientConnectionManager connectionManager, MsgType message){
        if (!permittedConnectionStates().contains(message)){
            System.out.println("Invalid state exception");
            return;
        }
        processMessage(connectionManager, message);

    }

    public abstract void processMessage(ClientConnectionManager connectionManager, MsgType message);
    public abstract Set<ConnectionState> permittedConnectionStates();
}
