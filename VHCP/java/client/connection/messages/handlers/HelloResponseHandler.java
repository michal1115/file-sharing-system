package client.connection.messages.handlers;

import client.connection.manager.ClientConnectionManager;
import client.connection.server.state.ConnectionState;
import org.jetbrains.annotations.NotNull;
import proto.messages.data.HelloResponseMessage;

import java.util.*;

public class HelloResponseHandler extends MessageHandler<HelloResponseMessage> {

    @Override
    public void processMessage(ClientConnectionManager connectionManager, HelloResponseMessage message) {
        if (!message.isUsernamePermitted()){
            //TODO throws exception
            System.out.println("Invalid username");
            return;
        }
        connectionManager.setWithServerConnectionState(ConnectionState.CONNECTED);
        connectionManager.setServerConnectionKey(message.getServerConnectionKey());
    }

    @Override
    public Set<ConnectionState> permittedConnectionStates() {
        return new HashSet<>(Arrays.asList(ConnectionState.DISCONNECTED));
    }
}
