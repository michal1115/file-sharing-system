package client.connection.messages.handlers;

import client.connection.manager.ClientConnectionManager;
import client.connection.server.state.ConnectionState;
import proto.messages.data.ConnectionRequestMessage;

import java.util.Set;

public class ConnectionRequestHandler extends MessageHandler<ConnectionRequestMessage> {
    @Override
    public void processMessage(ClientConnectionManager connectionManager, ConnectionRequestMessage message) {
        String peerUsername = message.getPeerUsername();
        if (connectionManager.isPeerConnected(peerUsername)){
            return;
        }


    }

    @Override
    public Set<ConnectionState> permittedConnectionStates() {
        return null;
    }


}
