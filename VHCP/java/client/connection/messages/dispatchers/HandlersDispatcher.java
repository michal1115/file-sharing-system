package client.connection.messages.dispatchers;

import client.connection.messages.handlers.HelloResponseHandler;
import client.connection.messages.handlers.MessageHandler;
import com.google.common.collect.ImmutableMap;
import proto.messages.data.*;

import java.util.Map;

public class HandlersDispatcher {
    private static Map<Class<?>, MessageHandler> messageHandlerMap;
    static {
        messageHandlerMap.put(HelloResponseMessage.class, new HelloResponseHandler());
    }

    public static MessageHandler dispatchMessage(Message message){
        return messageHandlerMap.get(message.getClass());
    }
}
