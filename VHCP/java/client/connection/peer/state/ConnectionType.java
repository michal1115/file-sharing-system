package client.connection.peer.state;

/**
 * Connection type:
 * - DIRECT - both devices are in same local network
 * - HOLE_PUNCHED - it is possible to directly communicate via hole-punching
 * - PROXY - direct communication is impossible, randezvous server behaves as proxy
 */
public enum ConnectionType {
    DIRECT,
    HOLE_PUNCHED,
    PROXY
}
