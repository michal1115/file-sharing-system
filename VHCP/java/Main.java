/*import de.javawi.jstun.attribute.MessageAttributeException;
import de.javawi.jstun.header.MessageHeaderParsingException;
import de.javawi.jstun.test.DiscoveryInfo;
import de.javawi.jstun.test.DiscoveryTest;
import de.javawi.jstun.util.UtilityException;

import java.io.IOException;
import java.net.InetAddress;*/

/*DiscoveryTest test = new DiscoveryTest(InetAddress.getByName("192.168.1.14"), 30001, "stun.ekiga.net", 3479);
DiscoveryInfo res = test.test();*/

import exceptions.MalformedMessageException;
import proto.messages.data.*;

import java.io.IOException;
import java.net.InetAddress;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException, MalformedMessageException {
        DataMessage message2 = DataMessage.builder()
                .fullDataMessageUuid("a")
                .packageInMessageSerialNumber(1)
                .packagesInMessageCount(2)
                .contentType("binary")
                .data("hello".getBytes())
                .dataLength(5)
                .peerKey("abc")
                .serverConnectionKey("def")
                .senderUsername("user")
                .build();
        System.out.println(new String(Message.fromRawMessage(new String(message2.getRawMessage()).getBytes()).getRawMessage()));

    }
}
