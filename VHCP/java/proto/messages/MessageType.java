package proto.messages;

import lombok.Getter;
import lombok.Setter;

/**
 * Message that can be send across system:
 * - INIT - initialize connection (with both server and clients)
 * - INIT_ACK - potwierdzenie tego, że przyszedł pakiet
 * - HELLO - przedstawienie się serwerowi
 * - HELLO_ACK - potwierdzenie przedstawienia się, otrzymanie informacji o tym czy nazwa jest wolna, otrzymanie klucza (żeby ktoś nagle nie wysyłał innych z nie tej nazwy)
 * - WANT_TO_CONNECT - (username, ip_local, port_local, ip_remote, port_remote, NAT_data) - prośba o nawiązanie połączenia z podanym peerem
 * - CONNECTION_REQUEST - wysyłane po WANT_TO_CONNECT do obu peerów w ramach prośby o nawiązanie połączenia (zawiera peerKey)
 * - DATA - dane
 * - ACK - potwierdzenie (uniwersalne do wszystkiego)
 * - ERROR - zgloszenie błędu
 */
@Getter
public enum MessageType {
    HELLO, //N-auto-ACK
    HELLO_RESPONSE,//N-auto-ACK
    INIT,//N-auto-ACK
    WANT_TO_CONNECT,
    CONNECTION_REQUEST,
    DATA,
    ACK,//N-auto-ACK
    ERR;
}
