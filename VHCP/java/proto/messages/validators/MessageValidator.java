package proto.messages.validators;

import proto.messages.data.Message;

public interface MessageValidator {
    boolean isValid(byte[] message);
    Message getMessage(byte[] message);
}
