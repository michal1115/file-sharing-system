package proto.messages.data;

import com.google.common.collect.ImmutableMap;
import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;
import proto.messages.readers.MessageContentReader;
import proto.messages.readers.MessageReaderFunction;

import java.util.*;

@Getter
@Setter
@SuperBuilder
public class Message {
    public static final String LINE_SEPARATOR = "\r\n";
    public static final String TYPE = null;

    private static ImmutableMap<String, MessageReaderFunction> messageReaderFunctions =
            ImmutableMap.<String, MessageReaderFunction>builder()
                    .put(AckMessage.TYPE, AckMessage::fromRawMessage)
                    .put(ConnectionRequestMessage.TYPE, ConnectionRequestMessage::fromRawMessage)
                    .put(DataMessage.TYPE, DataMessage::fromRawMessage)
                    .put(ErrMessage.TYPE, ErrMessage::fromRawMessage)
                    .put(HelloMessage.TYPE, HelloMessage::fromRawMessage)
                    .put(HelloResponseMessage.TYPE, HelloResponseMessage::fromRawMessage)
                    .put(HolePunchPingMessage.TYPE, HolePunchPingMessage::fromRawMessage)
                    .put(InitMessage.TYPE, InitMessage::fromRawMessage)
                    .put(WantToConnectMessage.TYPE, WantToConnectMessage::fromRawMessage)
                    .build();

    @NotNull
    private final String uuid = UUID.randomUUID().toString();
    @NotNull
    private final Long creationTime = System.currentTimeMillis();
    @NotNull
    private String senderUsername;
    @NotNull
    private String receiverUsername;
    @NotNull
    private String serverConnectionKey;

    public String getMethod(){
        return TYPE;
    }

    public Map<String, Object> getHeaders(){
        Map<String, Object> headers = new HashMap<>();
        headers.put("uuid", uuid);
        headers.put("creationTime", creationTime);
        headers.put("senderUsername", senderUsername);
        headers.put("receiverUsername", receiverUsername);
        headers.put("serverConnectionKey", serverConnectionKey);
        return headers;
    }

    public byte[] getBody(){
        return null;
    };

    public byte[] getRawMessage(){
        byte[] method = getMethodAsBytes();
        byte[] headers = getHeadersAsBytes();
        byte[] body = getBody();

        byte[] combined = new byte[method.length + headers.length + (getBody() == null ? 0 : body.length)];

        System.arraycopy(method, 0, combined, 0, method.length);
        System.arraycopy(headers, 0, combined, method.length, headers.length);
        if (getBody() != null){
            System.arraycopy(body, 0, combined, method.length + headers.length, body.length);
        }

        return combined;
    }

    private byte[] getMethodAsBytes(){
        return (getMethod() + LINE_SEPARATOR).getBytes();
    }

    private byte[] getHeadersAsBytes(){
        return (getHeadersString() + LINE_SEPARATOR).getBytes();
    }

    private String getHeadersString(){
        return getHeaders().entrySet().stream()
                .map(entry -> entry.getKey() + ": " + entry.getValue().toString())
                .reduce("", (acc, elem) -> acc + elem + "\r\n");
    }

    protected static MessageContent getMessageContent(byte[] message) throws MalformedMessageException {
        return MessageContentReader.fromRawMessage(message);
    }

    public static Message fromRawMessage(byte[] message) throws MalformedMessageException {
        MessageContent messageContent = getMessageContent(message);
        String method = messageContent.getMethod();
        return messageReaderFunctions.get(method).read(message);
    }

    public static void main(String[] args){
        System.out.println(messageReaderFunctions);
    }
}
