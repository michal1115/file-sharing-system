package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class ErrMessage extends Message{
    public static final String TYPE = "ERR";
    @NotNull
    private String sourceMessageUUID;
    @NotNull
    private Integer errorCode;
    @NotNull
    private String description;

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("sourceMessageUUID", sourceMessageUUID);
        headersMap.put("errorCode", errorCode);
        headersMap.put("description", description);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static ErrMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return ErrMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .sourceMessageUUID(headers.get("sourceMessageUUID"))
                .errorCode(Integer.parseInt(headers.get("errorCode")))
                .description(headers.get("description"))
                .build();
    }
}
