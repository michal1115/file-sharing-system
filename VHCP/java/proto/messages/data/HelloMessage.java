package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class HelloMessage extends Message{
    public static final String TYPE = "HELLO";
    @NotNull
    private String localAddress;
    @NotNull
    private Integer localPort;
    @NotNull
    private Boolean isNATFullCone;
    @NotNull
    private Boolean isNATAddressRestrictedCone;
    @NotNull
    private Boolean isNATPortRestrictedCone;
    @NotNull
    private Boolean isNATSymmetric;

    public boolean isNATFullCone(){
        return isNATFullCone;
    }

    public boolean isNATAddressRestrictedCone(){
        return isNATAddressRestrictedCone;
    }

    public boolean isNATPortRestrictedCone(){
        return isNATPortRestrictedCone;
    }

    public boolean isNATSymmetric(){
        return isNATSymmetric;
    }

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("localAddress", localAddress);
        headersMap.put("localPort", localPort);
        headersMap.put("isNATFullCone", isNATFullCone);
        headersMap.put("isNATAddressRestrictedCone", isNATAddressRestrictedCone);
        headersMap.put("isNATPortRestrictedCone", isNATPortRestrictedCone);
        headersMap.put("isNATSymmetric", isNATSymmetric);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static HelloMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return HelloMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .localAddress(headers.get("localAddress"))
                .localPort(Integer.parseInt(headers.get("localPort")))
                .isNATFullCone(Boolean.getBoolean(headers.get("isNATFullCone")))
                .isNATAddressRestrictedCone(Boolean.getBoolean(headers.get("isNATAddressRestrictedCone")))
                .isNATPortRestrictedCone(Boolean.getBoolean(headers.get("isNATPortRestrictedCone")))
                .isNATSymmetric(Boolean.getBoolean(headers.get("isNATSymmetric")))
                .build();
    }
}
