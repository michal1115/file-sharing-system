package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class WantToConnectMessage extends Message{
    public static final String TYPE = "WANT_TO_CONNECT";
    @NotNull
    private String destinationUser;

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("destinationUser", destinationUser);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static WantToConnectMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return WantToConnectMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .destinationUser(headers.get("destinationUser"))
                .build();
    }
}
