package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.Map;

/**
 * Message sent to the user in situation when one of the peer-to-peer connection members initialized connection
 */
@Getter
@Setter
@SuperBuilder
public class ConnectionRequestMessage extends Message{
    public static final String TYPE = "CONNECTION_REQUEST";
    @NotNull
    private String peerUsername;
    @NotNull
    private String peerKey;
    /**
     * Contains peer address if isViaProxy = false, else contains server address
     */
    @NotNull
    private String peerAddress;
    /**
     * Contains peer port if isViaProxy = false, else contains server port
     */
    @NotNull
    private Integer peerPort;
    /**
     * Specifies if message should be sent directly to the peer or via proxy server
     */
    @NotNull
    private Boolean isViaProxy;

    public boolean isViaProxy(){
        return isViaProxy;
    }

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("peerUsername", peerUsername);
        headersMap.put("peerKey", peerKey);
        headersMap.put("peerAddress", peerAddress);
        headersMap.put("peerPort", peerPort);
        headersMap.put("isViaProxy", isViaProxy);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static ConnectionRequestMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return ConnectionRequestMessage.builder()
                .peerUsername(headers.get("peerUsername"))
                .peerKey(headers.get("peerKey"))
                .peerAddress(headers.get("peerAddress"))
                .peerPort(Integer.parseInt(headers.get("peerPort")))
                .isViaProxy(Boolean.getBoolean(headers.get("isViaProxy")))
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .build();
    }
}
