package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class InitMessage extends Message{
    public static final String TYPE = "INIT";
    @NotNull
    private String peerKey;

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("peerKey", peerKey);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static InitMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return InitMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .peerKey(headers.get("peerKey"))
                .build();
    }
}
