package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import proto.messages.readers.MessageContent;

import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class DataMessage extends Message{
    public static final String TYPE = "DATA";
    @NotNull
    private String peerKey;
    @NotNull
    private String contentType;
    @NotNull
    private Integer dataLength;
    @NotNull
    private Integer packageInMessageSerialNumber;
    @NotNull
    private Integer packagesInMessageCount;
    @NotNull
    private String fullDataMessageUuid;
    @NotNull
    private byte[] data;

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("peerKey", peerKey);
        headersMap.put("contentType", contentType);
        headersMap.put("dataLength", dataLength);
        headersMap.put("packageInMessageSerialNumber", packageInMessageSerialNumber);
        headersMap.put("packagesInMessageCount", packagesInMessageCount);
        headersMap.put("fullDataMessageUuid", fullDataMessageUuid);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return data;
    }

    public static DataMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return DataMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .peerKey(headers.get("peerKey"))
                .contentType(headers.get("contentType"))
                .dataLength(Integer.parseInt(headers.get("dataLength")))
                .packageInMessageSerialNumber(Integer.parseInt(headers.get("packageInMessageSerialNumber")))
                .packagesInMessageCount(Integer.parseInt(headers.get("packagesInMessageCount")))
                .fullDataMessageUuid(headers.get("fullDataMessageUuid"))
                .data(messageContent.getBody())
                .build();
    }
}
