package proto.messages.data;

import exceptions.MalformedMessageException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import proto.messages.readers.MessageContent;

import java.util.Map;

@Getter
@Setter
@SuperBuilder
public class HolePunchPingMessage extends Message{
    public static final String TYPE = "HOLE_PUNCH_PING";

    private String peerKey;//not needed, if hole is punched through server-client side, this is not checked

    @Override
    public String getMethod(){
        return TYPE;
    }

    @Override
    public Map<String, Object> getHeaders(){
        Map<String, Object> headersMap = super.getHeaders();
        headersMap.put("peerKey", peerKey);
        return headersMap;
    }

    @Override
    public byte[] getBody(){
        return null;
    }

    public static HelloResponseMessage fromRawMessage(byte[] content) throws MalformedMessageException {
        MessageContent messageContent = Message.getMessageContent(content);
        Map<String, String> headers = messageContent.getHeaders();
        return HelloResponseMessage.builder()
                .senderUsername(headers.get("senderUsername"))
                .serverConnectionKey(headers.get("serverConnectionKey"))
                .isUsernamePermitted(Boolean.getBoolean(headers.get("isUsernamePermitted")))
                .userKey(headers.get("userKey"))
                .build();
    }
}
