package proto.messages.metadata;

import java.util.Map;

public interface IMessageMetaData {
    String getMethod();
    Map<String, MessageHeaderProperties> getHeaders();
    MessageBodyProperties getBody();
}
