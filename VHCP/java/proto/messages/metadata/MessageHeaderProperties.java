package proto.messages.metadata;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MessageHeaderProperties {
    private String name;
    private Class<?> propertyClass;
    private boolean isRequired;

    public static MessageHeaderProperties fromJson(JsonObject json) throws ClassNotFoundException {
        return new MessageHeaderProperties(
                json.get("name").getAsString(),
                Class.forName(json.get("class").getAsString()),
                json.get("isRequired").getAsBoolean()
        );
    }
}
