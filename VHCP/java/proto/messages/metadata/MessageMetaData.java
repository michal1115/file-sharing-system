package proto.messages.metadata;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Getter
@Setter
@AllArgsConstructor
public class MessageMetaData {
    private String method;
    private Map<String, MessageHeaderProperties> headers;
    private MessageBodyProperties body;

    public static MessageMetaData fromJson(JsonObject json) throws ClassNotFoundException {
        return new MessageMetaData(
                json.get("method").getAsString(),
                getHeadersMap(json.get("headers").getAsJsonArray()),
                MessageBodyProperties.fromJson(json.get("body").getAsJsonObject()));
    }

    private static Map<String, MessageHeaderProperties> getHeadersMap(JsonArray jsonHeadersArray) throws ClassNotFoundException {
        Map<String, MessageHeaderProperties> headers = new HashMap<>();
        for (JsonElement jsonElement : jsonHeadersArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            headers.put(jsonObject.get("name").getAsString(), MessageHeaderProperties.fromJson(jsonObject));
        }
        return headers;
    }
}
