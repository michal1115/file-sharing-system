package proto.messages.metadata;

import com.google.gson.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MessageMetaDataUtil {
    public static final String META_DATA_DIR_PATH = "C:\\inzynierka\\jstun\\VHCP\\src\\main\\resources\\MessageMetaData";
    public static Map<String, MessageMetaData> messageTypesWithMetaData;
    static{
        try {
            loadMetaData();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void loadMetaData() throws IOException, ClassNotFoundException {
        messageTypesWithMetaData = new HashMap<>();
        List<JsonObject> metaDataJsonObjects = loadMessageMetaDataFiles(META_DATA_DIR_PATH);
        for (JsonObject metaDataJsonObject : metaDataJsonObjects){
            messageTypesWithMetaData.put(metaDataJsonObject.get("method").getAsString(), MessageMetaData.fromJson(metaDataJsonObject));
        }
    }

    private static List<JsonObject> loadMessageMetaDataFiles(String dirPath) throws IOException {
        return Files.walk(Paths.get(META_DATA_DIR_PATH))
                .filter(Files::isRegularFile)
                .filter(file -> file.getFileName().toString().endsWith(".json"))
                .map(file -> file.toFile().getPath())
                .map(MessageMetaDataUtil::getFileScanner)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(MessageMetaDataUtil::getFileContent)
                .map(JsonParser::parseString)
                .map(JsonElement::getAsJsonObject)
                .collect(Collectors.toList());
    }

    private static Optional<Scanner> getFileScanner(String path){
        try {
            File file = new File(path);
            return Optional.of(new Scanner(file));
        } catch (FileNotFoundException e) {
            return Optional.empty();
        }
    }

    private static String getFileContent(Scanner fileScanner) {
        StringBuilder stringBuilder = new StringBuilder();
        while(fileScanner.hasNext()){
            stringBuilder.append(fileScanner.next());
        }
        return stringBuilder.toString();
    }

    public static MessageMetaData getMessageMetaData(String messageType){
        return messageTypesWithMetaData.get(messageType);
    }
}
