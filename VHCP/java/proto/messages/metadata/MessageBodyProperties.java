package proto.messages.metadata;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MessageBodyProperties {
    private String type;
    private boolean isRequired;

    public static MessageBodyProperties fromJson(JsonObject json){
        return new MessageBodyProperties(
                json.get("type").getAsString(),
                json.get("isRequired").getAsBoolean());
    }
}
