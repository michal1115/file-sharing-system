package proto.messages.readers;

import exceptions.MalformedMessageException;
import proto.messages.data.Message;

public interface MessageReaderFunction {
    Message read(byte[] data) throws MalformedMessageException;
}
