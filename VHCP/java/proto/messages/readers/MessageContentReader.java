package proto.messages.readers;

import com.google.common.collect.ImmutableMap;
import exceptions.MalformedMessageException;
import javafx.util.Pair;
import proto.messages.data.AckMessage;
import proto.messages.data.DataMessage;
import proto.messages.data.Message;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MessageContentReader {

    public static MessageContent fromRawMessage(byte[] message) throws MalformedMessageException {
        Pair<byte[], byte[]> headersAndBody = splitToHeadersAndBody(message);
        List<String> headerLines = Arrays.asList(new String(headersAndBody.getKey()).split("\r\n"));

        String method = headerLines.get(0);
        Map<String, String> headers = headerLines.subList(1, headerLines.size()).stream()
                .map(line -> line.split(": "))
                .collect(Collectors.toMap(line -> line[0], line -> line[1]));
        byte[] body = headersAndBody.getValue();

        return MessageContent.builder()
                .method(method)
                .headers(headers)
                .body(body)
                .build();
    }

    private static Pair<byte[], byte[]> splitToHeadersAndBody(byte[] message) throws MalformedMessageException {
        Optional<Integer> firstHeaderBodySeparatorId = getHeaderBodySeparatorId(message);
        if (!firstHeaderBodySeparatorId.isPresent()){
            throw new MalformedMessageException();
        }

        return new Pair<byte[], byte[]>(
                subArray(0, firstHeaderBodySeparatorId.get(), message),
                subArray(firstHeaderBodySeparatorId.get() + "\r\n\r\n".getBytes().length, message.length, message));
    }

    private static Optional<Integer> getHeaderBodySeparatorId(byte[] message){
        byte[] separator = "\r\n\r\n".getBytes();
        return IntStream.range(0, message.length)
                .boxed()
                .filter(startId -> compareBytes(message, startId, separator))
                .findFirst();
    }

    private static boolean compareBytes(byte[] source, int startId, byte[] toCompareWith){
        for (int i = 0; i < toCompareWith.length; i++){
            if (source[i + startId] != toCompareWith[i]){
                return false;
            }
        }
        return true;
    }

    private static byte[] subArray(int startId, int endId, byte[] array){
        byte[] copied = new byte[endId - startId];
        System.arraycopy(array, startId, copied, 0, endId - startId);
        return copied;
    }

    public static void main(String[] args) throws MalformedMessageException {
        //Message message = Message.builder().senderUsername("abc").serverConnectionKey("def").build();
        DataMessage message = DataMessage.builder()
                .senderUsername("abc")
                .serverConnectionKey("def")
                .contentType("binary")
                .dataLength(12)
                .fullDataMessageUuid("abc")
                .packageInMessageSerialNumber(32)
                .packagesInMessageCount(32)
                .peerKey("123")
                .data(new byte[]{1, 2, 3, 4, 72})
                .build();
        byte[] data = message.getRawMessage();
        MessageContent messageContent = fromRawMessage(data);
        System.out.println(messageContent.getMethod());
        System.out.println(messageContent.getHeaders());
        System.out.println("data:");
        for (byte b: messageContent.getBody()){
            System.out.println(b);
        }
    }
}
