package proto.messages.readers;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@Builder
public class MessageContent {
    private String method;
    private Map<String, String> headers;
    private byte[] body;
}
